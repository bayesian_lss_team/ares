/*+
    ARES/HADES/BORG Package -- ./tools/ares/ares_mock_gen.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018, 2020)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

+*/
#ifndef __ARES_MOCK_GEN_HPP
#define __ARES_MOCK_GEN_HPP

namespace LibLSS {

  template <typename PTree>
  void prepareMockData(
      PTree &ptree, MPI_Communication *comm, MainLoop &loop,
      CosmologicalParameters &cosmo_params, SamplerBundle &bundle) {
    ConsoleContext<LOG_INFO_SINGLE> ctx("prepareMockData");
    auto &state = loop.getState(0);

    createCosmologicalPowerSpectrum(state, cosmo_params);

    bundle.sel_updater.sample(state);
    bundle.sampler_s.setMockGeneration(true);
    bundle.sampler_catalog_projector.setMockGeneration(true);

    // Generate the tau
    bundle.sampler_catalog_projector.sample(state);
    bundle.sampler_s.sample(state);
    // Generate the data
    bundle.sampler_catalog_projector.sample(state);

    bundle.sampler_s.setMockGeneration(false);
    bundle.sampler_t.setMockGeneration(false);
    bundle.sampler_catalog_projector.setMockGeneration(false);

    {
      boost::optional<H5::H5File> f;

      if (comm->rank() == 0)
        f = H5::H5File("mock_data.h5", H5F_ACC_TRUNC);
      state.mpiSaveState(f, comm, false);
    }

    state.get<ArrayType>("s_field")->fill(0);
    state.get<ArrayType>("x_field")->fill(0);
  }
} // namespace LibLSS

#endif
