/*+
    ARES/HADES/BORG Package -- ./tools/common/mock_gen.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015, 2017-2018, 2020)

+*/
#ifndef __ARES_MOCK_DATA_HPP
#define __ARES_MOCK_DATA_HPP

#include <CosmoTool/algo.hpp>
#include <CosmoTool/cosmopower.hpp>
#include "libLSS/tools/console.hpp"
#include "libLSS/tools/log_traits.hpp"
#include <boost/format.hpp>
#include "configuration.hpp"
#include "libLSS/physics/cosmo_power.hpp"

#include SAMPLER_MOCK_GENERATOR

#endif
