/*+
    ARES/HADES/BORG Package -- ./libLSS/samplers/core/markov.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015, 2018, 2020)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

+*/
#pragma once
#ifndef __LIBLSS_MARKOV_SAMPLER_HPP
#  define __LIBLSS_MARKOV_SAMPLER_HPP

#  include <tuple>
#  include <memory>
#  include <list>
#  include "libLSS/mcmc/global_state.hpp"

namespace LibLSS {

  typedef std::tuple<unsigned int, MarkovState &> StateTuple;

  class MarkovSampler {
  protected:
    virtual void initialize(MarkovState &state);
    virtual void restore(MarkovState &state);

    virtual void initialize(std::list<StateTuple>);
    virtual void restore(std::list<StateTuple>);

  private:
    bool yet_init;
    std::string name;

  public:
    MarkovSampler();
    virtual ~MarkovSampler() {}

    std::string const &getName() const { return name; }
    void setName(std::string new_name) { name = new_name; }

    virtual void sample(MarkovState &state);

    virtual void sample(std::list<StateTuple> state);

    /**
     * @brief Called when the sampler needs to initialize the state and itself.
     *
     * LEGACY API: This only supports single chains.
     *
     * The overall dependency is defined by the program unfortunately. The samplers
     * have difference dependency requirements. The state provides some information
     * of the already initialized structures. Some others need to be allocated here
     * by the samplers.
     *
     * @param state the Read/Write markov state
     */
    void init_markov(MarkovState &state);

    /**
     * @brief Called when the sampler needs to initialize the state and itself <b>for restoration</b>.
     *
     * LEGACY API: This only supports single chains.
     *
     * The overall dependency is defined by the program unfortunately. The samplers
     * have difference dependency requirements. The state provides some information
     * of the already initialized structures. Some others need to be allocated here
     * by the samplers. Please note that it is <b>not</b> expected the value to be filled
     * correctly in general, as they will be overwritten quite immediately by reading
     * from the restart file.
     *
     * @param state the Read/Write markov state
     */
    void restore_markov(MarkovState &state);

    /**
     * @brief Called when the sampler needs to initialize the state and itself.
     *
     * New API: Supports multiple paralllel chains.
     *
     * The overall dependency is defined by the program unfortunately. The samplers
     * have difference dependency requirements. The state provides some information
     * of the already initialized structures. Some others need to be allocated here
     * by the samplers.
     *
     * @param state the Read/Write markov states
     */
    void init_markov(std::list<StateTuple> state);

    /**
     * @brief Called when the sampler needs to initialize the state and itself <b>for restoration</b>.
     *
     * New API: Supports multiple paralllel chains.
     *
     * The overall dependency is defined by the program unfortunately. The samplers
     * have difference dependency requirements. The state provides some information
     * of the already initialized structures. Some others need to be allocated here
     * by the samplers. Please note that it is <b>not</b> expected the value to be filled
     * correctly in general, as they will be overwritten quite immediately by reading
     * from the restart file.
     *
     * @param state the Read/Write markov states
     */
    void restore_markov(std::list<StateTuple> state);
  };

} // namespace LibLSS

#endif
