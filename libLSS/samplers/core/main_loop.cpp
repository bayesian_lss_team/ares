/*+
    ARES/HADES/BORG Package -- ./libLSS/samplers/core/main_loop.cpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Franz Elsner <felsner@nca-08.MPA-Garching.MPG.DE> (2017)
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018-2020)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

+*/
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/tools/console.hpp"
#include "libLSS/samplers/core/main_loop.hpp"
#include "libLSS/tools/timing_db.hpp"
#include "libLSS/tools/string_tools.hpp"
#include <algorithm>
#include <tuple>
#include <boost/optional/optional.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include "libLSS/tools/itertools.hpp"

using namespace LibLSS;

std::list<std::shared_ptr<MarkovSampler>>
MainLoop::queryByName(std::string const &name) {
  std::list<std::shared_ptr<MarkovSampler>> foundSamplers;

  for (auto &mclist : ensemble_mclist) {
    auto s = std::get<1>(mclist);
    if (s->getName() == name)
      foundSamplers.push_back(s);
  }
  return foundSamplers;
}

void MainLoop::finalizeGraph() {
  LIBLSS_AUTO_CONTEXT(LOG_INFO_SINGLE, ctx);
  typedef boost::adjacency_list<> Graph;
  typedef boost::graph_traits<Graph>::vertex_descriptor vertex_t;
  typedef std::vector<unsigned int> VI;
  // typedef std::tuple<VI, std::string> Descriptor;
  //std::vector<boost::optional<vertex_t>> chain_heading;

  std::map<vertex_t, Descriptor> desc_map;
  // std::vector<Descriptor> ensemble_mclist;
  Graph g;

  std::map<std::string, std::list<vertex_t>> vertexMap;

  for (auto &mc : ensemble_mclist) {
    auto vd = boost::add_vertex(g);
    desc_map[vd] = mc;
    auto name = std::get<1>(mc)->getName();
    if (name.size() != 0)
      vertexMap[name].push_back(vd);
  }

  // Add other dependencies that are desired for the ordering of samplers
  for (auto &other : otherDependencies) {
    std::list<vertex_t> toVertices;
    try {
      toVertices = vertexMap.at(other.first);
    } catch (std::out_of_range) {
      ctx.format2<LOG_WARNING>(
          "Missing dependency problem for '%s'", other.first);
    }
    for (auto &prev : other.second) {
      std::list<vertex_t> fromVertices;

      try {
        fromVertices = vertexMap.at(prev);
      } catch (std::out_of_range) {
        ctx.format2<LOG_WARNING>(
            "Missing dependency problem for '%s'", other.first);
        continue;
      }
      for (auto &to : toVertices) {
        for (auto &from : fromVertices) {
          //boost::add_edge(refVertex, prevVertex, g);
          boost::add_edge(to, from, g);
        }
      }
    }
  }
  run_order.clear();

  boost::topological_sort(g, std::back_inserter(run_order));
  ctx.print("Sampler ordering:");
  for (auto const &e : itertools::enumerate(run_order)) {
    auto original_id = std::get<1>(e);
    auto s = std::get<1>(ensemble_mclist[original_id]);
    ctx.format(
        "  run_order[%d] = %d (name=\"%s\")", std::get<0>(e), original_id,
        s->getName());
  }
}

void MainLoop::addDependency(
    std::string const &sampler1, std::list<std::string> const &other_samplers) {
  std::copy(
      std::begin(other_samplers), std::end(other_samplers),
      std::back_inserter(otherDependencies[sampler1]));
}

void BlockSampler::adder(Adder_t adder_func) const {
  ConsoleContext<LOG_DEBUG> ctx("adder classic");
  std::for_each(mclist.begin(), mclist.end(), adder_func);
}

void BlockSampler::addToList(
    MCList::const_iterator b, MCList::const_iterator e) {
  mclist.insert(mclist.end(), b, e);
}

BlockSampler::~BlockSampler() {}

void BlockSampler::add(std::shared_ptr<MarkovSampler> &s) {
  mclist.push_back(s);
}

BlockSampler &BlockSampler::operator<<(std::shared_ptr<MarkovSampler> &&s) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  add(s);
  return *this;
}

BlockSampler &BlockSampler::operator<<(std::shared_ptr<MarkovSampler> &s) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  add(s);
  return *this;
}

BlockSampler &BlockSampler::operator<<(MarkovSampler &s) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  auto dummy_s = std::shared_ptr<MarkovSampler>(&s, [](void *) {});
  add(dummy_s);
  return *this;
}

BlockSampler &BlockSampler::operator<<(const BlockSampler &l) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  l.adder([&](Sampler_t s) { add(s); });
  return *this;
}

void BlockLoop::adder(Adder_t adder_func) const {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  ctx.format("num_loop = %d", num_loop);
  for (int l = 0; l < num_loop; l++) {
    std::for_each(mclist.begin(), mclist.end(), adder_func);
  }
}

MainLoop::MainLoop(
    std::shared_ptr<MPI_Communication> comm_, std::string const &prefix_,
    unsigned int nchains)
    : comm(comm_), state(nchains), prefix_dir(prefix_) {
  show_splash();
  mcmc_id = 0;
  selectedChainSpec.push_back(0);
}

MarkovState &MainLoop::getState(unsigned int chain) {
  if (chain >= state.size())
    error_helper<ErrorOutOfBounds>("No such chain id allocated");
  return state[chain];
}

MarkovState const &MainLoop::getState(unsigned int chain) const {
  if (chain >= state.size())
    error_helper<ErrorOutOfBounds>("No such chain id allocated");
  return state[chain];
}

MainLoop::~MainLoop() {}

void MainLoop::add(std::shared_ptr<MarkovSampler> &s) {
  if (!s) {
    error_helper<ErrorParams>("Null MarkovSampler added to the main loop.");
  }
  ensemble_mclist.push_back(std::make_tuple(selectedChainSpec, s));
}

void MainLoop::addToList(MCList::const_iterator b, MCList::const_iterator e) {
  std::for_each(b, e, [&](Sampler_t s) {
    ensemble_mclist.push_back(std::make_tuple(selectedChainSpec, s));
  });
}

void MainLoop::setCurrentChainSpec(ChainSpec spec) {
  Console::instance().c_assert(
      std::all_of(
          spec.begin(), spec.end(),
          [&](unsigned int chain) { return chain < state.size(); }),
      "Invalid chain id");
  selectedChainSpec = spec;
}

void MainLoop::addToChains(ChainSpec chains, BlockLoop const &loop) {
  loop.adder([&](Sampler_t sampler) {
    ensemble_mclist.push_back(std::make_tuple(chains, sampler));
  });
}

void MainLoop::show_splash() {}

void MainLoop::increaseChainNumber(unsigned int targetNumChains) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  if (ensemble_mclist.size() != 0) {
    error_helper<ErrorBadState>(
        "Cannot change number of chains after first sampler injected.");
  }
  if (state.size() >= targetNumChains) {
    error_helper<ErrorOutOfBounds>(
        "Invalid number of targetted num chains (smaller than existing)");
  }
  state.resize(targetNumChains);
}

template <typename T>
using std_list = std::list<T>;

void MainLoop::dispatch(
    std::list<unsigned int> chains,
    std::function<void(std::list<StateTuple>)> F) {
  F(itertools::transform_container<std_list>(chains, [&](unsigned int id) {
    return std::make_tuple(id, std::ref(state[id]));
  }));
}

void MainLoop::initialize() {
  Console &cons = Console::instance();
  LibLSS::ConsoleContext<LOG_STD> ctx("Initializing samplers");

  finalizeGraph();
  for (auto &mc : ensemble_mclist) {
    std::get<1>(mc)->init_markov(itertools::transform_container<std_list>(
        std::get<0>(mc), [&](unsigned int id) {
          return std::make_tuple(id, std::ref(state[id]));
        }));
  }
}

void MainLoop::snap() {
  using boost::str;
  boost::optional<H5::H5File> f;

  if (comm->rank() == 0) {
    f = H5::H5File(
        lssfmt::format("%smcmc_%d.h5", prefix_dir, mcmc_id), H5F_ACC_TRUNC);
  }

  for (unsigned int chain = 0; chain < state.size(); chain++) {
    boost::optional<H5_CommonFileGroup &> g_opt;
    H5::Group g;
    if (f != boost::none) {
      if (state.size() > 1) {
        g = f->createGroup(lssfmt::format("chain_%d", chain));
        g_opt = g;
      } else {
        g_opt = *f;
      }
    }
    state[chain].mpiSaveState(g_opt, comm.get(), false, true);
  }
  mcmc_id++;
}

void MainLoop::save() {
  auto fname_final =
      lssfmt::format("%srestart.h5_%d", prefix_dir, comm->rank());
  auto fname_build = fname_final + "_build";

  {
    H5::H5File f(fname_build, H5F_ACC_TRUNC);

    for (unsigned int chain = 0; chain < state.size(); chain++) {
      boost::optional<H5_CommonFileGroup &> g_opt;
      H5::Group g;
      if (state.size() > 1) {
        g = f.createGroup(lssfmt::format("chain_%d", chain));
        state[chain].saveState(g);
      } else {
        state[chain].saveState(f);
      }
    }
    timings::save(f);
  }
  comm->barrier();

  rename(fname_build.c_str(), fname_final.c_str());
}

void MainLoop::save_crash() {
  auto fname_final =
      lssfmt::format("%scrash_file.h5_%d", prefix_dir, comm->rank());
  auto fname_build = fname_final + "_build";

  {
    H5::H5File f(fname_build, H5F_ACC_TRUNC);
    state[0].saveState(f);
  }

  rename(fname_build.c_str(), fname_final.c_str());
}

void MainLoop::run() {
  ConsoleContext<LOG_STD> ctx("MainLoop::run");
  int count = 0;
  unsigned int numSteps = 0;

  // Compute number of steps in a single pass of the loop
  numSteps = run_order.size();
  Progress<LOG_STD> progress = Console::instance().start_progress<LOG_STD>(
      "Main loop iteration", numSteps, 30);
  for (auto &sampler_id : run_order) {
    auto &mc = ensemble_mclist[sampler_id];
    std::get<1>(mc)->sample(itertools::transform_container<std_list>(
        std::get<0>(mc), [&](unsigned int id) {
          return std::make_tuple(id, std::ref(state[id]));
        }));
    count++;
    progress.update(count);
  }
  progress.destroy();
}

void MainLoop::restore(const std::string &fname, bool flexible) {
  //Console &cons = Console::instance();
  std::string fname_full =
      flexible ? fname : lssfmt::format("%s_%d", fname, comm->rank());
  H5::H5File f(fname_full, 0);
  ConsoleContext<LOG_INFO> ctx("restoration of MCMC state");

  finalizeGraph();
  if (flexible)
    Console::instance().print<LOG_WARNING>("Using flexible mechanism");

  ctx.print("Initialize variables");
  for (auto &mc : ensemble_mclist) {
    std::get<1>(mc)->restore_markov(itertools::transform_container<std_list>(
        std::get<0>(mc), [&](unsigned int id) {
          return std::make_tuple(id, std::ref(state[id]));
        }));
  }

  ctx.print("Load markov state from file");
  for (unsigned int chain = 0; chain < state.size(); chain++) {
    boost::optional<H5_CommonFileGroup &> g_opt;
    H5::Group g;
    if (state.size() > 1) {
      g = f.openGroup(lssfmt::format("chain_%d", chain));

      state[chain].restoreState(g, flexible);
    } else {
      state[chain].restoreState(f, flexible);
    }
  }

  timings::load(f);
}
