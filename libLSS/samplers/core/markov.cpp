/*+
+*/
#include "libLSS/samplers/core/markov.hpp"
#include "libLSS/tools/errors.hpp"
#include <tuple>
#include <list>

using namespace LibLSS;

MarkovSampler::MarkovSampler() : yet_init(false) {}

void MarkovSampler::initialize(MarkovState &state) {
  error_helper<ErrorBadState>(
      "Missing override for initialize() in legacy markov sampler");
}

void MarkovSampler::restore(MarkovState &state) {
  error_helper<ErrorBadState>(
      "Missing override for restore() in legacy markov sampler");
}

void MarkovSampler::sample(MarkovState &state) {
  error_helper<ErrorBadState>(
      "Missing override for sample() in legacy markov sampler");
}

void MarkovSampler::initialize(std::list<StateTuple> state) {
  Console::instance().c_assert(
      state.size() == 1, "Legacy compatibility only works with single chain.");
  initialize(std::get<1>(state.front()));
}

void MarkovSampler::restore(std::list<StateTuple> state) {
  Console::instance().c_assert(
      state.size() == 1, "Legacy compatibility only works with single chain.");
  restore(std::get<1>(state.front()));
}

void MarkovSampler::sample(std::list<StateTuple> state) {
  Console::instance().c_assert(
      state.size() == 1, "Legacy compatibility only works with single chain.");
  sample(std::get<1>(state.front()));
}

void MarkovSampler::init_markov(std::list<StateTuple> state) {
  if (!yet_init) {
    yet_init = true;
    initialize(std::move(state));
  }
}

void MarkovSampler::restore_markov(std::list<StateTuple> state) {
  if (!yet_init) {
    yet_init = true;
    restore(std::move(state));
  }
}

void MarkovSampler::init_markov(MarkovState &state) {
  init_markov({{0, std::ref(state)}});
}

void MarkovSampler::restore_markov(MarkovState &state) {
  restore_markov({{0, std::ref(state)}});
}
