/*+
    ARES/HADES/BORG Package -- ./libLSS/samplers/core/main_loop.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018-2020)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

+*/
#pragma once
#ifndef __LIBLSS_SAMPLERS_MAINLOOP_HPP
#  define __LIBLSS_SAMPLERS_MAINLOOP_HPP

#  include "libLSS/mpi/generic_mpi.hpp"
#  include <functional>
#  include <utility>
#  include <list>
#  include "libLSS/tools/console.hpp"
#  include "libLSS/samplers/core/markov.hpp"
#  include "libLSS/mcmc/global_state.hpp"

namespace LibLSS {

  class BlockLoop;
  class BlockSampler {
  public:
    typedef std::shared_ptr<MarkovSampler> Sampler_t;
    typedef std::list<Sampler_t> MCList;
    typedef std::function<void(Sampler_t)> Adder_t;

  protected:
    MCList mclist;
    friend class BlockLoop;

    virtual void
    addToList(MCList::const_iterator begin, MCList::const_iterator end);

    virtual void add(Sampler_t &s);

  public:
    virtual ~BlockSampler();

    virtual void adder(Adder_t) const;

    BlockSampler &operator<<(std::shared_ptr<MarkovSampler> &&s);

    BlockSampler &operator<<(std::shared_ptr<MarkovSampler> &s);

    BlockSampler &operator<<(MarkovSampler &s);

    BlockSampler &operator<<(const BlockSampler &l);
  };

  class BlockLoop : public BlockSampler {
  private:
    int num_loop;

  protected:
    friend class BlockSampler;
    // Prevent any copy.
    BlockLoop(const BlockLoop &l) { num_loop = l.num_loop; }
    BlockLoop &operator=(const BlockLoop &l) { return *this; }

  public:
    BlockLoop(int loop = 1) : num_loop(loop) {}
    virtual ~BlockLoop() {}

    void setLoop(int loop) { num_loop = loop; }

    void adder(Adder_t) const override;
  };

  class MainLoop : public BlockSampler {
  public:
    typedef std::vector<unsigned int> ChainSpec;

  protected:
    typedef std::tuple<std::vector<unsigned int>, Sampler_t> Descriptor;
    std::map<std::string, std::list<std::string>> otherDependencies;
    std::shared_ptr<MPI_Communication> comm;
    std::vector<MarkovState> state;
    std::vector<Descriptor> ensemble_mclist;
    std::list<unsigned int> run_order;
    int mcmc_id;
    ChainSpec selectedChainSpec;
    std::string prefix_dir;

    void show_splash();

    void addToList(
        MCList::const_iterator begin, MCList::const_iterator end) override;

    void add(Sampler_t &) override;

  public:
    MainLoop(
        std::shared_ptr<MPI_Communication> comm_,
        std::string const &prefix_dir = "", unsigned int nchains = 1);
    virtual ~MainLoop();

    void increaseChainNumber(unsigned int numChains);
    unsigned int getNumberOfChains() const { return state.size(); }

    void setCurrentChainSpec(ChainSpec input_chains);
    void addDependency(
        std::string const &sampler_group,
        std::list<std::string> const &other_sampler_group);
    void addToChains(ChainSpec input_chains, BlockLoop const &loop);

    void finalizeGraph();

    void initialize();
    void restore(const std::string &fname, bool flexible = false);
    void run();
    void save();
    void save_crash();
    void snap();
    std::list<std::shared_ptr<MarkovSampler>>
    queryByName(std::string const &name);

    [[deprecated("PLease use getState instead")]] MarkovState &get_state() {
      return getState();
    }

    void dispatch(
        std::list<unsigned int>, std::function<void(std::list<StateTuple>)>);

    MarkovState &getState(unsigned int chain = 0);
    const MarkovState &getState(unsigned int chain = 0) const;

    void setStepID(int i) { mcmc_id = i; }
  };

} // namespace LibLSS

#endif
