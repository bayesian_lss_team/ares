/*+
+*/
#ifndef __LIBLSS_OPTIMIZATION_ARRAY_REDUCTION_HPP
#define __LIBLSS_OPTIMIZATION_ARRAY_REDUCTION_HPP

#include "libLSS/mpi/generic_mpi.hpp"
#include <memory>
#include <cmath>

namespace LibLSS {

  namespace Optimization {

    template <typename T, size_t N>
    struct ArrayReduction {
      typedef boost::multi_array_ref<T, N> array_t;
      std::shared_ptr<array_t> weight;
      ArrayReduction() {}
      ArrayReduction(std::shared_ptr<array_t> weight_) : weight(weight_) {}

      template <typename Array1, typename Array2>
      inline double
      dotprod(MPI_Communication *comm, Array1 const &a1, Array2 const &a2) {
        auto r = [](auto &&a) { return std::real(a); };
        auto i = [](auto &&a) { return std::imag(a); };
        auto r1 = r(a1);
        auto r2 = r(a2);
        auto i1 = i(a1);
        auto i2 = i(a2);

        long N0 = a1->shape()[0], N1 = a1->shape()[1], N2 = a1->shape()[2];
        double totalsum;
        if (weight) {
          totalsum = (fwrap(*weight) * (r1 * r2 + i1 * i2)).sum();
        } else {
          totalsum = ((r1 * r2 + i1 * i2)).sum();
        }

        comm->all_reduce_t(MPI_IN_PLACE, &totalsum, 1, MPI_SUM);
        return totalsum;
      }
    };
  } // namespace Optimization
} // namespace LibLSS

#endif
