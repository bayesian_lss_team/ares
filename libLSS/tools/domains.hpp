/*+
    ARES/HADES/BORG Package -- libLSS/tools/domains.hpp
    Copyright (C) 2020-2021 Guilhem Lavaux <guilhem.lavaux@iap.fr>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2020-2021)

+*/
#pragma once
#ifndef __LIBLSS_TOOLS_MPI_DOMAIN_HPP
#  define __LIBLSS_TOOLS_MPI_DOMAIN_HPP

#  include <boost/multi_array.hpp>
#  include <cstdint>
#  include <list>
#  include "libLSS/mpi/generic_mpi.hpp"
#  include <boost/optional.hpp>
#  include "libLSS/samplers/core/types_samplers.hpp"

/*
 * The data shuffling algorithm used for domain decomposition is a series of
 * simple steps that, together, achieve fairly complex data exchange tasks.
 * The assumption is that a domain is always an hypercube in N-d. The
 * instanciation exists only for 1d,2d and 3d though. The steps to do
 * a data reshuffling are the following:
 *    - do a collective ``computeCompleteDomainSpec``, which transform gather the information
 *      on the tesselation of the domain over the communicator
 *    - execute the collective ``mpiDomainComputeTodo`` which creates a local list of MPI operations
 *      to execute to build the new decomposition from input_spec to output_spec
 *    - execute the tasks themselves. Only a (small) subset of the tasks are involved for that operation.
 * At the end one tranform a given data decomposition to a new one. The input data array may be discarded if
 * it is not useful anymore.
 * If the geometry of the data reshuffling does not change it is not required to recompute the tasks. That may be
 * the case when data is transposed between a slab representation (adequate for FFTW) and a grid representation (adequate
 * for local convolutions).
 */

namespace LibLSS {

  /**
   * @brief Specify the kind of operation to be executed on a task/slice combination
   */
  enum SliceOperation { REPLICATE, ACCUMULATE };

  /**
   * @brief Specifies the boundary of domains
   *
   * @tparam N number of dimensions
   */
  template <size_t N>
  struct DomainSpec {
    /**
     * @brief Type defining an hypercube boundary.
     *
     * The hypercube is defined by two corners defined in an N-d space.
     * Thus we need 2 * Nd elements in that array.
     */
    typedef std::array<ssize_t, 2 * N> DomainLimit_t;

    /**
     * @brief Boundaries
     *
     * A domain is defined by a set of hypercube boundaries.
     * The assumption is that the hypercubes are disjoint.
     */
    std::vector<DomainLimit_t> domains;

    boost::optional<DomainSpec<N>> intersect(DomainSpec<N> const &other) const;
  };

  /**
   * @brief Hold the complete description of the domain on all nodes.
   *
   * Any sane description should cover completely the hypercube domain.
   *
   * @tparam N number of dimensions of the hypercube
   */
  template <size_t N>
  struct CompleteDomainSpec {
    std::vector<DomainSpec<N>> domainOnRank;
  };

  /**
   * @brief Specific task to exchange data on domains
   *
   * @tparam N
   */
  template <size_t N>
  struct DomainTask {
    typename DomainSpec<N>::DomainLimit_t slice;
    int rankIO;
    bool recv;
    SliceOperation operation;
  };

  /**
   * @brief Todo list of tasks to complete to redistribute domain data on nodes
   *
   * @tparam N
   */
  template <size_t N>
  struct DomainTodo {
    std::list<DomainTask<N>> tasks;
  };

  /**
   * @brief Array of data for the domain
   *
   * @tparam T
   * @tparam N
   */
  template <typename T, size_t N>
  using Domain = LibLSS::multi_array_ref<T, N>;

  /**
   * @brief Gather the complete domain specification.
   *
   * This function gather the description from each tasks about their domain
   * specification and store the result in `complete`.
   * @tparam N
   * @param comm
   * @param complete
   * @param inputSpec
   */
  template <size_t N>
  void computeCompleteDomainSpec(
      MPI_Communication *comm, CompleteDomainSpec<N> &complete,
      DomainSpec<N> const &inputSpec);

  /**
   * @brief Compute the list of tasks to do to transform from one spec to another
   *
   * The algorithm must complete the list of tasks to go from `inputSpec` to `outputSpec`
   * on each node. Provided the domain does not change the list of tasks must be
   * invariant w.r.t held data.
   *
   * @tparam N
   * @param comm
   * @param inputSpec
   * @param outputSpec
   * @param todo
   */
  template <size_t N>
  void mpiDomainComputeTodo(
      MPI_Communication *comm, CompleteDomainSpec<N> const &inputSpec,
      CompleteDomainSpec<N> const &outputSpec, DomainTodo<N> &todo,
      SliceOperation default_op = REPLICATE);

  /**
   * @brief Execute a domain redecomposition
   *
   * @tparam T
   * @tparam N
   * @param comm
   * @param inputSpec
   * @param inputDomain
   * @param outputSpec
   * @param outputDomain
   */
  template <typename T, size_t N>
  void mpiDomainRun(
      MPI_Communication *comm, Domain<T, N> const &input_domains,
      Domain<T, N> &output_domains, DomainTodo<N> const &todo);

} // namespace LibLSS

#endif
// ARES TAG: num_authors = 1
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: year(0) = 2020-2021
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
