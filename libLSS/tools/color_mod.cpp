/*+
    ARES/HADES/BORG Package -- ./libLSS/tools/color_mod.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015, 2018, 2020)

+*/
#include <string>
#include <boost/format.hpp>
#include "libLSS/tools/color_mod.hpp"

using namespace LibLSS;
using namespace LibLSS::Color;

std::string LibLSS::Color::fg(
    ColorValue c, const std::string &text, ColorIntensity i, bool is_console) {
  if (is_console)
    return boost::str(
        boost::format("\x1b[%d;%dm%s\x1b[39;0m") % ((int)c + 30) % (int)i %
        text);
  else
    return text;
}

std::string LibLSS::Color::bg(
    ColorValue c, const std::string &text, ColorIntensity i, bool is_console) {
  if (is_console)
    return boost::str(
        boost::format("\x1b[%d;%dm%s\x1b[49;0m") % ((int)c + 40) % (int)i %
        text);
  else
    return text;
}
