/*+
    ARES/HADES/BORG Package -- ./libLSS/tools/hdf5_type.hpp
    Copyright (C) 2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2019-2020)

+*/
#ifndef __LIBLSS_HDF5_TYPE_HPP
#  define __LIBLSS_HDF5_TYPE_HPP

#  include <CosmoTool/hdf5_array.hpp>

namespace LibLSS {
  using CosmoTool::H5_CommonFileGroup;
}

#endif

// ARES TAG: authors_num = 2
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
// ARES TAG: year(0) = 2019
