/*+
    ARES/HADES/BORG Package -- ./libLSS/tools/sigcatcher.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016, 2018, 2020)

+*/
#ifndef __LIBLSS_SIGCATCHER_HPP
#define __LIBLSS_SIGCATCHER_HPP

#include "libLSS/tools/static_auto.hpp"

AUTO_REGISTRATOR_DECL(sigCatcher);

#endif
