/*+
    ARES/HADES/BORG Package -- libLSS/tools/domains.cpp
    Copyright (C) 2020-2021 Guilhem Lavaux <guilhem.lavaux@iap.fr>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2020-2021)

+*/
#include "libLSS/cconfig.h"
#include <vector>
#include <memory>
#include "libLSS/tools/console.hpp"
#include "libLSS/tools/string_tools.hpp"
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/tools/domains.hpp"
#include "libLSS/tools/array_tools.hpp"
#include "libLSS/tools/uninitialized_type.hpp"
#include "libLSS/tools/itertools.hpp"

using namespace LibLSS;

template <typename T, size_t N>
using TemporarySlice = LibLSS::U_Array<T, N>;

template <typename T, size_t N>
auto makeTempSlice(typename DomainSpec<N>::DomainLimit_t const &slice) {
  std::array<ssize_t, N> bases, shapes;

  for (unsigned int i = 0; i < N; i++) {
    bases[i] = slice[2 * i];
    shapes[i] = slice[2 * i + 1] - slice[2 * i];
  }
  auto ext = array::make_extent<N>(bases.data(), shapes.data());
  return std::make_shared<TemporarySlice<T, N>>(ext);
}

template <typename T, size_t N>
std::shared_ptr<TemporarySlice<T, N>> extractSlice(
    Domain<T, N> const &input,
    typename DomainSpec<N>::DomainLimit_t const &slice) {
  auto ret = makeTempSlice<T, N>(slice);

  fwrap(ret->get_array()) = input;
  return ret;
}

namespace {
  template <size_t I, size_t N>
  struct _slice_maker {
    template <typename E>
    static auto make(E e, typename DomainSpec<N>::DomainLimit_t const &slice) {
      typedef boost::multi_array_types::index_range r;
      return _slice_maker<I + 1, N>::make(
          e[r(slice[2 * I], slice[2 * I + 1])], slice);
    }
  };

  template <size_t N>
  struct _slice_maker<N, N> {
    template <typename E>
    static auto make(E e, typename DomainSpec<N>::DomainLimit_t const &slice) {
      return e;
    }
  };

  template <size_t N>
  auto _make_slice(typename DomainSpec<N>::DomainLimit_t const &slice) {
    return _slice_maker<0, N>::make(boost::indices, slice);
  }

  template <typename Array>
  std::string get_str_shape(Array const &a) {
    constexpr size_t N = Array::dimensionality;
    std::string s = "(";

    for (size_t i = 0; i < N; i++) {
      s += std::to_string(a.index_bases()[i]) + " - " +
           std::to_string(a.index_bases()[i] + a.shape()[i]) + ", ";
    }
    return s + ")";
  }
} // namespace

template <typename T, size_t N>
void pushSlice(
    std::shared_ptr<TemporarySlice<T, N>> tmp_slice, Domain<T, N> &output,
    typename DomainSpec<N>::DomainLimit_t const &slice,
    SliceOperation operation) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  // We have to restrict the output in case
  auto s = _make_slice<N>(slice);
  ctx.format("tmp_slice (%s)", get_str_shape(tmp_slice->get_array()));
  ctx.format("output (%s)", get_str_shape(output));
  ctx.format("sliced output (%s)", get_str_shape(output[s]));
  switch (operation) {
  case REPLICATE:
    fwrap(output[s]) = tmp_slice->get_array()[s];
    break;
  case ACCUMULATE:
    fwrap(output[s]) += tmp_slice->get_array()[s];
    break;
  default:
    error_helper_fmt<ErrorBadState>("Invalid slice operation %d", operation);
  };
  ctx.format("output[0] = %g", output.data()[0]);
}

template <size_t N>
boost::optional<DomainSpec<N>>
DomainSpec<N>::intersect(DomainSpec<N> const &other) const {
  if (domains.size() == 0 || other.domains.size() == 0)
    return boost::optional<DomainSpec<N>>();

  Console::instance().c_assert(
      domains.size() == 1,
      "Only intersect of single hypercube are supported at the moment");
  DomainSpec<N> result;

  std::array<ssize_t, N> start, end;
  result.domains.resize(1);
  for (unsigned int i = 0; i < N; i++) {
    start[i] = domains[0][2 * i];
    end[i] = domains[0][2 * i + 1];

    auto other_start = other.domains[0][2 * i];
    auto other_end = other.domains[0][2 * i + 1];

    if (end[i] < other_start || other_end < start[i])
      return boost::optional<DomainSpec<N>>();

    start[i] = std::max(start[i], other_start);
    end[i] = std::min(end[i], other_end);

    result.domains[0][2 * i] = start[i];
    result.domains[0][2 * i + 1] = end[i];
  }
  return result;
}

template <size_t N>
void LibLSS::computeCompleteDomainSpec(
    MPI_Communication *comm, CompleteDomainSpec<N> &complete,
    DomainSpec<N> const &inputSpec) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  size_t commSize = comm->size();
  size_t rank = comm->rank();
  std::unique_ptr<int[]> domainPerNodes(new int[commSize]),
      displs(new int[commSize]);
  std::unique_ptr<ssize_t[]> tmp_domain(new ssize_t[2 * N * commSize]);
  RequestArray requests(boost::extents[commSize]);

  complete.domainOnRank.resize(commSize);

  domainPerNodes[rank] = boost::numeric_cast<int>(inputSpec.domains.size());
  comm->all_gather_t(&domainPerNodes[comm->rank()], 1, domainPerNodes.get(), 1);
  // We now have the size of each input domain
  //
  // Now each node must broadcast its exact domain spec to everybody.
  {
    int previous = 0;
    for (size_t i = 0; i < commSize; i++) {
      complete.domainOnRank[i].domains.resize(domainPerNodes[i]);
      // Now domainPerNodes contain the number of elements for the descriptor.
      domainPerNodes[i] *= 2 * N;
      // Add to the displacement.
      displs[i] = previous;
      ctx.format(
          "domainPerNodes[%d]=%d,  displs[%d]=%d", i, domainPerNodes[i], i,
          displs[i]);
      previous += domainPerNodes[i];
    }
  }

  // Do a vector gather over the communicator
  comm->all_gatherv_t(
      &inputSpec.domains[0][0], domainPerNodes[rank], tmp_domain.get(),
      domainPerNodes.get(), displs.get());

  // Copy the result back in place
  for (size_t i = 0; i < commSize; i++) {
    int numDomains = domainPerNodes[i] / (2 * int(N));
    for (int j = 0; j < numDomains; j++) {
      std::copy(
          &tmp_domain[displs[i]], &tmp_domain[displs[i] + domainPerNodes[i]],
          complete.domainOnRank[i].domains[j].begin());
    }
  }
}

template <size_t N>
void LibLSS::mpiDomainComputeTodo(
    MPI_Communication *comm, CompleteDomainSpec<N> const &inputSpec,
    CompleteDomainSpec<N> const &outputSpec, DomainTodo<N> &todo,
    SliceOperation default_op) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  // Now that all nodes know everything. We may compute the I/O operations to achieve.
  // i.e. which nodes are peers for the current one and which slices

  // Clear up the todo list
  todo.tasks.clear();

  // We will build the tasks to execute between this node and the others based on the description.
  // First which pieces to send

  {
    auto &current_domain = inputSpec.domainOnRank[comm->rank()];

    for (int r = 0; r < comm->size(); r++) {
      //An intersection of two hypercube is still a single hybercube
      DomainTask<N> task;

      auto result = current_domain.intersect(outputSpec.domainOnRank[r]);
      if (!result)
        continue;
      Console::instance().c_assert(
          result->domains.size() == 1,
          "Only single element domains supported at the moment");
      task.slice = result->domains[0];
      task.recv = false;
      task.rankIO = r;
      task.operation = default_op;
      todo.tasks.push_back(task);
    }
  }

  {
    auto &current_domain = outputSpec.domainOnRank[comm->rank()];

    for (int r = 0; r < comm->size(); r++) {
      //An intersection of two hypercube is still a single hybercube
      DomainTask<N> task;

      auto result = current_domain.intersect(inputSpec.domainOnRank[r]);
      if (!result)
        continue;
      Console::instance().c_assert(
          result->domains.size() == 1,
          "Only single element domains supported at the moment");
      task.slice = result->domains[0];
      task.recv = true;
      task.rankIO = r;
      task.operation = default_op;
      todo.tasks.push_back(task);
    }
  }
}

template <typename T, size_t N>
void LibLSS::mpiDomainRun(
    MPI_Communication *comm, Domain<T, N> const &input_domains,
    Domain<T, N> &output_domains, DomainTodo<N> const &todo) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  size_t numTasks = todo.tasks.size();
  int thisRank = comm->rank();
  std::vector<MPICC_Request> requestList(numTasks);
  std::vector<MPI_Status> statusList(numTasks);
  std::vector<std::shared_ptr<TemporarySlice<T, N>>> slices(numTasks);
  size_t local_task = 0;
  bool local_task_set = false;

  // Schedule all exchanges
  for (auto iter : itertools::enumerate(todo.tasks)) {
    size_t t = std::get<0>(iter);
    auto &task = std::get<1>(iter);

    if (!task.recv) {
      if (thisRank == task.rankIO) {
        // Look for the recv
        ctx.print("Local copy");
        slices[t] = extractSlice(input_domains, task.slice);
        local_task = t;
        local_task_set = true;
      } else {
        ctx.format(
            "Sending slice %s to %d", to_string(task.slice), task.rankIO);
        slices[t] = extractSlice(input_domains, task.slice);
        requestList[t] = comm->IsendT(
            slices[t]->get_array().data(),
            int(slices[t]->get_array().num_elements()), task.rankIO, 0);
      }
    } else {
      if (thisRank == task.rankIO) {
        Console::instance().c_assert(
            local_task_set,
            "This is impossible the send task must be before the recv task.");
        pushSlice(
            slices[local_task], output_domains, task.slice, task.operation);
      } else {
        ctx.format(
            "Receive slice %s from %d", to_string(task.slice), task.rankIO);
        slices[t] = makeTempSlice<T, N>(task.slice);
        requestList[t] = comm->IrecvT(
            slices[t]->get_array().data(),
            int(slices[t]->get_array().num_elements()), task.rankIO, 0);
      }
    }
  }
  ctx.print("Now process");

  for (auto iter : itertools::enumerate(todo.tasks)) {
    size_t t = std::get<0>(iter);
    auto &task = std::get<1>(iter);

    if (task.recv && task.rankIO != thisRank) {
      requestList[t].wait();
      ctx.format("Saving slice %s", to_string(task.slice));
      pushSlice(slices[t], output_domains, task.slice, task.operation);
    }
  }
  for (auto iter : itertools::enumerate(todo.tasks)) {
    size_t t = std::get<0>(iter);
    auto &task = std::get<1>(iter);

    if (!task.recv && task.rankIO != thisRank)
      requestList[t].wait();
  }
}

#define FORCE(N)                                                               \
  template void LibLSS::mpiDomainComputeTodo<>(                                \
      MPI_Communication * comm, CompleteDomainSpec<N> const &inputSpec,        \
      CompleteDomainSpec<N> const &outputSpec, DomainTodo<N> &todo,            \
      SliceOperation default_op);                                              \
  template void LibLSS::computeCompleteDomainSpec<>(                           \
      MPI_Communication *, CompleteDomainSpec<N> & complete,                   \
      DomainSpec<N> const &inputSpec);                                         \
  template void LibLSS::mpiDomainRun<>(                                        \
      MPI_Communication * comm, Domain<double, N> const &input_domains,        \
      Domain<double, N> &output_domains, DomainTodo<N> const &todo);

FORCE(1);
FORCE(2);
FORCE(3);

// ARES TAG: num_authors = 1
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: year(0) = 2020-2021
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
