/*+
    ARES/HADES/BORG Package -- ./libLSS/tools/fused_array.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016-2020)

+*/
#pragma once
#ifndef __LIBLSS_FUSE_TYPES_HPP
#  define __LIBLSS_FUSE_TYPES_HPP

#  include <boost/multi_array.hpp>

namespace LibLSS {

  namespace FUSE_details {

    namespace FusedArrayTypes {

      typedef boost::multi_array_types::size_type size_type;
      typedef boost::multi_array_types::index index;

    } // namespace FusedArrayTypes

  } // namespace FUSE_details
} // namespace LibLSS

#endif
