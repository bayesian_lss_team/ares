/*+
+*/
#pragma once
#ifndef __LIBLSS_TOOLS_DEMANGLE_HPP
#  define __LIBLSS_TOOLS_DEMANGLE_HPP

#  include <string>

namespace LibLSS {

  std::string cxx_demangle(const char *mangled_name);

}

#endif
