/*+
+*/
#include <string>
#include <stdlib.h>
#include <cxxabi.h>
#include "libLSS/tools/demangle.hpp"
#include "libLSS/tools/console.hpp"
#include "libLSS/tools/errors.hpp"

using namespace LibLSS;

std::string LibLSS::cxx_demangle(const char *sym_name) {
  int status;
  const char *decoded_name = abi::__cxa_demangle(sym_name, 0, 0, &status);

  switch (status) {
  case 0:
    // success
    break;
  case -1:
    Console::instance().c_assert(
        false, "Memory allocation failure in demangling");
    break;
  case -2:
    error_helper<ErrorBadState>("Invalid mangled name");
    break;
  case -3:
    Console::instance().c_assert(
        false, "Invalid argument to abi::__cxa_demangle");
    break;
  default:
    Console::instance().c_assert(false, "Unknown error abi::__cxa_demangle");
    break;
  }
  std::string s(decoded_name);

  ::free((void *)decoded_name);

  return s;
}
