/*+
    ARES/HADES/BORG Package -- ./libLSS/tools/checkmem.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015, 2018, 2020)

+*/
#ifndef __LIBLSS_CHECKMEM_HPP
#define __LIBLSS_CHECKMEM_HPP

/* Stackoverflow: http://stackoverflow.com/questions/257288/is-it-possible-to-write-a-c-template-to-check-for-a-functions-existence
 */
#define HAS_MEM_FUNC(func, name)                                               \
  template <typename T, typename Sign>                                         \
  struct name {                                                                \
    typedef char yes[1];                                                       \
    typedef char no[2];                                                        \
    template <typename U, U>                                                   \
    struct type_check;                                                         \
    template <typename _1>                                                     \
    static yes &chk(type_check<Sign, &_1::func> *);                            \
    template <typename>                                                        \
    static no &chk(...);                                                       \
    static bool const value = sizeof(chk<T>(0)) == sizeof(yes);                \
  }

#endif
