
template <typename T>
class FFTW_Manager<T, 3> : public fftw_details::FFTW_Manager_base<T, 3> {
public:
  unsigned int N0, N1, N2, N2_HC, N2real;

  typedef fftw_details::FFTW_Manager_base<T, 3> super_t;

  typedef typename super_t::Calls Calls;
  typedef typename super_t::plan_type plan_type;
  typedef typename super_t::element element;
  typedef typename super_t::complex_element complex_element;

  typedef typename super_t::ArrayReal ArrayReal;
  typedef typename super_t::ArrayRealPlane ArrayRealPlane;
  typedef typename super_t::ArrayFourier ArrayFourier;
  typedef typename super_t::ArrayFourierPlane ArrayFourierPlane;

  typedef typename super_t::U_ArrayReal U_ArrayReal;
  typedef typename super_t::U_ArrayFourier U_ArrayFourier;

  typedef typename super_t::StrictRange StrictRange;

  // ==== For compatibility with old implementation
  typedef boost::multi_array_ref<complex_element, 1> Plane;
  typedef UninitializedArray<Plane> U_Plane;
  typedef typename U_Plane::array_type U_Array;

  U_Plane *_newPlane() const { return new U_Plane(boost::extents[N1 * N2_HC]); }
  // ==== End compatibility

public:
  FFTW_Manager(std::shared_ptr<MPI_Communication> c, std::array<size_t, 3> d)
      : super_t(c, d) {
    setup_3d(d[0], d[1], d[2]);
  }

  FFTW_Manager(size_t pN0, size_t pN1, size_t pN2, MPI_Communication *c)
      : super_t(c, {pN0, pN1, pN2}) {
    setup_3d(pN0, pN1, pN2);
  }

  void setup_3d(size_t pN0, size_t pN1, size_t pN2) {
    N0 = (unsigned int)(pN0);
    N1 = (unsigned int)(pN1);
    N2 = (unsigned int)(pN2);

#ifdef ARES_MPI_FFTW
    this->local_size = Calls::local_size_3d(
        N0, N1, N2, this->comm->comm(), &this->localN0, &this->startN0);
    // Local size when first two dims are swapped
    this->local_size_t = Calls::local_size_3d(
        N1, N0, N2, this->comm->comm(), &this->localN1, &this->startN1);
#else
    this->local_size_t = this->local_size = N0 * N1 * (N2 / 2 + 1);

    this->localN0 = N0;
    this->startN0 = 0;
    this->startN1 = 0;
    this->localN1 = N1;
#endif
    this->setup();

    N2_HC = (unsigned int)(this->N_HC);
    N2real = (unsigned int)(this->N_real);
  }

  plan_type
  create_c2c_plan(std::complex<T> *in, std::complex<T> *out, int sign) {
    unsigned int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;

    return Calls::plan_dft_3d(
        N0, N1, N2, (typename Calls::complex_type *)in,
        (typename Calls::complex_type *)out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        sign, flags);
  }

  plan_type
  create_r2c_plan(T *in, std::complex<T> *out, bool transposed_out = false) {
    ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::create_r2c_plan");
    unsigned int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;
#ifdef ARES_MPI_FFTW
    if (transposed_out)
      flags |= FFTW_MPI_TRANSPOSED_OUT;
#endif

    return Calls::plan_dft_r2c_3d(
        N0, N1, N2, in, (typename Calls::complex_type *)out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        flags);
  }

  plan_type
  create_c2r_plan(std::complex<T> *in, T *out, bool transposed_in = false) {
    ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::create_c2r_plan");
    int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;
#ifdef ARES_MPI_FFTW
    if (transposed_in)
      flags |= FFTW_MPI_TRANSPOSED_IN;
#endif

    return Calls::plan_dft_c2r_3d(
        N0, N1, N2, (typename Calls::complex_type *)in, out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        flags);
  }
};
