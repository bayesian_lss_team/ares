
namespace fftw_details {
  template <typename T, size_t Dim>
  class FFTW_Manager_base {
  public:
    static const bool FFTW_HELPER_CHATTY = false;

    typedef FFTW_Allocator<T> AllocReal;
    typedef FFTW_Allocator<std::complex<T>> AllocComplex;
#ifdef ARES_MPI_FFTW
    typedef CosmoTool::FFTW_MPI_Calls<T> Calls;
#else
    typedef CosmoTool::FFTW_Calls<T> Calls;
#endif
    typedef typename boost::multi_array_types::extent_gen::gen_type<Dim>::type
        Extent;
    typedef typename Calls::plan_type plan_type;
    typedef T element;
    typedef std::complex<T> complex_element;
    enum { dimensionality = Dim };

    typedef boost::multi_array<element, Dim, AllocReal> ArrayReal;
    typedef boost::multi_array<element, Dim - 1> ArrayRealPlane;
    typedef boost::multi_array<complex_element, Dim, AllocComplex> ArrayFourier;
    typedef boost::multi_array<complex_element, Dim - 1> ArrayFourierPlane;

    typedef UninitializedArray<ArrayReal, AllocReal> U_ArrayReal;
    typedef UninitializedArray<ArrayFourier, AllocComplex> U_ArrayFourier;

    typedef typename ArrayReal::index_gen::template gen_type<Dim, Dim>::type
        StrictRange;

    std::array<size_t, Dim> N;
    long N_HC, N_real;

    boost::multi_array<int, 1> peer;
    ptrdiff_t local_size, local_size_t, localN0, startN0, localN1, startN1;

    AllocReal allocator_real, allocator_real_strict, allocator_t_real;
    AllocComplex allocator_complex, allocator_t_complex;

  protected:
    std::shared_ptr<MPI_Communication> comm;

    void setup() {
      N_HC = N[Dim - 1] / 2 + 1;
#ifdef ARES_MPI_FFTW
      N_real = N_HC * 2;
#else
      N_real = N[Dim - 1];
#endif
      allocator_real.minAllocSize = local_size * 2;
      allocator_complex.minAllocSize = local_size;

      allocator_real_strict.minAllocSize = 0;

      allocator_t_real.minAllocSize = local_size_t * 2;
      allocator_t_complex.minAllocSize = local_size_t;

      init_peer_upgrade_system();
    }

  public:
    FFTW_Manager_base(
        std::shared_ptr<MPI_Communication> c, std::array<size_t, Dim> N_)
        : N(N_), comm(c) {}

    FFTW_Manager_base(MPI_Communication *c, std::array<size_t, Dim> N_)
        : N(N_), comm(std::shared_ptr<MPI_Communication>(c, [](void *) {})) {}

    // -------------------------------------------------------------------------

    enum class enabler_t {};

    template <bool U>
    using EnableIf = typename std::enable_if<U, int>::type;

    // This function returns an index range that strictly
    // reduces the view to the allowed part of the multi_array.
    // This can be used on multi_array 'a' using a[strict_range()].
    template <size_t a = Dim>
    auto strict_range() const {
      typedef boost::multi_array_types::index_range i_range;
      return array::make_star_indices<Dim - 1>(
          boost::indices)[i_range(0, N[Dim - 1])];
    }

    template <size_t a = Dim, EnableIf<(a >= 2)> = 0>
    auto extra_strict_range() const {
      typedef boost::multi_array_types::index_range i_range;
      return array::make_star_indices<Dim - 2>(boost::indices[i_range(
          startN0, startN0 + localN0)])[i_range(0, N[Dim - 1])];
    }

    template <size_t a = Dim, EnableIf<(a == 1)> = 0>
    auto extra_strict_range() const {
      typedef boost::multi_array_types::index_range i_range;
      return boost::indices[i_range(0, N[Dim - 1])];
    }

    template <size_t a = Dim, EnableIf<(a == 1)> = 0>
    auto complex_range() const {
      typedef boost::multi_array_types::index_range i_range;
      return boost::indices[i_range(0, N_HC)];
    }

    template <size_t a = Dim, EnableIf<(a >= 2)> = 0>
    auto complex_range() const {
      typedef boost::multi_array_types::index_range i_range;
      return array::make_star_indices<Dim - 2>(boost::indices[i_range(
          startN0, startN0 + localN0)])[i_range(0, N_HC)];
    }

    // -------------------------------------------------------------------------

    auto extents_complex() const {
      std::array<size_t, Dim> b, e;
      std::fill(b.begin(), b.end(), 0);
      if (Dim >= 2) {
        b[0] = startN0;
        e[0] = localN0;
        std::copy((N.begin() + 1), N.end() - 1, e.begin() + 1);
      }
      e[Dim - 1] = N_HC;

      return array::make_extent<Dim>(b.data(), e.data());
    }

    auto extents_complex_transposed() const {
#ifdef ARES_MPI_FFTW
      std::array<size_t, Dim> b, e;
      std::fill(b.begin(), b.end(), 0);

      if (Dim >= 3) {
        b[0] = startN1;
        e[0] = localN1;
        e[1] = N[0];
        std::copy((N.begin() + 1), N.end() - 1, e.begin() + 1);
      } else if (Dim == 2) {
        b[0] = startN1;
        e[0] = localN1;
      }
      e[Dim - 1] = N_HC;

      return array::make_extent<Dim>(b.data(), e.data());
#else
      return extents_complex();
#endif
    }

    // -------------------------------------------------------------------------
    template <typename ExtentList>
    auto extents_real(
        ExtentList lst, int extra_plane_leakage = 0,
        int negative_plane_leakage = 0) const {
      std::array<size_t, Dim> b, e;
      std::fill(b.begin(), b.end(), 0);

      if (Dim >= 2) {
        b[0] = startN0 - negative_plane_leakage;
        e[0] = localN0 + extra_plane_leakage;
        std::copy((N.begin() + 1), N.end() - 1, e.begin() + 1);
      }
      e[Dim - 1] = N_real;

      return array::make_extent<Dim>(b.data(), e.data(), lst);
    }

    auto extents_real(
        int extra_plane_leakage = 0, int negative_plane_leakage = 0) const {
      return extents_real(
          boost::extents, extra_plane_leakage, negative_plane_leakage);
    }

    template <typename ExtentList>
    auto
    extents_real_strict(ExtentList lst, int extra_plane_leakage = 0) const {
      std::array<size_t, Dim> b, e;
      std::fill(b.begin(), b.end(), 0);

      if (Dim >= 2) {
        b[0] = startN0;
        e[0] = localN0 + extra_plane_leakage;
        std::copy((N.begin() + 1), N.end() - 1, e.begin() + 1);
      }
      e[Dim - 1] = N[Dim - 1];

      return array::make_extent<Dim>(b.data(), e.data(), lst);
    }

    auto extents_real_strict(int extra_plane_leakage = 0) const {
      return extents_real_strict(boost::extents, extra_plane_leakage);
    }

    auto extents_c2c() const {
      std::array<size_t, Dim> b, e;
      std::fill(b.begin(), b.end(), 0);

      if (Dim >= 2) {
        b[0] = startN0;
        e[0] = localN0;
        std::copy((N.begin() + 1), N.end() - 1, e.begin() + 1);
      }
      e[Dim - 1] = N[Dim - 1];

      return array::make_extent<Dim>(b.data(), e.data());
    }

    // -------------------------------------------------------------------------
    U_ArrayReal allocate_array(
        int extra_plane_leakage = 0, int extra_negative_plane_leakage = 0) {
      return U_ArrayReal(
          extents_real(extra_plane_leakage, extra_negative_plane_leakage),
          allocator_real, boost::c_storage_order());
    }

    U_ArrayReal allocate_array_strict(int extra_plane_leakage = 0) {
      return U_ArrayReal(
          extents_real_strict(extra_plane_leakage), allocator_real_strict,
          boost::c_storage_order());
    }

    auto allocate_ptr_array(int extra_plane_leakage = 0) {
      return std::unique_ptr<U_ArrayReal>(new U_ArrayReal(
          extents_real(extra_plane_leakage), allocator_real,
          boost::c_storage_order()));
    }

    auto allocate_ptr_array_strict(int extra_plane_leakage = 0) {
      return std::unique_ptr<U_ArrayReal>(new U_ArrayReal(
          extents_real_strict(extra_plane_leakage), allocator_real,
          boost::c_storage_order()));
    }

    U_ArrayFourier allocate_complex_array() {
      return U_ArrayFourier(
          extents_complex(), allocator_complex, boost::c_storage_order());
    }

    auto allocate_ptr_complex_array(bool transposed = false) {
      return std::unique_ptr<U_ArrayFourier>(new U_ArrayFourier(
          transposed ? extents_complex_transposed() : extents_complex(),
          allocator_complex, boost::c_storage_order()));
    }

    auto allocate_c2c_array() {
      return U_ArrayFourier(
          extents_c2c(), allocator_complex, boost::c_storage_order());
    }

    auto allocate_ptr_c2c_array() {
      return std::unique_ptr<U_ArrayFourier>(new U_ArrayFourier(
          extents_c2c(), allocator_complex, boost::c_storage_order()));
    }

    // -------------------------------------------------------------------------

    MPI_Communication *getComm() { return comm.get(); }

    MPI_Communication const *getComm() const { return comm.get(); }

    auto getComm_p() { return comm; }

    auto getComm_p() const { return comm; }

    // -------------------------------------------------------------------------

    void destroy_plan(plan_type p) const {
      ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::destroy_plan");
      return Calls::destroy_plan(p);
    }

    template <typename InType, typename OutType>
    static void execute_r2c(plan_type p, InType *in, OutType *out) {
      ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::execute_r2c");
      Calls::execute_r2c(p, in, out);
    }

    template <typename InType, typename OutType>
    static void execute_c2r(plan_type p, InType *in, OutType *out) {
      ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::execute_c2r");
      Calls::execute_c2r(p, in, out);
    }

    template <typename InType, typename OutType>
    static void execute_c2c(plan_type p, InType *in, OutType *out) {
      ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::execute_c2c");
      Calls::execute_c2c(p, in, out);
    }

    bool on_core(long i) const {
      return (i >= startN0 && i < (startN0 + localN0));
    }

    long get_peer(long i) const { return peer[i]; }

  private:
    void init_peer_upgrade_system() {
      ConsoleContext<LOG_DEBUG> ctx("Initializing peer system");
      using boost::extents;
      using boost::format;

      ctx.format("Comm size is %d", comm->size());

      boost::multi_array<ptrdiff_t, 1> all_N0s(extents[comm->size()]);
      size_t localAccumN0 = 0;

      peer.resize(extents[N[0]]);

      // Figure out the peer for each line of Nyquist planes
      // First gather the MPI structure

      comm->all_gather_t(&localN0, 1, all_N0s.data(), 1);

      if (FFTW_HELPER_CHATTY)
        ctx.print("Peers: ");

      for (int p = 0; p < comm->size(); p++) {
        if (FFTW_HELPER_CHATTY)
          ctx.format(" N0[%d] = %d", p, all_N0s[p]);
        // Find the position of the mirror of this line
        for (int i = 0; i < all_N0s[p]; i++)
          peer[i + localAccumN0] = p;
        localAccumN0 += all_N0s[p];
      }
    }
  };

} // namespace fftw_details
