#ifndef __LIBLSS_OLD_UPGRADE_3D_HPP
#define __LIBLSS_OLD_UPGRADE_3D_HPP
#pragma once
#include <boost/core/enable_if.hpp>
#include "libLSS/tools/console.hpp"
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/tools/mpi_fftw_helper.hpp"

namespace LibLSS {

  namespace UpDeGrade {

    namespace details {
#include "libLSS/tools/mpi_fftw/copy_utils.hpp"

#define CHECK_NYQ(q)                                                           \
  Console::instance().c_assert(nyqCheck[q], "Plane not imported")

      template <
          bool upgrading, typename self_t, typename InArray, typename OutArray>
      static typename boost::enable_if<boost::is_same<
          typename InArray::element, typename self_t::complex_element>>::type
      _ud_grade(
          self_t &out_mgr, self_t &small_mgr, self_t &big_mgr,
          const InArray &in_modes, OutArray &out_modes) {
        typedef typename self_t::U_Plane U_Plane;
        typedef typename self_t::element element;
        typedef typename self_t::complex_element complex_element;

        MPI_Status status;
        MPI_Communication *comm = out_mgr.getComm();
        //long thisRank = comm->rank();
        U_Plane *tmp_plane;
        std::vector<U_Plane *> request_planes(small_mgr.N0 + 1);
        RequestArray request_array(boost::extents[small_mgr.N0 + 1]);
        std::vector<bool> request_io(small_mgr.N0 + 1);
        Console &cons = Console::instance();
        typedef internal::copy_utils<upgrading, element> c_util;
        constexpr const bool FFTW_HELPER_CHATTY = self_t::FFTW_HELPER_CHATTY;

        std::fill(request_planes.begin(), request_planes.end(), (U_Plane *)0);
        std::fill(request_io.begin(), request_io.end(), false);

        long half_N0 = small_mgr.N0 / 2;

        // First pass: here we schedule I/O on lines that are no
        for (long i = 0; i < small_mgr.N0; i++) {
          long big_plane = i <= half_N0 ? i : (big_mgr.N0 - small_mgr.N0 + i);
          bool on_core_small = small_mgr.on_core(i),
               on_core_big = big_mgr.on_core(big_plane);
          bool double_on_core = on_core_small && on_core_big;
          bool double_not_on_core = !on_core_small && !on_core_big;

          // Send the line that are here (by construction from startN0, localN0), but that the remote needs (peer tells it)
          if ((i != half_N0 && (double_on_core || double_not_on_core)) ||
              (i == half_N0 && double_on_core &&
               big_mgr.on_core(big_mgr.N0 - half_N0))) {
            continue;
          }

          // Allocate a new plane
          tmp_plane = upgrading ? small_mgr._newPlane() : big_mgr._newPlane();

          auto &a_plane = tmp_plane->get_array();
          long io_id = i;
          bool on_core_source;
          bool on_core_target;
          long peer_source, peer_target;
          long plane_source;

          if (upgrading) {
            on_core_source = on_core_small;
            on_core_target = on_core_big;
            peer_source = small_mgr.peer[i];
            peer_target = big_mgr.peer[big_plane];
            plane_source = i;
          } else {
            on_core_source = on_core_big;
            on_core_target = on_core_small;
            peer_source = big_mgr.peer[big_plane];
            peer_target = small_mgr.peer[i];
            plane_source = big_plane;
          }
          if (FFTW_HELPER_CHATTY)
            cons.format<LOG_DEBUG>(
                "Plane %d / %d, on_core_small = %d, on_core_big = %d", i,
                big_plane, on_core_small, on_core_big);
          if (!on_core_source && on_core_target) {
            if (FFTW_HELPER_CHATTY)
              cons.format<LOG_DEBUG>(
                  "Scheduling to recv plane %d (low) from %d", i, peer_source);
            // This is not in our range. Schedule a receive.

            request_array[i] = comm->Irecv(
                a_plane.data(), a_plane.num_elements(),
                translateMPIType<complex_element>(), peer_source, io_id);
            request_io[i] = true;
            if (FFTW_HELPER_CHATTY)
              cons.print<LOG_DEBUG>("Finished");
          } else if (on_core_source && !on_core_target) {
            if (FFTW_HELPER_CHATTY)
              cons.format<LOG_DEBUG>(
                  "Scheduling to send plane %d (low) to %d", i, peer_target);
            // This is in the range of the other node. Schedule a send.

            // Flatten the data into it
            c_util::_flat_copy_2d_array(
                big_mgr, small_mgr, a_plane, in_modes[plane_source]);
            // Isend the data.
            request_array[i] = comm->Isend(
                a_plane.data(), a_plane.num_elements(),
                translateMPIType<complex_element>(), peer_target, io_id);
            request_io[i] = true;
            if (FFTW_HELPER_CHATTY)
              cons.print<LOG_DEBUG>("Finished");
          }

          if (upgrading) {
            if (i == half_N0 && on_core_small &&
                !big_mgr.on_core(big_mgr.N0 - half_N0)) {
              long peer = big_mgr.get_peer(big_mgr.N0 - half_N0);
              if (FFTW_HELPER_CHATTY)
                cons.format<LOG_DEBUG>(
                    "Scheduling to send plane %d (low, 2) to %d", i, peer);
              request_array[small_mgr.N0] = comm->Isend(
                  a_plane.data(), a_plane.num_elements(),
                  translateMPIType<complex_element>(), peer, io_id);
              request_planes[small_mgr.N0] = tmp_plane;
              request_io[small_mgr.N0] = true;
              if (FFTW_HELPER_CHATTY)
                cons.print<LOG_DEBUG>("Finished");
            }
            if (i == half_N0 && !on_core_small &&
                big_mgr.on_core(big_mgr.N0 - half_N0)) {
              long peer = small_mgr.get_peer(half_N0);
              if (FFTW_HELPER_CHATTY)
                cons.format<LOG_DEBUG>(
                    "Scheduling to recv plane %d (low, 2) from %d", i, peer);
              request_array[small_mgr.N0] = comm->Irecv(
                  a_plane.data(), a_plane.num_elements(),
                  translateMPIType<complex_element>(), peer, io_id);
              request_planes[small_mgr.N0] = tmp_plane;
              request_io[small_mgr.N0] = true;
              if (FFTW_HELPER_CHATTY)
                cons.print<LOG_DEBUG>("Finished");
            }
          } else {
            if (i == half_N0 && on_core_small &&
                !big_mgr.on_core(big_mgr.N0 - half_N0)) {
              long peer = big_mgr.get_peer(big_mgr.N0 - half_N0);
              cons.format<LOG_DEBUG>(
                  "Scheduling to send plane %d (low, 3) to %d", i, peer);
              U_Plane *tmp2_plane = big_mgr._newPlane();
              request_array[small_mgr.N0] = comm->Irecv(
                  tmp2_plane->get_array().data(),
                  tmp2_plane->get_array().num_elements(),
                  translateMPIType<complex_element>(), peer, io_id);
              request_planes[small_mgr.N0] = tmp2_plane;
              request_io[small_mgr.N0] = true;
              if (FFTW_HELPER_CHATTY)
                cons.print<LOG_DEBUG>("Finished");
            }
            if (i == half_N0 && !on_core_small &&
                big_mgr.on_core(big_mgr.N0 - half_N0)) {
              long peer = small_mgr.get_peer(half_N0);
              if (FFTW_HELPER_CHATTY)
                cons.format<LOG_DEBUG>(
                    "Scheduling to recv plane %d (low, 3) from %d", i, peer);

              U_Plane *tmp2_plane = big_mgr._newPlane();

              c_util::_flat_copy_2d_array(
                  big_mgr, small_mgr, tmp2_plane->get_array(),
                  in_modes[big_mgr.N0 - half_N0]);

              request_array[small_mgr.N0] = comm->Isend(
                  tmp2_plane->get_array().data(),
                  tmp2_plane->get_array().num_elements(),
                  translateMPIType<complex_element>(), peer, io_id);
              request_planes[small_mgr.N0] = tmp2_plane;
              request_io[small_mgr.N0] = true;
              if (FFTW_HELPER_CHATTY)
                cons.print<LOG_DEBUG>("Finished");
            }
          }

          // Note down for later destruction
          request_planes[i] = tmp_plane;
        }

        // Now do copy of lines already present. First the positive low freq
        for (long i = 0; i < small_mgr.N0 / 2; i++) {
          if (!small_mgr.on_core(i) || !big_mgr.on_core(i))
            continue;

          if (FFTW_HELPER_CHATTY)
            cons.format<LOG_DEBUG>("copying plane in place %d -> %d", i, i);
          c_util::_copy_sub_2d_plane(
              big_mgr, small_mgr, out_modes[i], in_modes[i]);
        }

        // Next the negative low freq
        for (long i = small_mgr.N0 / 2 + 1; i < small_mgr.N0; i++) {
          long big_plane = big_mgr.N0 - small_mgr.N0 + i;
          if (!small_mgr.on_core(i) || !big_mgr.on_core(big_plane))
            continue;

          long input_plane = upgrading ? i : big_plane;
          long output_plane = upgrading ? big_plane : i;

          if (FFTW_HELPER_CHATTY)
            cons.format<LOG_DEBUG>(
                "copying plane in place %d -> %d", input_plane, output_plane);
          c_util::_copy_sub_2d_plane(
              big_mgr, small_mgr, out_modes[output_plane],
              in_modes[input_plane]);
        }

        for (long i = 0; i < small_mgr.N0 / 2; i++) {
          if (FFTW_HELPER_CHATTY)
            cons.format<LOG_DEBUG>(
                "small.on_core(%d) = %d, big_mgr.on_core(%d) = %d", i,
                small_mgr.on_core(i), i, big_mgr.on_core(i));
          if ((upgrading && (small_mgr.on_core(i) || !big_mgr.on_core(i))) ||
              (!upgrading && (!small_mgr.on_core(i) || big_mgr.on_core(i))))
            continue;

          cons.c_assert(
              request_planes[i] != 0,
              str(boost::format("There should be a pending recv I/O for %d") %
                  i));

          if (FFTW_HELPER_CHATTY)
            cons.format<LOG_DEBUG>("Importing plane %d (big is %d)", i, i);

          request_array[i].wait(&status);
          c_util::_copy_sub_2d_plane_flat(
              big_mgr, small_mgr, out_modes[i], request_planes[i]->get_array());
          internal::safe_delete(request_planes[i]);
        }

        // Half N0 is special. It may broadcast or gather from several cores.

        if (FFTW_HELPER_CHATTY)
          cons.print<LOG_DEBUG>("Half plane");

        internal::Nyquist_adjust<element, upgrading>::handle(
            small_mgr, big_mgr, request_planes, request_io, request_array,
            in_modes, out_modes);

        if (FFTW_HELPER_CHATTY)
          cons.print<LOG_DEBUG>("Half plane done");

        for (long i = small_mgr.N0 / 2 + 1; i < small_mgr.N0; i++) {
          long big_plane = big_mgr.N0 - small_mgr.N0 + i;
          if (FFTW_HELPER_CHATTY)
            cons.format<LOG_DEBUG>(
                "small.on_core(%d) = %d, big_mgr.on_core(%d) = %d", i,
                small_mgr.on_core(i), big_plane, big_mgr.on_core(big_plane));

          //long input_plane = upgrading ? i : big_plane;
          long output_plane = upgrading ? big_plane : i;

          if ((upgrading &&
               (small_mgr.on_core(i) || !big_mgr.on_core(big_plane))) ||
              (!upgrading &&
               (!small_mgr.on_core(i) || big_mgr.on_core(big_plane))))
            continue;

          cons.c_assert(
              request_planes[i] != 0,
              str(boost::format("There should be a pending recv I/O for %d") %
                  i));

          if (FFTW_HELPER_CHATTY)
            cons.format<LOG_DEBUG>(
                "Importing plane %d (big is %d)", i, big_plane);

          request_array[i].wait(&status);
          request_io[i] = false;
          c_util::_copy_sub_2d_plane_flat(
              big_mgr, small_mgr, out_modes[output_plane],
              request_planes[i]->get_array());
          internal::safe_delete(request_planes[i]);
        }

        // Cleanup the send queue.
        for (long i = 0; i <= small_mgr.N0; i++) {
          if (request_io[i]) {
            if (FFTW_HELPER_CHATTY)
              cons.format<LOG_DEBUG>("Waiting for I/O on  plane %d", i);
            request_array[i].wait(&status);
          }
        }
        // No "=" here. The last plane is a potential duplicate.
        long last_plane = upgrading ? small_mgr.N0 : small_mgr.N0 + 1;
        for (long i = 0; i < last_plane; i++) {
          internal::safe_delete(request_planes[i]);
        }
      }

      template <typename self_t, typename Plane>
      void fixBaseNyquist(self_t &mgr, Plane &plane, self_t const &in_mgr) {
        typedef typename Plane::index_range range;
        typename Plane::index_gen indices;

        if (mgr.N1 > in_mgr.N1) {
          array::scalePlane(
              plane[indices[range()][in_mgr.N1 / 2][range()]], 0.5);
          array::scalePlane(
              plane[indices[range()][mgr.N1 - in_mgr.N1 / 2][range()]], 0.5);
        }
        if (mgr.N2 > in_mgr.N2) {
          array::scalePlane(
              plane[indices[range()][range()][in_mgr.N2 / 2]], 0.5);
        }
      }

      /* This function upgrade the multi_array in_modes, described by in_mgr
       * to out_modes described by this manager.
       */
      template <typename self_t, typename InArray, typename OutArray>
      void upgrade_complex(
          self_t &in_mgr, self_t &out_mgr, const InArray &in_modes,
          OutArray &out_modes) {
        ConsoleContext<LOG_DEBUG> ctx("Upgrading modes");
        using namespace boost::lambda;
        using boost::lambda::_1;

        ctx.format(
            "Going from (%d,%d,%d) to (%d,%d,%d)", in_mgr.N0, in_mgr.N1,
            in_mgr.N2, out_mgr.N0, out_mgr.N1, out_mgr.N2);

        if (in_mgr.N0 == out_mgr.N0 && in_mgr.N1 == out_mgr.N1 &&
            in_mgr.N2 == out_mgr.N2) {
          LibLSS::copy_array(out_modes, in_modes);
          return;
        }

        // Same ordering please
        Console::instance().c_assert(
            out_mgr.N0 >= in_mgr.N0, "N0 is not bigger than in_mgr.N0");
        Console::instance().c_assert(
            out_mgr.N1 >= in_mgr.N1, "N1 is not bigger than in_mgr.N1");
        Console::instance().c_assert(
            out_mgr.N2 >= in_mgr.N2, "N2 is not bigger than in_mgr.N2");

        _ud_grade<true>(out_mgr, in_mgr, out_mgr, in_modes, out_modes);

        // Now we have to correct Nyquist planes as they are twice too big
        if (out_mgr.N0 != in_mgr.N0) {
          if (out_mgr.on_core(in_mgr.N0 / 2)) {
            array::scalePlane(out_modes[in_mgr.N0 / 2], 0.5);
          }
          if (out_mgr.on_core(out_mgr.N0 - in_mgr.N0 / 2)) {
            array::scalePlane(out_modes[out_mgr.N0 - in_mgr.N0 / 2], 0.5);
          }
        }
        fixBaseNyquist(out_mgr, out_modes, in_mgr);
      }

      template <
          typename self_t, typename InArray, typename NyqArray,
          typename Functor1, typename Functor2>
      void _degradeExchange(
          self_t &out_mgr, self_t &in_mgr, const InArray &in_modes,
          NyqArray &nyqPlane, boost::multi_array<bool, 1> &nyqCheck,
          const Functor1 &f, ptrdiff_t s, ptrdiff_t e, const Functor2 &g,
          ptrdiff_t in_s, ptrdiff_t in_e, RequestArray &req,
          RequestArray &req_send) {
        typedef typename InArray::index_range in_range;
        typename InArray::index_gen in_indices;
        long half_N2 = out_mgr.N2 / 2;
        Console &cons = Console::instance();
        auto comm = out_mgr.getComm();
        auto const FFTW_HELPER_CHATTY = self_t::FFTW_HELPER_CHATTY;
        long thisRank = comm->rank();

        for (long i = std::max(s, out_mgr.startN0);
             i < std::min(e, out_mgr.startN0 + out_mgr.localN0); i++) {
          long ci = f(i);
          long peer = in_mgr.get_peer(ci);
          long req_idx = ci;

          if (!in_mgr.on_core(ci)) {
            // One comm only please
            if (!req[req_idx].is_active()) {
              if (FFTW_HELPER_CHATTY)
                cons.format<LOG_DEBUG>("Need plane %lg on core %d", ci, peer);
              req[req_idx] =
                  comm->IrecvT(&nyqPlane[ci][0], in_mgr.N1, peer, ci);
              nyqCheck[ci] = true;
            }
          } else {
            cons.c_assert(out_mgr.on_core(i), "Both lines are not on core");
            cons.format<LOG_DEBUG>(
                "Copying line %ld (sz=%ld -> %ld)", ci, in_modes.shape()[1],
                nyqPlane.shape()[1]);
            copy_array_rv(
                nyqPlane[ci],
                in_modes[in_indices[ci][in_range(0, in_mgr.N1)][half_N2]]);
            nyqCheck[ci] = true;
            if (FFTW_HELPER_CHATTY)
              cons.print<LOG_DEBUG>("Done");
          }
        }

        if (FFTW_HELPER_CHATTY)
          cons.format<LOG_DEBUG>("For loop (in_s=%d, in_e=%d)", in_s, in_e);
        for (long ci = std::max(in_s, in_mgr.startN0);
             ci < std::min(in_e, in_mgr.startN0 + in_mgr.localN0); ci++) {
          long i = g(ci);
          long peer = out_mgr.get_peer(i);
          long req_idx = peer + comm->size() * ci;
          if (FFTW_HELPER_CHATTY)
            cons.format<LOG_DEBUG>(
                "Consider to send plane %lg (%ld) to core %d", ci, i, peer);
          if (!req_send[req_idx].is_active() && peer != thisRank) {
            if (FFTW_HELPER_CHATTY)
              cons.format<LOG_DEBUG>("Send plane %lg to core %d", ci, peer);
            cons.c_assert(!req[ci].is_active(), "Plane already allotted");
            copy_array_rv(
                nyqPlane[ci],
                in_modes[in_indices[ci][in_range(0, in_mgr.N1)][half_N2]]);
            req_send[req_idx] =
                comm->IsendT(&nyqPlane[ci][0], in_mgr.N1, peer, ci);
          }
        }
      }

      template <typename self_t, typename InArray, typename OutArray>
      void degrade_complex(
          self_t &in_mgr, self_t &out_mgr, const InArray &in_modes,
          OutArray &out_modes) {
        ConsoleContext<LOG_DEBUG> ctx("Degrading modes");
        using namespace boost::lambda;
        using boost::lambda::_1;
        typedef typename self_t::Plane Plane;
        typedef typename self_t::ArrayFourierPlane ArrayFourierPlane;
        //typedef typename Plane::index_range range;
        typename Plane::index_gen indices;
        auto const FFTW_HELPER_CHATTY = self_t::FFTW_HELPER_CHATTY;
        auto comm = out_mgr.getComm();

        ctx.format(
            "Going from (%d,%d,%d) to (%d,%d,%d)", in_mgr.N0, in_mgr.N1,
            in_mgr.N2, out_mgr.N0, out_mgr.N1, out_mgr.N2);

        if (in_mgr.N0 == out_mgr.N0 && in_mgr.N1 == out_mgr.N1 &&
            in_mgr.N2 == out_mgr.N2) {
          LibLSS::copy_array(out_modes, in_modes);
          return;
        }

        // Same ordering please
        Console::instance().c_assert(
            out_mgr.N0 <= in_mgr.N1, "N0 is not smaller than in_mgr.N0");
        Console::instance().c_assert(
            out_mgr.N1 <= in_mgr.N1, "N1 is not smaller than in_mgr.N1");
        Console::instance().c_assert(
            out_mgr.N2 <= in_mgr.N2, "N2 is not smaller than in_mgr.N2");

        _ud_grade<false>(out_mgr, out_mgr, in_mgr, in_modes, out_modes);

        if (out_mgr.N2 != in_mgr.N2) {
          typedef typename ArrayFourierPlane::const_reference InArrayPlane;
          typedef typename OutArray::reference OutArrayPlane;
          using boost::lambda::_1;
          ArrayFourierPlane nyqPlane(boost::extents[in_mgr.N0][in_mgr.N1]);
          boost::multi_array<bool, 1> nyqCheck(boost::extents[in_mgr.N0]);
          RequestArray req(boost::extents[in_mgr.N0]);
          RequestArray req_send(boost::extents[in_mgr.N0 * comm->size()]);
          StatusArray status(boost::extents[in_mgr.N0]);
          StatusArray status_send(boost::extents[in_mgr.N0 * comm->size()]);

          unsigned int half_N2 = out_mgr.N2 / 2;
          //unsigned int in_half_N2 = out_mgr.N2 / 2;

          // One half sum has not been done.
          // In MPI it will require recapturing part of the N2_HC plane and
          // adding half of its value with conjugation.

          //
          // First we gather all required lines for the collapse of Nyquist plane
          //

          // One round for the forward conjugate planes
          _degradeExchange(
              out_mgr, in_mgr, in_modes, nyqPlane, nyqCheck,
              // Forward
              ((in_mgr.N0 - _1) %
               in_mgr.N0), // transform local plane -> conjugate index
              0, out_mgr.N0 / 2 + 1,
              // Inverse
              (in_mgr.N0 - _1), // transform conjugate index -> local plane
              in_mgr.N0 - out_mgr.N0 / 2, in_mgr.N0,

              req, req_send);

          // One round for the not conjugate planes
          _degradeExchange(
              out_mgr, in_mgr, in_modes, nyqPlane, nyqCheck,
              // Forward
              _1, // identity
              0, out_mgr.N0 / 2 + 1,
              // Inverse
              _1, // identity
              0, out_mgr.N0 / 2 + 1, req, req_send);

          // One round for the backward not conjugate planes
          _degradeExchange(
              out_mgr, in_mgr, in_modes, nyqPlane, nyqCheck,
              // Forward
              out_mgr.N0 - _1, out_mgr.N0 / 2, out_mgr.N0,
              // Inverse
              (out_mgr.N0 - _1) % out_mgr.N0, 0, out_mgr.N0 / 2 + 1, req,
              req_send);

          // One round for the backward conjugate planes
          _degradeExchange(
              out_mgr, in_mgr, in_modes, nyqPlane, nyqCheck,
              // Forward
              (_1 + in_mgr.N0) - out_mgr.N0, out_mgr.N0 / 2, out_mgr.N0,
              // Inverse
              (_1 + out_mgr.N0) - in_mgr.N0, in_mgr.N0 - out_mgr.N0 / 2,
              in_mgr.N0, req, req_send);

          if (FFTW_HELPER_CHATTY)
            ctx.print("All IOs scheduled");

          for (long i = out_mgr.startN0;
               i < std::min(
                       ptrdiff_t(out_mgr.N0 / 2),
                       out_mgr.startN0 + out_mgr.localN0);
               i++) {
            long ci = (in_mgr.N0 - i) % in_mgr.N0, qi = i;
            OutArrayPlane out_i = out_modes[i];
            InArrayPlane in_qi = nyqPlane[qi], in_ci = nyqPlane[ci];

            if (FFTW_HELPER_CHATTY)
              ctx.format("wait for ci=%d", ci);
            req[ci].wait();
            if (FFTW_HELPER_CHATTY)
              ctx.format("wait for qi=%d", qi);
            req[qi].wait();
            if (FFTW_HELPER_CHATTY)
              ctx.print("Done waiting");
            CHECK_NYQ(qi);
            CHECK_NYQ(ci);

            for (long j = 0; j < out_mgr.N1 / 2; j++) {
              long cj = (in_mgr.N1 - j) % in_mgr.N1;
              long qj = j;
              out_modes[i][j][half_N2] =
                  0.5 * (in_qi[qj] + std::conj(in_ci[cj]));
            }

            {
              long cj = out_mgr.N1 / 2, qj = in_mgr.N1 - out_mgr.N1 / 2;

              out_i[out_mgr.N1 / 2][half_N2] = 0.25 * in_qi[cj];
              out_i[out_mgr.N1 / 2][half_N2] += 0.25 * std::conj(in_ci[qj]);
              out_i[out_mgr.N1 / 2][half_N2] += 0.25 * (in_qi[qj]);
              out_i[out_mgr.N1 / 2][half_N2] += 0.25 * std::conj(in_ci[cj]);
            }

            for (long j = out_mgr.N1 / 2 + 1; j < out_mgr.N1; j++) {
              long cj = (out_mgr.N1 - j), qj = in_mgr.N1 - out_mgr.N1 + j;
              out_i[j][half_N2] = 0.5 * (in_qi[qj]);
              out_i[j][half_N2] += 0.5 * std::conj(in_ci[cj]);
            }
          }

          // This one is easier
          if (out_mgr.on_core(out_mgr.N0 / 2)) {
            long i = out_mgr.N0 / 2, qi = out_mgr.N0 / 2,
                 ci = in_mgr.N0 - out_mgr.N0 / 2;
            OutArrayPlane out_i = out_modes[i];
            InArrayPlane in_qi = nyqPlane[qi], in_ci = nyqPlane[ci];

            req[ci].wait();
            req[qi].wait();
            CHECK_NYQ(qi);
            CHECK_NYQ(ci);
            for (long j = 0; j < out_mgr.N1 / 2; j++) {
              long cj = (in_mgr.N1 - j) % in_mgr.N1;
              long qj = j;

              out_i[j][half_N2] = 0.25 * (in_ci[qj]);
              out_i[j][half_N2] += 0.25 * std::conj(in_qi[cj]);
              out_i[j][half_N2] += 0.25 * (in_qi[qj]);
              out_i[j][half_N2] += 0.25 * std::conj(in_ci[cj]);
            }

            {
              long cj = in_mgr.N1 - out_mgr.N1 / 2;
              long qj = out_mgr.N1 / 2;
              long j = out_mgr.N1 / 2;

              out_modes[i][j][half_N2] =
                  0.25 * (in_qi[qj].real() + in_ci[qj].real() +
                          in_qi[cj].real() + in_ci[cj].real());
            }

            for (long j = out_mgr.N1 / 2 + 1; j < out_mgr.N1; j++) {
              long cj = (out_mgr.N1 - j), qj = in_mgr.N1 - out_mgr.N1 + j;
              out_modes[i][j][half_N2] = 0.25 * (in_ci[qj]);
              out_modes[i][j][half_N2] += 0.25 * std::conj(in_qi[cj]);
              out_modes[i][j][half_N2] += 0.25 * (in_qi[qj]);
              out_modes[i][j][half_N2] += 0.25 * std::conj(in_ci[cj]);
            }
          }

          // Resume the conjugate planes. It is [N0/2+1,N0[ intersected with [startN0, startN0+localN0[.
          for (long i =
                   std::max(out_mgr.startN0, ptrdiff_t(out_mgr.N0 / 2 + 1));
               i <
               std::min(
                   out_mgr.startN0 + out_mgr.localN0, ptrdiff_t(out_mgr.N0));
               i++) {
            long ci = out_mgr.N0 - i, qi = in_mgr.N0 - out_mgr.N0 + i;
            // Problem: we need ci and qi planes.
            InArrayPlane in_qi = nyqPlane[qi], in_ci = nyqPlane[ci];
            OutArrayPlane out_i = out_modes[i];

            if (FFTW_HELPER_CHATTY)
              ctx.format("wait for ci=%d", ci);
            req[ci].wait();
            if (FFTW_HELPER_CHATTY)
              ctx.format("wait for qi=%d", qi);
            req[qi].wait();
            if (FFTW_HELPER_CHATTY)
              ctx.print("Done waiting");
            CHECK_NYQ(qi);
            CHECK_NYQ(ci);

            for (long j = 0; j < out_mgr.N1 / 2; j++) {
              long cj = (in_mgr.N1 - j) % in_mgr.N1;
              long qj = j;
              out_i[j][half_N2] = 0.5 * (in_qi[qj]);
              out_i[j][half_N2] += 0.5 * std::conj(in_ci[cj]);
            }

            {
              long cj = out_mgr.N1 / 2, qj = in_mgr.N1 - out_mgr.N1 / 2;
              typename OutArrayPlane::reference::reference node =
                  out_i[out_mgr.N1 / 2][half_N2];

              node = 0.25 * in_qi[cj];
              node += 0.25 * std::conj(in_ci[qj]);
              node += 0.25 * (in_qi[qj]);
              node += 0.25 * std::conj(in_ci[cj]);
            }

            for (long j = out_mgr.N1 / 2 + 1; j < out_mgr.N1; j++) {
              long cj = (out_mgr.N1 - j), qj = in_mgr.N1 - out_mgr.N1 + j;
              out_i[j][half_N2] = 0.5 * (in_qi[qj]);
              out_i[j][half_N2] += 0.5 * std::conj(in_ci[cj]);
            }
          }

          if (FFTW_HELPER_CHATTY)
            ctx.print("Wait for all receive operations to finish");
          comm->WaitAll(req, status);
          if (FFTW_HELPER_CHATTY)
            ctx.print("Wait for all send operations to finish");
          comm->WaitAll(req_send, status_send);
        }
      }

      template <typename self_t, typename InArray, typename OutArray>
      void degrade_real(
          self_t &in_mgr, self_t &out_mgr, const InArray &in_density,
          OutArray &out_density) {
        int rx = in_mgr.N0 / out_mgr.N0, ry = in_mgr.N1 / out_mgr.N1,
            rz = in_mgr.N2 / out_mgr.N2;

        array::fill(out_density, 0);

#pragma omp parallel for schedule(static)
        for (long ix = 0; ix < in_mgr.N0; ix++) {
          int qx = ix / rx;

          for (long iy = 0; iy < in_mgr.N1; iy++) {
            int qy = iy / ry;

            for (long iz = 0; iz < in_mgr.N2; iz++) {
              int qz = iz / rz;
              out_density[qx][qy][qz] += in_density[ix][iy][iz];
            }
          }
        }

        array::scaleArray3d(
            out_density, 1 / (double(rx) * double(ry) * double(rz)));
      }
    } // namespace details

    using details::degrade_complex;
    using details::degrade_real;
    using details::upgrade_complex;

#undef CHECK_NYQ

  } // namespace UpDeGrade
} // namespace LibLSS
#endif
