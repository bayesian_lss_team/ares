
template <typename T>
class FFTW_Manager<T, 2> : public fftw_details::FFTW_Manager_base<T, 2> {
public:
  long N0, N1, N1_HC, N1real;

  typedef fftw_details::FFTW_Manager_base<T, 2> super_t;

  typedef typename super_t::Calls Calls;
  typedef typename super_t::plan_type plan_type;

public:
  FFTW_Manager(std::shared_ptr<MPI_Communication> c, std::array<size_t, 2> d)
      : super_t(c, d) {
    setup_2d(d[0], d[1]);
  }

  FFTW_Manager(size_t pN0, size_t pN1, MPI_Communication *c)
      : super_t(c, {pN0, pN1}) {
    setup_2d(pN0, pN1);
  }

  void setup_2d(size_t pN0, size_t pN1) {
    N0 = pN0;
    N1 = pN1;

#ifdef ARES_MPI_FFTW
    this->local_size = Calls::local_size_2d(
        N0, N1, this->comm->comm(), &this->localN0, &this->startN0);
    // Local size when first two dims are swapped
    this->local_size_t = Calls::local_size_2d(
        N1, N0, this->comm->comm(), &this->localN1, &this->startN1);
#else
    this->local_size_t = this->local_size = N0 * (N1 / 2 + 1);

    this->localN0 = N0;
    this->startN0 = 0;
    this->startN1 = 0;
    this->localN1 = N1;
#endif
    this->setup();

    N1_HC = this->N_HC;
    N1real = this->N_real;
  }

  plan_type
  create_c2c_plan(std::complex<T> *in, std::complex<T> *out, int sign) {
    unsigned int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;

    return Calls::plan_dft_2d(
        N0, N1, (typename Calls::complex_type *)in,
        (typename Calls::complex_type *)out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        sign, flags);
  }

  plan_type
  create_r2c_plan(T *in, std::complex<T> *out, bool transposed_out = false) {
    ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::create_r2c_plan");
    unsigned int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;
#ifdef ARES_MPI_FFTW
    if (transposed_out)
      flags |= FFTW_MPI_TRANSPOSED_OUT;
#endif

    return Calls::plan_dft_r2c_2d(
        N0, N1, in, (typename Calls::complex_type *)out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        flags);
  }

  plan_type
  create_c2r_plan(std::complex<T> *in, T *out, bool transposed_in = false) {
    ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::create_c2r_plan");
    int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;
#ifdef ARES_MPI_FFTW
    if (transposed_in)
      flags |= FFTW_MPI_TRANSPOSED_IN;
#endif

    return Calls::plan_dft_c2r_2d(
        N0, N1, (typename Calls::complex_type *)in, out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        flags);
  }
};
