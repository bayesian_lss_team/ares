/*+
+*/

template <typename T>
class FFTW_Manager<T, 1> : public fftw_details::FFTW_Manager_base<T, 1> {
public:
  long N0, N0_HC, N0real;

  typedef fftw_details::FFTW_Manager_base<T, 1> super_t;

  typedef typename super_t::Calls Calls;
  typedef typename super_t::plan_type plan_type;

public:
  FFTW_Manager(std::shared_ptr<MPI_Communication> c, std::array<size_t, 1> d)
      : super_t(c, d) {
    setup_1d(d[0]);
  }

  FFTW_Manager(size_t pN0, MPI_Communication *c) : super_t(c, {pN0}) {
    setup_1d(pN0);
  }

  void setup_1d(size_t pN0) {
    N0 = pN0;

    this->local_size_t = this->local_size = N0;

    this->localN0 = N0;
    this->startN0 = 0;
    this->setup();

    N0_HC = this->N_HC;
    N0real = this->N_real;
  }

  plan_type
  create_c2c_plan(std::complex<T> *in, std::complex<T> *out, int sign) {
    unsigned int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;

    return Calls::plan_dft_1d(
        N0, (typename Calls::complex_type *)in,
        (typename Calls::complex_type *)out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        sign, flags);
  }

  plan_type
  create_r2c_plan(T *in, std::complex<T> *out, bool transposed_out = false) {
    ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::create_r2c_plan");
    unsigned int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;
#ifdef ARES_MPI_FFTW
    if (transposed_out)
      flags |= FFTW_MPI_TRANSPOSED_OUT;
#endif

    return Calls::plan_dft_r2c_1d(
        N0, in, (typename Calls::complex_type *)out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        flags);
  }

  plan_type
  create_c2r_plan(std::complex<T> *in, T *out, bool transposed_in = false) {
    ConsoleContext<LOG_DEBUG> ctx("FFTW_Manager::create_c2r_plan");
    int flags = FFTW_DESTROY_INPUT | FFTW_MEASURE;

    return Calls::plan_dft_c2r_1d(
        N0, (typename Calls::complex_type *)in, out,
#ifdef ARES_MPI_FFTW
        this->comm->comm(),
#endif
        flags);
  }
};

// ARES TAG: authors_num = 1
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: year(0) = 2021
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
