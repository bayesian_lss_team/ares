#include <algorithm>
#include "libLSS/tools/powerspectrum/measure.hpp"

using namespace LibLSS;

void LibLSS::PowerSpectrum::createPowerKeys(
    std::shared_ptr<MPI_Communication> comm, BoxModel const &box, int Nmodes,
    boost::multi_array_ref<int, 3> &keys,
    boost::multi_array_ref<int, 1> &key_counts,
    boost::multi_array_ref<int, 1> &nmodes,
    boost::multi_array_ref<int, 3> &adjust) {
  std::array<size_t, 3> N;
  std::array<double, 3> L;
  auto s = [](double x) { return x * x; };

  box.fill(N);
  FFTW_Manager<double, 3> mgr(comm, N);
  box.get_L(L);

  double k_min = 2 * M_PI / std::max({L[0], L[1], L[2]});
  double k_max = 2 * M_PI * 1.1 *
                 std::sqrt(s(N[0] / L[0]) + s(N[1] / L[1]) + s(N[2] / L[2]));

  init_helpers::initialize_powerspectrum_keys(
      mgr, keys, key_counts, adjust, nmodes, L, k_min, k_max, Nmodes);
}
