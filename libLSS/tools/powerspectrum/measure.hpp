/*+
    ARES/HADES/BORG Package -- ./libLSS/tools/powerspectrum/measure.hpp
    Copyright (C) 2014-2022 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2022 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Florent Leclercq <florent.leclercq@polytechnique.org> (2021)
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018-2021)
       Jens Jasche <j.jasche@tum.de> (2015)

+*/
#ifndef __LIBLSS_POWERSPECTRUM_MEASURE_HPP
#define __LIBLSS_POWERSPECTRUM_MEASURE_HPP

#include "libLSS/mpi/generic_mpi.hpp"
#include <memory>
#include <CosmoTool/algo.hpp>
#include <CosmoTool/hdf5_array.hpp>
#include "libLSS/samplers/core/types_samplers.hpp"
#include "libLSS/samplers/core/powerspec_tools.hpp"
#include "libLSS/physics/model_io/box.hpp"

namespace LibLSS {

  namespace PowerSpectrum {

    void createPowerKeys(
        std::shared_ptr<MPI_Communication> comm, BoxModel const &box,
        int Nmodes, boost::multi_array_ref<int, 3> &keys,
        boost::multi_array_ref<int, 1> &keycounts,
        boost::multi_array_ref<int, 1> &nmodes,
        boost::multi_array_ref<int, 3> &adjust);

    template <typename CArray>
    void computePower(
        ArrayType1d::ArrayType &power, const CArray &density,
        const IArrayType::RefArrayType &keys,
        const IArrayType::RefArrayType &adjust,
        const IArrayType1d::RefArrayType &nmode, double volume,
        bool clear = true) {
      using CosmoTool::square;
      int begin0 = density.index_bases()[0];
      int begin1 = density.index_bases()[1];
      int begin2 = density.index_bases()[2];
      int end0 = density.index_bases()[0] + density.shape()[0];
      int end1 = density.index_bases()[1] + density.shape()[1];
      int end2 = density.index_bases()[2] + density.shape()[2];

      if (clear)
        std::fill(power.begin(), power.end(), 0);
      for (int i = begin0; i < end0; i++) {
        for (int j = begin1; j < end1; j++) {
          for (int k = begin2; k < end2; k++) {
            double P = square(density[i][j][k].real()) +
                       square(density[i][j][k].imag());

            power[keys[i][j][k]] += P * adjust[i][j][k];
          }
        }
      }

      for (int i = 0; i < power.num_elements(); i++)
        if (nmode[i] != 0)
          power[i] /= (volume * nmode[i]);
    }

    template <typename CArray>
    void computePower(
        ArrayType1d::ArrayType &power, const CArray &density,
        const IArrayType &a_keys, const IArrayType &a_adjust,
        const IArrayType1d &a_nmode, double volume, bool clear = true) {
      computePower(
          power, density, *a_keys.array, *a_adjust.array, *a_nmode.array,
          volume, clear);
    }

    template <typename F, typename CArray>
    void savePower(
        F &f, const std::string &n, const CArray &density,
        const IArrayType &a_keys, const IArrayType &a_adjust,
        const IArrayType1d &a_nmode, double volume) {
      ArrayType1d::ArrayType P(boost::extents[a_nmode.array->num_elements()]);

      computePower(P, density, a_keys, a_adjust, a_nmode, volume);
      CosmoTool::hdf5_write_array(f, n, P);
    }
  } // namespace PowerSpectrum

}; // namespace LibLSS

#endif
