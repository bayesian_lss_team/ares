/*+
    ARES/HADES/BORG Package -- ./libLSS/tools/itertools.hpp
    Copyright (C) 2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2019-2020)

+*/
#ifndef __LIBLSS_ITERTOOLS_HPP
#  define __LIBLSS_ITERTOOLS_HPP

#  include <boost/fusion/adapted/std_tuple.hpp>
#  include <boost/iterator/counting_iterator.hpp>
#  include <boost/iterator/zip_iterator.hpp>

namespace LibLSS {
  namespace itertools {

    template <typename I>
    auto enumerate_base(size_t i, I j) {
      return boost::make_zip_iterator(
          std::make_tuple(boost::counting_iterator<size_t>(i), j));
    }

    template <typename T>
    struct Enumerate {
    public:
      T const &t;

      Enumerate(T const &t_) : t(t_) {}

      auto begin() const { return enumerate_base(0, t.begin()); }
      auto end() const { return enumerate_base(t.size(), t.end()); }
    };

    struct Range {
      size_t i0, i1;

      Range(size_t i0_, size_t i1_) : i0(i0_), i1(i1_) {}
      auto begin() const { return boost::counting_iterator<size_t>(i0); }
      auto end() const { return boost::counting_iterator<size_t>(i1); }
    };

    /**
     * This function creates a pseudo container (range container) over
     * which one can iterate upon.
     * A typical use is:
     * <code>
     *   for (size_t id: range(0, N)) { blabla; }
     * </code>
     *
     * @param i0 start of the range
     * @param i1 end of the range
     * @return a container
     */
    inline Range range(size_t i0, size_t i1) { return Range(i0, i1); }

    /**
     * This function creates a pseudo container made of a zip iterator binding
     * a range and an iterator over the provided container.
     * A typical use is:
     * <code>
     *    for (auto x: enumerate(some_vector)) {
     *       size_t id = x.get<0>();
     *       // Some stuff
     *       the_type_in_vector const& v = x.get<1>();
     *    }
     * </code>
     *
     * @param t a container
     * @return a container with an enumeration
     */
    template <typename T>
    inline Enumerate<T> enumerate(T const &t) {
      return Enumerate<T>(t);
    }

    template <
        template <typename> class Container, typename Container_input,
        typename Functor>
    static auto transform_container(Container_input const &input, Functor f) {
      typedef decltype(f(
          std::declval<typename Container_input::value_type>())) U;
      Container<U> ret;

      std::transform(
          std::begin(input), std::end(input), std::back_inserter(ret), f);
      return ret;
    }

  } // namespace itertools
} // namespace LibLSS
#endif

// ARES TAG: authors_num = 1
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
// ARES TAG: year(0) = 2019
