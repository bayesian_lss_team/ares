/*+
    ARES/HADES/BORG Package -- libLSS/physics/class_cosmo.hpp
    Copyright (C) 2020 Jens Jasche <jens.jasche@fysik.su.se>
    Copyright (C) 2021 Guilhem Lavaux <guilhem.lavaux@iap.fr>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2021)

+*/
#ifndef __LIBLSS_CLASS_COSMO_HPP
#  define __LIBLSS_CLASS_COSMO_HPP

#  include <map>
#  include <string>
#  include <memory>
#  include "libLSS/physics/cosmo_params.hpp"
#  include "libLSS/tools/auto_interpolator.hpp"

namespace LibLSS {

  struct OpaqueClass;

  class ClassCosmo {
  private:
    std::unique_ptr<OpaqueClass> opaque;
    typedef boost::multi_array_ref<double, 1> array_ref_1d;
    typedef boost::multi_array<double, 1> array_1d;

    size_t numInterpolationPoints;
    CosmologicalParameters current_params;
    double log_k_min, log_k_max, delta_ln_k;
    double sigma_8;

  public:
    enum TransferType { TotalMatter, Cold, Baryon, Neutrino };
    typedef std::map<std::string, double> DictCosmology;

    ClassCosmo(
        CosmologicalParameters const &params, unsigned int k_per_decade,
        double k_max); // This is the constructor
    ~ClassCosmo();

    void setInterpolation(size_t numPoints);

    double primordial_Pk(double k);
    void updateCosmo();
    double get_Tk(double k, TransferType type = TotalMatter);
    void retrieve_Tk(double z);
    void computeSigma8();
    DictCosmology getCosmology();

    const CosmologicalParameters &getParameters() const {
      return current_params;
    }

    void queryGrowthFunction(double z, double &D, double &Dprime);

  protected:
    void reinterpolate(
        array_ref_1d const &k, array_ref_1d const &Tk,
        LibLSS::auto_interpolator<double> &);
  };

} // namespace LibLSS

#endif

// ARES TAG: authors_num = 2
// ARES TAG: name(0) = Jens Jasche
// ARES TAG: email(0) = jens.jasche@fysik.su.se
// ARES TAG: year(0) = 2020
// ARES TAG: name(1) = Guilhem Lavaux
// ARES TAG: email(1) = guilhem.lavaux@iap.fr
// ARES TAG: year(1) = 2021
