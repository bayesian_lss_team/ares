/*+
    ARES/HADES/BORG Package -- ./libLSS/physics/class_cosmo.cpp
    Copyright (C) 2020 Jens Jasche <jens.jasche@fysik.su.se>
    Copyright (C) 2021 Guilhem Lavaux <guilhem.lavaux@iap.fr>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2021)

+*/
#include <boost/numeric/conversion/cast.hpp>
#include <iostream>
#include <fstream>
#include <locale.h>
#include <boost/algorithm/string/trim.hpp>
#include "libLSS/tools/console.hpp"
#include "class_cosmo.hpp"
#include <class_code/class.h>
#include "libLSS/tools/errors.hpp"
#include "libLSS/tools/string_tools.hpp"
#include "libLSS/tools/auto_interpolator.hpp"
#include "libLSS/tools/log_traits.hpp"

using namespace LibLSS;
using namespace std;

namespace LibLSS {
  struct OpaqueClass {
    struct precision pr;  // for precision parameters
    struct background ba; // for cosmological background
    struct thermo th;     // for thermodynamics
    struct perturbs pt;   // for source functions
    struct transfers tr;  // for transfer functions
    struct primordial pm; // for primordial spectra
    struct spectra sp;    // for output spectra
    struct nonlinear nl;  // for non-linear spectra
    struct lensing le;    // for lensed spectra
    struct output op;     // for output files
    ErrorMsg errmsg;      // for error messages

    std::shared_ptr<file_content> fc;

    bool bg_init, th_init, pt_init, prim_init;

    OpaqueClass() {
      bg_init = false;
      th_init = false;
      pt_init = false;
      prim_init = false;
      ba.N_ncdm = 0;
    }

    ~OpaqueClass() {
      if (bg_init) {
        background_free(&ba);
      }
      if (th_init)
        thermodynamics_free(&th);
      if (pt_init)
        perturb_free(&pt);
      if (prim_init)
        primordial_free(&pm);
    }

    LibLSS::auto_interpolator<double> interpolate_mTk, interpolate_mTnu,
        interpolate_mTc, interpolate_mTb;
  };
} // namespace LibLSS

ClassCosmo::ClassCosmo(
    CosmologicalParameters const &cosmo, unsigned int k_per_decade,
    double k_max)
    : current_params(cosmo), sigma_8(-1.0) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);

  numInterpolationPoints = 1024;
  opaque = std::make_unique<OpaqueClass>();

  std::string previous_locale = std::string(setlocale(LC_NUMERIC, 0));
  // CLASS is not safe w.r.t Locale settings. It reads table with sscanf which
  // is sensitive to the locale setup.
  setlocale(LC_NUMERIC, "C");

  try {
    // Set all class values to default
    if (input_default_params(
            &opaque->ba, &opaque->th, &opaque->pt, &opaque->tr, &opaque->pm,
            &opaque->sp, &opaque->nl, &opaque->le, &opaque->op) == _FAILURE_) {
      ctx.format2<LOG_ERROR>(
          "Error running input_default_params => %s", opaque->op.error_message);
      error_helper<ErrorBadState>("Error in CLASS");
    }

    opaque->fc =
        std::shared_ptr<file_content>(new file_content, [](file_content *fc) {
          if (fc->size > 0) {
            delete[] fc->name;
            delete[] fc->value;
            delete[] fc->read;
            delete[] fc->filename;
          }
        });
    opaque->fc->filename = new char[30];
    strcpy(opaque->fc->filename, "NOFILE");
    opaque->fc->size = 0;

    std::vector<std::tuple<std::string, std::string>> params;

    auto inject_argument = [&](std::string const &n, auto x) {
      std::string s = LibLSS::to_string(x);
      params.push_back(std::make_tuple(n, s));
    };

    inject_argument("Omega_cdm", cosmo.omega_m - cosmo.omega_b);
    inject_argument("Omega_b", cosmo.omega_b);
    inject_argument("N_ncdm", cosmo.sum_mnu == 0. ? 0 : 1);
    if (cosmo.sum_mnu > 0)
      inject_argument("m_ncdm", cosmo.sum_mnu);
    inject_argument("n_s", cosmo.n_s);
    inject_argument("h", cosmo.h);
    inject_argument("output", "mTk");
    inject_argument("z_max_pk", 50);
    inject_argument("w0_fld", cosmo.w);
    inject_argument("wa_fld", cosmo.wprime);
    inject_argument("Omega_Lambda", 0);

    opaque->fc->name = new FileArg[params.size()];
    opaque->fc->value = new FileArg[params.size()];
    opaque->fc->read = new short[params.size()];
    opaque->fc->size = boost::numeric_cast<int>(params.size());

    {
      for (size_t i = 0; i < params.size(); i++) {
        strncpy(
            opaque->fc->name[i], std::get<0>(params[i]).data(),
            sizeof(FileArg));
        strncpy(
            opaque->fc->value[i], std::get<1>(params[i]).data(),
            sizeof(FileArg));
        ctx.format(
            "CLASS param: %s = %s", opaque->fc->name[i], opaque->fc->value[i]);
        opaque->fc->read[i] = _FALSE_;
      }
    }

    if (input_init(
            opaque->fc.get(), &opaque->pr, &opaque->ba, &opaque->th,
            &opaque->pt, &opaque->tr, &opaque->pm, &opaque->sp, &opaque->nl,
            &opaque->le, &opaque->op, opaque->errmsg) == _FAILURE_) {
      ctx.format2<LOG_ERROR>(
          "Error while reading arguments => %s", opaque->pr.error_message);
      error_helper<ErrorBadState>("Error in CLASS");
    }
    opaque->pr.k_per_decade_for_pk = k_per_decade;

    //initialize background calculations
    if (background_init(&opaque->pr, &opaque->ba) == _FAILURE_) {
      ctx.format2<LOG_ERROR>(
          "Error running background_init => %s", opaque->ba.error_message);
      error_helper<ErrorBadState>("Error in CLASS");
    }
    opaque->bg_init = true;

    //opaque->th.thermodynamics_verbose = _TRUE_;
    if (thermodynamics_init(&opaque->pr, &opaque->ba, &opaque->th) ==
        _FAILURE_) {
      ctx.format2<LOG_ERROR>(
          "Error running thermodynamics_init => %s", opaque->th.error_message);
      error_helper<ErrorBadState>("Error in CLASS");
    }
    opaque->th_init = true;

    opaque->pt.has_perturbations = _TRUE_;
    //opaque->pt.perturbations_verbose = 1;
    opaque->pt.has_pk_matter = _TRUE_;
    opaque->pt.has_density_transfers = _TRUE_;
    opaque->pt.has_cls = _FALSE_;
    opaque->pt.k_max_for_pk = k_max;

    if (perturb_init(&opaque->pr, &opaque->ba, &opaque->th, &opaque->pt) ==
        _FAILURE_) {
      ctx.format2<LOG_ERROR>(
          "Error running perturb_init => %s", opaque->pt.error_message);
      error_helper<ErrorBadState>("Error in CLASS");
    }
    opaque->pt_init = true;

    if (primordial_init(&opaque->pr, &opaque->pt, &opaque->pm) == _FAILURE_) {
      ctx.format2<LOG_ERROR>(
          "Error running primordial_init => %s", opaque->pm.error_message);
      error_helper<ErrorBadState>("Error in CLASS");
    }
    opaque->prim_init = true;

    retrieve_Tk(0);
  } catch (std::exception &e) {
    setlocale(LC_NUMERIC, previous_locale.c_str());
    throw;
  }
  setlocale(LC_NUMERIC, previous_locale.c_str());

  current_params.omega_r = opaque->ba.Omega0_r;
  current_params.omega_m = opaque->ba.Omega0_m;
  current_params.omega_q = opaque->ba.Omega0_fld;
  current_params.omega_k = opaque->ba.Omega0_k;
}

double ClassCosmo::primordial_Pk(double k) {
  //Input: wavenumber k in 1/Mpc (linear mode)
  //Output: primordial spectra P(k) in \f$Mpc^3\f$ (linear mode)

  double output;

  primordial_spectrum_at_k(
      &opaque->pm,
      0, //choose scalar mode
      linear, k, &output);

  return output;
}

double ClassCosmo::get_Tk(double k, TransferType type) {
  LibLSS::auto_interpolator<double> *interpolator;

  switch (type) {
  case TotalMatter:
    interpolator = &opaque->interpolate_mTk;
    break;
  case Neutrino:
    if (current_params.sum_mnu == 0.)
      return 0;
    interpolator = &opaque->interpolate_mTnu;
    break;
  case Cold:
    interpolator = &opaque->interpolate_mTc;
    break;
  case Baryon:
    interpolator = &opaque->interpolate_mTb;
    break;
  default:
    error_helper<ErrorBadState>("Unknown transfer function");
  }
  return -std::exp((*interpolator)(std::log(k)));
}

void ClassCosmo::retrieve_Tk(double const output_redshift) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  ctx.format("Obtaining transfer functions for redshift %g", output_redshift);

  char *c_titles;
  std::string titles;

  // Query the available columns
  c_titles = new char[_MAXTITLESTRINGLENGTH_];
  std::fill(c_titles, c_titles + _MAXTITLESTRINGLENGTH_, 0);
  if (perturb_output_titles(&opaque->ba, &opaque->pt, class_format, c_titles) ==
      _FAILURE_) {
    delete[] c_titles;
    ctx.format2<LOG_ERROR>(
        "Error running perturb_output_titles => %s", opaque->pt.error_message);
    error_helper<ErrorBadState>("Error in CLASS");
  }
  titles = c_titles;
  delete[] c_titles;

  // Retrieve relevant data
  auto names = LibLSS::tokenize(boost::algorithm::trim_copy(titles), "\t");
  ctx.print(LibLSS::to_string(names));
  auto index_md = opaque->pt.index_md_scalars;
  auto number_of_titles = boost::numeric_cast<size_t>(names.size());
  // UNUSED
  //auto number_of_ic = opaque->pt.ic_size[index_md];
  auto timesteps = boost::numeric_cast<size_t>(opaque->pt.k_size[index_md]);
  auto size_ic_data = timesteps * number_of_titles;
  auto ic_num = opaque->pt.ic_size[index_md];

  auto data = new double[size_ic_data * ic_num];

  if (perturb_output_data(
          &opaque->ba, &opaque->pt, class_format, output_redshift,
          boost::numeric_cast<int>(number_of_titles), data) == _FAILURE_) {
    delete[] data;
    ctx.format2<LOG_ERROR>(
        "Error running perturb_output_data => %s", opaque->pt.error_message);
    error_helper<ErrorBadState>("Error in CLASS");
  }

  // Adiabatic mode is referenced at opaque->pt.index_ic_ad
  auto index_ic = opaque->pt.index_ic_ad;
  auto result_k = std::find(names.begin(), names.end(), "k (h/Mpc)");
  Console::instance().c_assert(
      result_k != names.end(), "Invalid returned arrays for k from CLASS");
  auto k_title = std::distance(names.begin(), result_k);

  auto get_data = [&](size_t step, size_t title) {
    return data[index_ic * size_ic_data + step * number_of_titles + title];
  };

  array_1d k, Ttot, Tnu, Tb, Tc;

  k.resize(boost::extents[timesteps]);

  auto make_interpolation = [&](std::string const &n, array_1d &T) {
    ctx.format("Extracting %s", n);
    auto result = std::find(names.begin(), names.end(), n);
    if (result == names.end()) {
      delete[] data;
      error_helper<ErrorBadState>("Invalid returned arrays from CLASS");
    }
    auto title = std::distance(names.begin(), result);
    T.resize(boost::extents[timesteps]);
    for (size_t step = 0; step < timesteps; step++) {
      T[step] = -get_data(step, title);
    }
  };

  for (size_t step = 0; step < timesteps; step++) {
    k[step] = get_data(step, k_title);
  }

  make_interpolation("d_tot", Ttot);
  if (current_params.sum_mnu > 0.) {
    make_interpolation("d_ncdm[0]", Tnu);
    reinterpolate(k, Tnu, opaque->interpolate_mTnu);
  }
  make_interpolation("d_b", Tb);
  make_interpolation("d_cdm", Tc);
  reinterpolate(k, Ttot, opaque->interpolate_mTk);
  reinterpolate(k, Tc, opaque->interpolate_mTc);
  reinterpolate(k, Tb, opaque->interpolate_mTb);

  delete[] data;
}

ClassCosmo::~ClassCosmo() {}

void ClassCosmo::reinterpolate(
    array_ref_1d const &k, array_ref_1d const &Tk,
    LibLSS::auto_interpolator<double> &interpolator) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);

  double k_min = opaque->pt.k_min / opaque->ba.h;
  double k_max = opaque->pt.k_max / opaque->ba.h;

  delta_ln_k = std::log(k_max / k_min) /
               boost::numeric_cast<double>(numInterpolationPoints + 1);
  log_k_min = std::log(k_min);
  log_k_max = std::log(k_max);
  size_t i_in_k = 0;

  auto newTk =
      new boost::multi_array<double, 1>(boost::extents[numInterpolationPoints]);

  ctx.format(
      "numInterpolationPoints = %d, k.size() = %d, k_min=%g, k_max=%g",
      numInterpolationPoints, k.size(), k_min, k_max);
  for (size_t i = 0; i < numInterpolationPoints; i++) {
    double target_k = std::exp(delta_ln_k * double(i) + log_k_min);
    while (k[i_in_k] < target_k && i_in_k < k.size()) {
      i_in_k++;
    }

    Console::instance().c_assert(i_in_k < k.size(), "Bad reinterpolation");
    if (i_in_k == 0 && k[i_in_k] == k_min) {
      (*newTk)[i] = std::log(Tk[0]);
    } else if (k[i_in_k - 1] == 0) {
      (*newTk)[i] =
          std::log(Tk[i_in_k]) / std::log(k[i_in_k]) * std::log(target_k);
    } else {
      double alpha = std::log(target_k / k[i_in_k - 1]) /
                     std::log(k[i_in_k] / k[i_in_k - 1]);
      Console::instance().c_assert(
          alpha > 0 && alpha < 1, "Bad alpha for interpolation");
      (*newTk)[i] =
          std::log(Tk[i_in_k - 1]) * (1 - alpha) + std::log(Tk[i_in_k]) * alpha;
    }
  }

  interpolator = LibLSS::auto_interpolator<double>(
      log_k_min, log_k_max, delta_ln_k, std::log(Tk[0]), 0.0, newTk);
  interpolator.setThrowOnOverflow();
}

void ClassCosmo::updateCosmo() {}

void ClassCosmo::computeSigma8() {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);

  auto W = [](double xx) {
    if (xx < 0.01)
      return 1 - xx * xx / 10;
    else
      return (3 / (xx * xx * xx) * (sin(xx) - xx * cos(xx)));
  };

  // UNUSED
  //double om = opaque->ba.Omega0_m;
  double oc = opaque->ba.Omega0_cdm;
  double ob = opaque->ba.Omega0_b;
  double onu = (current_params.sum_mnu > 0.) ? opaque->ba.Omega0_ncdm[0] : 0;
  double fc = oc / (oc + ob + onu);
  double fb = ob / (oc + ob + onu);
  double fnu = onu / (oc + ob + onu);
  double k_pivot =
      0.05 / current_params.h; // pivot is 0.05 Mpc^{-1}, convert to h/Mpc
  // UNUSED
  //double h3 = current_params.h * current_params.h * current_params.h;

  auto P_k = [&](double k) {
    double T_cdm = get_Tk(k, TransferType::Cold);
    double T_baryon = get_Tk(k, TransferType::Baryon);
    double T_nu = get_Tk(k, TransferType::Neutrino);

    double T_tot = fc * T_cdm + fb * T_baryon + fnu * T_nu;
    double Scale = (current_params.n_s != 1)
                       ? std::pow(k / k_pivot, current_params.n_s - 1)
                       : 1;

    if (std::isnan(T_tot)) {
      ctx.format("T_cdm=%g, T_baryon=%g, T_nu=%g", T_cdm, T_baryon, T_nu);
      error_helper<ErrorBadState>("Nan in transfer function");
    }
    return current_params.A_s * 2 * M_PI * M_PI / (k * k * k) * Scale * T_tot *
           T_tot;
  };

  auto integrand = [&](double k) {
    double w = W(k * 8.0);

    double ret = P_k(k) * k * k * k / (2 * M_PI * M_PI) * w * w;
    if (std::isnan(ret)) {
      ctx.format("k=%g, P_k(k)=%g, w=%g", k, P_k(k), w);
      ctx.format(
          "A_s = %g, fc=%g, fb=%g, fnu=%g", current_params.A_s, fc, fb, fnu);
      error_helper<ErrorBadState>("Nan in integrand");
    }
    return ret;
  };

  double I = 0;
  double log_k = log_k_min;
  try {
    I += 0.5 * delta_ln_k * integrand(std::exp(log_k));
    log_k += delta_ln_k;
    if (std::isnan(I)) {
      ctx.format("Nan in I at k=%g", std::exp(log_k));
      return;
    }
    while (log_k < log_k_max - 3 * delta_ln_k) {
      I += delta_ln_k * integrand(std::exp(log_k));
      if (std::isnan(I)) {
        ctx.format("Nan in I at k=%g", std::exp(log_k));
        return;
      }
      log_k += delta_ln_k;
    }
    I += 0.5 * delta_ln_k * integrand(std::exp(log_k));
  } catch (ErrorParams const &) {
    ctx.format(
        "Overflow at k=%g (log_k_min=%g, log_k_max=%g, delta_ln_k=%g)",
        std::exp(log_k), log_k_min, log_k_max, delta_ln_k);
    throw;
  }
  sigma_8 = std::sqrt(I);
}

ClassCosmo::DictCosmology ClassCosmo::getCosmology() {

  DictCosmology state;

  if (sigma_8 > 0.0)
    state["sigma_8"] = sigma_8;

  state["Omega_g"] = opaque->ba.Omega0_g;
  state["Omega_m"] = opaque->ba.Omega0_m;
  state["N_ncdm"] = opaque->ba.N_ncdm;
  if (current_params.sum_mnu > 0.)
    state[lssfmt::format("Omega0_ncdm_%d", 0)] = opaque->ba.Omega0_ncdm[0];
  state["Omega_k"] = opaque->ba.Omega0_k;
  state["Omega_lambda"] = opaque->ba.Omega0_lambda;
  state["Omega_m"] = opaque->ba.Omega0_m;
  state["Omega_ncdm_tot"] = opaque->ba.Omega0_ncdm_tot;
  state["Omega_b"] = opaque->ba.Omega0_b;
  state["Omega_c"] = opaque->ba.Omega0_cdm;
  state["Omega_q"] = opaque->ba.Omega0_fld;

  return state;
}

void ClassCosmo::setInterpolation(size_t numPoints) {
  numInterpolationPoints = numPoints;
  retrieve_Tk(0);
}

void ClassCosmo::queryGrowthFunction(double z, double &D, double &D_prime) {
  LIBLSS_AUTO_DEBUG_CONTEXT(ctx);
  std::vector<double> vecback(opaque->ba.bg_size);
  double tau_growth;
  int last_index;
  double a = 1 / (1 + z);

  if (background_tau_of_z(&opaque->ba, z, &tau_growth) == _FAILURE_) {
    ctx.format2<LOG_ERROR>(
        "Error running background_tau_of_z => %s", opaque->ba.error_message);
    error_helper<ErrorBadState>("Error in CLASS");
  }

  if (background_at_tau(
          &opaque->ba, tau_growth, opaque->ba.long_info,
          opaque->ba.inter_normal, &last_index, vecback.data()) == _FAILURE_) {
    ctx.format2<LOG_ERROR>(
        "Error running background_tau => %s", opaque->ba.error_message);
    error_helper<ErrorBadState>("Error in CLASS");
  }

  D = vecback[opaque->ba.index_bg_D];
  D_prime = vecback[opaque->ba.index_bg_f] * D / a;
}

// ARES TAG: num_authors = 2
// ARES TAG: name(0) = Jens Jasche
// ARES TAG: email(0) = jens.jasche@fysik.su.se
// ARES TAG: year(0) = 2020
// ARES TAG: name(1) = Guilhem Lavaux
// ARES TAG: email(1) = guilhem.lavaux@iap.fr
// ARES TAG: year(1) = 2021
