/*+
    ARES/HADES/BORG Package -- libLSS/physics/cosmo_params.hpp
    Copyright (C) 2012-2021 Jens Jasche <jens.jasche@fysik.su.se>
    Copyright (C) 2016-2021 Guilhem Lavaux <guilhem.lavaux@iap.fr>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2021)

+*/
#pragma once
#ifndef __LIBLSS_COSMO_PARAMS_HPP
#  define __LIBLSS_COSMO_PARAMS_HPP

#  include <CosmoTool/hdf5_array.hpp>

namespace LibLSS {

  struct CosmologicalParameters {

    double omega_r; /* negligible radiation density */
    double omega_k; /* curvature - flat prior for everything! */
    double omega_m;
    double omega_b;
    double omega_q;
    double w;
    double n_s;
    double fnl; /* non-linearity parameter, for primordial non-Gaussianity */
    double wprime;
    double sigma8;
    double A_s;
    double rsmooth;
    double h;
    double beta;
    double z0;
    double a0;      /* scale factor at epoch of observation usually 1*/
    double sum_mnu; /* sum of neutrino masses */

    CosmologicalParameters()
        : omega_r(0), omega_k(0), omega_m(0), omega_b(0), omega_q(0), w(0),
          n_s(0), fnl(0), wprime(0), sigma8(0), A_s(0), h(0), beta(0), z0(0),
          a0(0), sum_mnu(0) {}

    bool operator==(CosmologicalParameters const &p2) const {
      return omega_r == p2.omega_r && omega_k == p2.omega_k &&
             omega_m == p2.omega_m && omega_b == p2.omega_b &&
             omega_q == p2.omega_q && w == p2.w && n_s == p2.n_s &&
             wprime == p2.wprime && sigma8 == p2.sigma8 && A_s == p2.A_s &&
             h == p2.h && sum_mnu == p2.sum_mnu;
    }
    bool operator!=(CosmologicalParameters const &p2) const {
      return !operator==(p2);
    }

    void check_differences(CosmologicalParameters const &p2);
  };
} // namespace LibLSS

// clang-format off
CTOOL_STRUCT_TYPE(LibLSS::CosmologicalParameters,
                  HDF5T_CosmologicalParameters,
    ((double, omega_r))
    ((double, omega_k))
    ((double, omega_m))
    ((double, omega_b))
    ((double, omega_q))
    ((double, w))
    ((double, n_s))
    ((double, fnl))
    ((double, wprime))
    ((double, sigma8))
    ((double, rsmooth))
    ((double, h))
    ((double, beta))
    ((double, z0))
    ((double, a0))
    ((double, A_s))
    ((double, sum_mnu))
);
// clang-format on

#endif
// ARES TAG: authors_num = 2
// ARES TAG: name(0) = Jens Jasche
// ARES TAG: year(0) = 2012-2021
// ARES TAG: email(0) = jens.jasche@fysik.su.se
// ARES TAG: name(1) = Guilhem Lavaux
// ARES TAG: year(1) = 2016-2021
// ARES TAG: email(1) = guilhem.lavaux@iap.fr
