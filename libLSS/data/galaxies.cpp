
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/data/galaxies.hpp"

LIBLSS_MPI_STRUCT_TYPE(LibLSS::BaseGalaxyDescriptor, LIBLSS_GALAXY_ELEMENT);

MPI_FORCE_COMPOUND_TYPE_NO_INLINE(LibLSS::BaseGalaxyDescriptor);