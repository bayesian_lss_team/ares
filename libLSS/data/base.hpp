/*+
    ARES/HADES/BORG Package -- ./libLSS/data/base.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015, 2018, 2020)

+*/
#ifndef __ARES2_BASE_HPP
#define __ARES2_BASE_HPP

namespace LibLSS {

  class Base_Data {
  public:
  };

}; // namespace LibLSS

#endif
