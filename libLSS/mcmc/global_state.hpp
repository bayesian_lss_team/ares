/*+
    ARES/HADES/BORG Package -- ./libLSS/mcmc/global_state.hpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Franz Elsner <felsner@nca-08.MPA-Garching.MPG.DE> (2017)
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2014-2020)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

+*/
#pragma once
#ifndef _LIBLSS_GLOBAL_STATE_HPP
#  define _LIBLSS_GLOBAL_STATE_HPP

#  include <boost/optional.hpp>
#  include <boost/type_traits/is_base_of.hpp>
#  include <boost/format.hpp>
#  include <functional>
#  include <set>
#  include <typeindex>
#  include "libLSS/mpi/generic_mpi.hpp"
#  include "libLSS/tools/console.hpp"
#  include "libLSS/tools/string_tools.hpp"
#  include "libLSS/mcmc/state_element.hpp"

namespace LibLSS {

  /**
   * @brief This is the class that manages the dictionnary that is saved in each MCMC/Restart file.
   *
   * It is *not* copy-constructible.
   */
  class MarkovState {
  public:
    typedef std::map<std::string, bool> SaveMap;
    typedef std::map<std::string, StateElement *> StateMap;
    typedef std::map<std::string, std::type_index> TypeMap;
    typedef std::set<std::string> Requirements;

  private:
    SaveMap save_map;
    StateMap state_map, toProcess;
    TypeMap type_map;
    std::list<std::tuple<Requirements, std::function<void()>>> postLoad;
    std::set<std::string> loaded;

  public:
    MarkovState(MarkovState const &) = delete;

    MarkovState(MarkovState &&other);

    MarkovState &operator=(MarkovState &&other);

    /**
     * @brief Construct a new empty Markov State object.
     *
     */
    MarkovState() {}

    /**
     * @brief Destroy the Markov State object.
     *
     * All the elements stored in the dictionnary will be destroyed, as the ownership
     * is given the dictionnary implicitly when the element is added to it.
     */
    ~MarkovState();

    template <typename T>
    static void check_class() {
      static_assert(
          boost::is_base_of<StateElement, T>::value, "T is not a StateElement");
    }

    /**
     * @brief Function to access by its name an element stored in the dictionnary.
     *
     * This function makes a lookup and a dynamic cast to the specified template "StateElement".
     * It tries to find the indicated state element by name. If it fails an error is thrown.
     * A dynamic cast is then issued to ensure that the stored type is the same as the requested one.
     *
     * @tparam T     type of the element, cast will be checked
     * @param name   string id of the element
     * @return T*    pointer to the element
     */
    template <typename T>
    T *get(const std::string &name) {
      check_class<T>();
      StateMap::iterator i = state_map.find(name);
      if (i == state_map.end() || i->second == 0) {
        error_helper_fmt<ErrorBadState>("Invalid access to %s", name);
      }
      T *ptr = dynamic_cast<T *>(i->second);
      if (ptr == 0) {
        error_helper_fmt<ErrorBadCast>("Bad cast in access to %s", name);
      }
      return ptr;
    }

    /**
     * @brief Access using a boost::format object.
     *
     * @tparam T
     * @param f
     * @return T*
     */
    template <typename T>
    T *get(const boost::format &f) {
      return get<T>(f.str());
    }

    template <typename T, typename... Args>
    T *formatGet(std::string const &s, Args &&...args) {
      return get<T>(lssfmt::format(s, args...));
    }

    template <typename T>
    const T *get(const boost::format &f) const {
      return get<T>(f.str());
    }

    template <typename T>
    const T *get(const std::string &name) const {
      check_class<T>();
      StateMap::const_iterator i = state_map.find(name);
      if (i == state_map.end() || i->second == 0) {
        error_helper_fmt<ErrorBadState>("Invalid access to %s", name);
      }

      const T *ptr = dynamic_cast<const T *>(i->second);
      if (ptr == 0) {
        error_helper_fmt<ErrorBadCast>("Bad cast in access to %s", name);
      }
      return ptr;
    }

    /**
     * @brief Check existence of an element in the dictionnary.
     *
     * @param name   string id of the element
     * @return true  if it exists
     * @return false if it does not exist
     */
    bool exists(const std::string &name) const {
      return state_map.find(name) != state_map.end();
    }

    /**
     * @brief Access an element through operator [] overload.
     *
     * @param name
     * @return StateElement&
     */
    StateElement &operator[](const std::string &name) {
      return *get<StateElement>(name);
    }

    const StateElement &operator[](const std::string &name) const {
      return *get<StateElement>(name);
    }

    std::type_index getStoredType(const std::string &name) const;

    /**
     * @brief Add an element in the dictionnary.
     *
     * @param name  string id of the new element
     * @param elt   Object to add in the dictionnary. The ownership is transferred to MarkovState.
     * @param write_to_snapshot indicate, if true, that the element has to be written in mcmc files
     * @return StateElement* the same object as "elt", used to daisy chain calls.
     */
    template <typename T>
    T *newElement(
        const std::string &name, T *elt,
        const bool &write_to_snapshot = false) {
      static_assert(
          std::is_base_of<StateElement, T>::value,
          "newElement accepts only StateElement based objects");
      state_map[name] = elt;
      type_map.insert(std::pair<std::string, std::type_index>(
          name, std::type_index(typeid(T))));
      toProcess[name] = elt;
      elt->name = name;
      elt->added();
      set_save_in_snapshot(name, write_to_snapshot);
      return elt;
    }

    /**
     * @brief Alias an element from another MarkovState into present one.
     *
     * @param name name of the element to alias
     * @param other MarkovState that holds the other element
     * @param duplicate if true we make a fully aliasing element, otherwise the data is separated.
     */
    void aliasStateElement(
        const std::string &name, MarkovState &other, bool duplicate = false);

    template <size_t N>
    void aliasScalarArray(
        const std::string &prefix, MarkovState &other, bool duplicate = false) {
      for (unsigned int i = 0; i < N; i++)
        aliasStateElement(prefix + std::to_string(i), other, duplicate);
    }

    /**
     * @brief Add an element in the dictionnary.
     *
     * @param f   boost::format object used to build the string-id
     * @param elt  Object to add in the dictionnary. The ownership is transferred to MarkovState.
     * @param write_to_snapshot indicate, if true, that the element has to be written in mcmc files
     * @return StateElement* the same object as "elt", used to daisy chain calls.
     */
    template <typename T>
    T *newElement(
        const boost::format &f, T *elt, const bool &write_to_snapshot = false) {
      return newElement(f.str(), elt, write_to_snapshot);
    }

    /**
     * @brief Get the content of a series of variables into a static array
     * That function is an helper to retrieve the value of a series "variable0",
     * "variable1", ..., "variableQ" of ScalarElement of type Scalar (with Q=N-1).
     * Such a case is for the length:
     * @code
     *  double L[3];
     *  state.getScalarArray<double, 3>("L", L);
     * @endcode
     * This will retrieve L0, L1 and L2 and store their value (double float) in
     * L[0], L[1], L2].
     *
     * @tparam Scalar inner type of the variable to be retrieved in the dictionnary
     * @tparam N      number of elements
     * @param prefix  prefix for these variables
     * @param scalars output scalar array
     */
    template <typename Scalar, size_t N, typename ScalarArray>
    void getScalarArray(const std::string &prefix, ScalarArray &&scalars) {
      for (unsigned int i = 0; i < N; i++) {
        scalars[i] = getScalar<Scalar>(prefix + std::to_string(i));
      }
    }

    /**
     * @brief Get the value of a scalar object.
     *
     * @tparam Scalar
     * @param name
     * @return Scalar&
     */
    template <typename Scalar>
    Scalar &getScalar(const std::string &name) {
      return this->template get<ScalarStateElement<Scalar>>(name)->value;
    }

    template <typename Scalar>
    Scalar &getScalar(const boost::format &name) {
      return this->template getScalar<Scalar>(name.str());
    }

    template <typename Scalar, typename... U>
    Scalar &formatGetScalar(std::string const &name, U &&...u) {
      return this
          ->template formatGet<ScalarStateElement<Scalar>>(
              name, std::forward<U>(u)...)
          ->value;
    }

    template <typename Scalar>
    ScalarStateElement<Scalar> *newScalar(
        const std::string &name, Scalar x,
        const bool &write_to_snapshot = false) {
      ScalarStateElement<Scalar> *elt = new ScalarStateElement<Scalar>();

      elt->value = x;
      newElement(name, elt, write_to_snapshot);
      return elt;
    }

    template <typename Scalar>
    ScalarStateElement<Scalar> *newScalar(
        const boost::format &name, Scalar x,
        const bool &write_to_snapshot = false) {
      return this->newScalar(name.str(), x, write_to_snapshot);
    }

    ///@deprecated
    [[deprecated("Do not use anymore mpiSync")]] void
    mpiSync(MPI_Communication &comm, int root = 0);

    void set_save_in_snapshot(const std::string &name, const bool save);
    void set_save_in_snapshot(const boost::format &name, const bool save);
    bool get_save_in_snapshot(const std::string &name);
    bool get_save_in_snapshot(const boost::format &name);

    /**
     * @brief Save the full content of the dictionnary into the indicated HDF5 group.
     *
     * @param fg HDF5 group/file to save the state in.
     */
    void saveState(H5_CommonFileGroup &fg);

    /**
     * @brief Save the full content of the dictionnary into the indicated HDF5 group.
     * This is the MPI parallel variant.
     *
     * @param fg HDF5 file to save the state in.
     */
    void mpiSaveState(
        boost::optional<H5::H5File> fg, MPI_Communication *comm,
        bool reassembly, const bool write_snapshot = false);

    /**
     * @brief Save the full content of the dictionnary into the indicated HDF5 group.
     * This is the MPI parallel variant.
     *
     * @param fg HDF5 group/file to save the state in.
     */
    void mpiSaveState(
        boost::optional<H5_CommonFileGroup &> fg, MPI_Communication *comm,
        bool reassembly, const bool write_snapshot = false);

    void restoreStateWithFailure(H5_CommonFileGroup &fg);

    // Function to launch another function once all indicated requirements have been loaded from the
    // restart file.
    void subscribePostRestore(
        Requirements const &requirements, std::function<void()> f);

    void triggerPostRestore(std::string const &n);

    void restoreState(
        H5_CommonFileGroup &fg, bool partial = false, bool loadSnapshot = false,
        bool acceptFailure = false);
  };

  /** @example example_markov_state.cpp
   *  This is an example of how to use the MarkovState class.
   */
}; // namespace LibLSS

#endif

// ARES TAG: authors_num = 2
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: year(0) = 2014-2021
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
// ARES TAG: name(1) = Jens Jasche
// ARES TAG: year(1) = 2009-2020
// ARES TAG: email(1) = jens.jasche@fysik.su.se
