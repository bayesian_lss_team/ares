/*+
+*/
#include "libLSS/mpi/generic_mpi.hpp"
#include <boost/optional.hpp>
#include "libLSS/mcmc/global_state.hpp"
#include <algorithm>

using namespace LibLSS;

MarkovState &MarkovState::operator=(MarkovState &&other) {
  state_map = std::move(other.state_map);
  save_map = std::move(other.save_map);
  toProcess = std::move(other.toProcess);
  type_map = std::move(other.type_map);
  postLoad = std::move(other.postLoad);
  loaded = std::move(other.loaded);
  return *this;
}

MarkovState::MarkovState(MarkovState &&other) {
  (void)operator=(std::move(other));
}

void MarkovState::mpiSaveState(
    boost::optional<H5::H5File> fg, MPI_Communication *comm, bool reassembly,
    const bool write_snapshot) {
  boost::optional<H5_CommonFileGroup &> opt_fg;

  if (fg != boost::none) {
    opt_fg = *fg;
  }
  mpiSaveState(opt_fg, comm, reassembly, write_snapshot);
}

MarkovState::~MarkovState() {
  for (StateMap::iterator i = state_map.begin(); i != state_map.end(); ++i) {
    Console::instance().format<LOG_VERBOSE>("Destroying %s", i->first);
    delete i->second;
  }
  save_map.clear();
}

std::type_index MarkovState::getStoredType(const std::string &name) const {
  auto iter = type_map.find(name);
  if (iter == type_map.end())
    error_helper<ErrorBadState>("Unknown entry " + name + " during type query");
  return iter->second;
}

void MarkovState::mpiSaveState(
    boost::optional<H5_CommonFileGroup &> fg, MPI_Communication *comm,
    bool reassembly, const bool write_snapshot) {
  ConsoleContext<LOG_VERBOSE> ctx("mpiSaveState");
  H5::Group g_scalar;
  boost::optional<H5_CommonFileGroup &> g_scalar_opt;

  if (fg != boost::none) {
    try {
      g_scalar = fg->createGroup("scalars");
    } catch (const H5::Exception &e) {
      error_helper<ErrorIO>(e.getDetailMsg());
    }
    g_scalar_opt = g_scalar;
  }

  for (auto &&i : state_map) {
    if (write_snapshot && (!get_save_in_snapshot(i.first))) {
      ctx.print("Skip saving " + i.first);
      continue;
    }
    ctx.print("Saving " + i.first);
    if (i.second->isScalar())
      i.second->saveTo(g_scalar_opt, comm, reassembly);
    else {
      H5::Group g;
      boost::optional<H5_CommonFileGroup &> g_opt;
      if (fg != boost::none) {
        try {
          g = fg->createGroup(i.first);
        } catch (const H5::Exception &e) {
          error_helper<ErrorIO>(e.getDetailMsg());
        }
        g_opt = g;
      }
      i.second->saveTo(g_opt, comm, reassembly);
    }
  }
}

void MarkovState::restoreStateWithFailure(H5_CommonFileGroup &fg) {
  Console &cons = Console::instance();
  H5::Group g_scalar = fg.openGroup("scalars");
  for (StateMap::iterator i = state_map.begin(); i != state_map.end(); ++i) {
    cons.print<LOG_VERBOSE>("Attempting to restore " + i->first);
#if H5_VERSION_GE(1, 10, 1)
    if (!g_scalar.nameExists(i->first)) {
      cons.print<LOG_WARNING>("Failure to restore");
      continue;
    }
#endif
    if (i->second->isScalar())
      // Partial is only valid for 'scalar' types.
      i->second->loadFrom(g_scalar, false);
    else {
      H5::Group g = fg.openGroup(i->first);
      i->second->loadFrom(g);
    }
  }
}

void MarkovState::restoreState(
    H5_CommonFileGroup &fg, bool partial, bool loadSnapshot,
    bool acceptFailure) {
  Console &cons = Console::instance();
  H5::Group g_scalar = fg.openGroup("scalars");
  StateMap currentMap = state_map; // Protect against online modifications

  do {
    for (StateMap::iterator i = currentMap.begin(); i != currentMap.end();
         ++i) {
      if (loadSnapshot && !get_save_in_snapshot(i->first))
        continue;

      cons.print<LOG_VERBOSE>("Restoring " + i->first);
#if H5_VERSION_GE(1, 10, 1)
      if (acceptFailure && !g_scalar.nameExists(i->first)) {
        cons.print<LOG_WARNING>("Failure to restore. Skipping.");
        continue;
      }
#endif
      if (i->second->isScalar())
        // Partial is only valid for 'scalar' types.
        i->second->loadFrom(g_scalar, partial);
      else {
        auto g = fg.openGroup(i->first);
        i->second->loadFrom(g);
      }
      triggerPostRestore(i->first);
    }
    currentMap = toProcess;
    toProcess.clear();
  } while (currentMap.size() > 0);

  // Clear up all pending
  if (postLoad.size() > 0) {
    cons.print<LOG_ERROR>("Some post-restore triggers were not executed.");
    MPI_Communication::instance()->abort();
  }
  loaded.clear();
  postLoad.clear();
}

void MarkovState::subscribePostRestore(
    Requirements const &requirements, std::function<void()> f) {
  if (std::includes(
          requirements.begin(), requirements.end(), loaded.begin(),
          loaded.end())) {
    f();
    return;
  }
  postLoad.push_back(std::make_tuple(requirements, f));
}

void MarkovState::triggerPostRestore(std::string const &n) {
  loaded.insert(n);
  auto i = postLoad.begin();
  while (i != postLoad.end()) {
    auto const &req = std::get<0>(*i);
    if (!std::includes(req.begin(), req.end(), loaded.begin(), loaded.end())) {
      ++i;
      continue;
    }
    std::get<1> (*i)();
    auto j = i;
    ++j;
    postLoad.erase(i);
    i = j;
  }
}

void MarkovState::saveState(H5_CommonFileGroup &fg) {
  ConsoleContext<LOG_DEBUG> ctx("saveState");
  H5::Group g_scalar = fg.createGroup("scalars");
  for (auto &&i : state_map) {
    ctx.print("Saving " + i.first);
    if (i.second->isScalar())
      i.second->saveTo(g_scalar);
    else {
      H5::Group g = fg.createGroup(i.first);
      i.second->saveTo(g);
    }
  }
}

void MarkovState::set_save_in_snapshot(
    const std::string &name, const bool save) {
  save_map[name] = save;
}

void MarkovState::set_save_in_snapshot(
    const boost::format &name, const bool save) {
  set_save_in_snapshot(name.str(), save);
}

bool MarkovState::get_save_in_snapshot(const std::string &name) {
  SaveMap::const_iterator i = save_map.find(name);
  if (i == save_map.end()) {
    error_helper_fmt<ErrorBadState>("Invalid access to %s", name);
  }
  return i->second;
}

bool MarkovState::get_save_in_snapshot(const boost::format &name) {
  return get_save_in_snapshot(name.str());
}

void MarkovState::mpiSync(MPI_Communication &comm, int root) {
  namespace ph = std::placeholders;
  for (StateMap::iterator i = state_map.begin(); i != state_map.end(); ++i) {
    i->second->syncData(std::bind(
        &MPI_Communication::broadcast, comm, ph::_1, ph::_2, ph::_3, root));
  }
}

void MarkovState::aliasStateElement(
    std::string const &name, MarkovState &other, bool duplicate) {
  StateElement *elt;
  if (!duplicate)
    elt = other[name].makeAlias();
  else
    elt = other[name].duplicate();
  state_map[name] = elt;
  elt->name = name;
  type_map.insert(
      std::pair<std::string, std::type_index>(name, other.getStoredType(name)));
  toProcess[name] = elt;
  if (elt->name != name) {
    error_helper<ErrorBadState>("Internal name is not what is expected.");
  }
  save_map[name] = other.save_map[name];
}

// ARES TAG: authors_num = 2
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: year(0) = 2014-2021
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
// ARES TAG: name(1) = Jens Jasche
// ARES TAG: year(1) = 2009-2020
// ARES TAG: email(1) = jens.jasche@fysik.su.se
