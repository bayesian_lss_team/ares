/*+
    ARES/HADES/BORG Package -- ./libLSS/mcmc/state_element.cpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2014-2015, 2018, 2020)

+*/
#include <H5Cpp.h>
#include <CosmoTool/hdf5_array.hpp>
#include "libLSS/mcmc/state_element.hpp"
#include "libLSS/tools/errors.hpp"

using namespace LibLSS;

StateElement::~StateElement() {}

void StateElement::added() {
  Console::instance().c_assert(
      !addedElement, "Element can be added only once.");
  addedElement = true;
}

StateElement::NotifyFunction StateElement::trackUpdate(NotifyFunction f) {
  // std::function has no == operator.
  std::shared_ptr<NotifyFunction> f_ptr = std::make_shared<NotifyFunction>(f);
  updateRegistrar.push_back(f_ptr);
  return [f_ptr, this]() { updateRegistrar.remove(f_ptr); };
}

void StateElement::notifyUpdate() {
  for (auto &a : updateRegistrar) {
    (*a)();
  }
}

StateElement *StateElement::makeAlias() {
  error_helper<ErrorNotImplemented>(
      "makeAlias is not implemented for this type.");
}

StateElement *StateElement::duplicate() {
  error_helper<ErrorNotImplemented>(
      "duplicate is not implemented for this type.");
}
