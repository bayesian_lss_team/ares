#define BOOST_TEST_MODULE mpi_types
#define BOOST_TEST_NO_MAIN
#define BOOST_TEST_ALTERNATIVE_INIT_API
#include <boost/test/included/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include "libLSS/tools/static_init.hpp"
#include <boost/test/included/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/variant.hpp>
#include "libLSS/mpi/generic_mpi.hpp"

using namespace LibLSS;

namespace utf = boost::unit_test;

struct MyStruct {
  double a;
  int b;
  char c;
};

LIBLSS_MPI_STRUCT_TYPE(MyStruct, ((double, a))((int, b)));

MPI_FORCE_COMPOUND_TYPE_NO_INLINE(MyStruct);

BOOST_AUTO_TEST_CASE(basic_type) { auto t = translateMPIType<MyStruct>(); }

int main(int argc, char **argv) {
  setupMPI(argc, argv);

  LibLSS::QUIET_CONSOLE_START = true;
  StaticInit::execute();
  LibLSS::Console::instance().setVerboseLevel<LOG_DEBUG>();

  int ret = utf::unit_test_main(&init_unit_test, argc, argv);

  StaticInit::finalize();

  doneMPI();
  return 0;
}
