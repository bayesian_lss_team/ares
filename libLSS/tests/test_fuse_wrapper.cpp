/*+
    ARES/HADES/BORG Package -- ./libLSS/tests/test_fuse_wrapper.cpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2017-2020)

+*/
#define BOOST_TEST_MODULE modelio
#define BOOST_TEST_NO_MAIN
#define BOOST_TEST_ALTERNATIVE_INIT_API
#include <boost/test/included/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include "libLSS/mpi/generic_mpi.hpp"
#include <iostream>
#include <boost/multi_array.hpp>
#include <boost/timer/timer.hpp>
#include "libLSS/tools/fused_array.hpp"
#include "libLSS/tools/fusewrapper.hpp"
#include "libLSS/tools/fuse/operators.hpp"
#include "libLSS/tools/static_init.hpp"

double fun() {
  static int i = 0;
  i++;
  return i;
}

using namespace boost::timer;

namespace utf = boost::unit_test_framework;

static size_t N = 32;

static void init_A(boost::multi_array<double, 3> &A) {
  auto fA = LibLSS::fwrap(A);
  // Initialize A with some linear space.
  fA = LibLSS::b_fused_idx<double, 3>([](int i, int j, int k) -> double {
    return double(i) / N + double(j) / N + double(k) / N;
  });
}

BOOST_AUTO_TEST_CASE(test_fuse1) {
  using boost::extents;
  using boost::multi_array;
  using LibLSS::b_fused_idx;
  using LibLSS::b_va_fused;
  using LibLSS::fwrap;
  const double REF_VALUE = 47616;
  boost::multi_array<double, 3> A(extents[N][N][N]);
  boost::multi_array<double, 3> B(extents[N][N][N]);

  auto fA = fwrap(A);
  auto fC = fwrap(fA.fautowrap(fun));
  auto fD = LibLSS::b_fused<double>(A, [](auto x) { return 2.0 * M_PI * x; });

  // Initialize A with some linear space.
  init_A(A);

  BOOST_CHECK_CLOSE(LibLSS::reduce_sum<double>(A), REF_VALUE, 0.1);
  {
    double r = 0;
    for (int i = 0; i < N; i++)
      for (int j = 0; j < N; j++)
        for (int k = 0; k < N; k++)
          r += A[i][j][k];
    BOOST_CHECK_CLOSE(r, REF_VALUE, 0.1);
  }
  BOOST_CHECK_CLOSE(LibLSS::fwrap(fD).sum(), 299180.15158666315, 0.1);
}

BOOST_AUTO_TEST_CASE(test_fuse_timing) {
  boost::multi_array<double, 3> A(boost::extents[N][N][N]);

  auto fA = LibLSS::fwrap(A);
  cpu_timer timer;

  double r = 0;
  for (int i = 0; i < 10; i++)
    r += ((fA * 2. + 5.) / 7).sum();
  std::cout << "10 composite multiply, sum and reduce:" << timer.format() << " "
            << r << std::endl;
}

template <typename A, typename B>
static void array_equal(A const &a, B const &b) {
  for (int d = 0; d < a.num_dimensions(); d++)
    BOOST_CHECK_EQUAL(a.shape()[d], b.shape()[d]);
}

BOOST_AUTO_TEST_CASE(test_fuse_lazy) {
  boost::multi_array<double, 3> A(boost::extents[N][N][N]);
  boost::multi_array<double, 3> B(boost::extents[N][N][N]);

  auto fA = LibLSS::fwrap(A);
  init_A(A);

  // Create a lazy expression.
  auto fB = std::cos(fA * (2 * M_PI)); //std::cos(fA*2*M_PI);
  // WORKS PARTIALLY: shapeness must be better computed
  auto fB2 = std::cos((2 * M_PI) * fA); //std::cos(fA*2*M_PI);

  // This does a full collapse of the expression, including the squaring

  {
    cpu_timer timer;
    double value = LibLSS::ipow<2>(fB).sum() / LibLSS::ipow<3>(N);
    BOOST_CHECK_CLOSE(value, 0.5, 0.1);
    std::cout << "Composite multiply, cos, square and reduce:" << timer.format()
              << std::endl;
    double value2 = LibLSS::ipow<2>(fB2).sum() / LibLSS::ipow<3>(N);
    BOOST_CHECK_CLOSE(value, 0.5, 0.1);
  }
  {
    cpu_timer timer;
    double value = std::abs(fB).sum() / LibLSS::ipow<3>(N);
    BOOST_CHECK_CLOSE(value, 0.63457314922555363, 0.1);
    std::cout << "Composite multiply, cos, abs and reduce:" << timer.format()
              << std::endl;
  }
  auto fE = LibLSS::fwrap(B);
  {
    cpu_timer timer;
    fE = fB;
    array_equal(B, *fB);
    std::cout << "Composite multiply, cos and assign:" << timer.format()
              << std::endl;
  }

  {
    cpu_timer timer;
    std::cout << (fE * fE).sum() << std::endl;
    std::cout << "Composite square and reduce:" << timer.format() << std::endl;
  }
}

BOOST_AUTO_TEST_CASE(test_fuse_accumulators) {
  using LibLSS::fwrap;
  boost::multi_array<double, 3> A(boost::extents[N][N][N]);
  boost::multi_array<double, 3> B(boost::extents[N][N][N]);
  auto fA = LibLSS::fwrap(A);
  init_A(A);
  init_A(B);

  fA += B;

  fwrap(B) *= 2;
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      for (int k = 0; k < N; k++)
        BOOST_CHECK_CLOSE(A[i][j][k], B[i][j][k], 0.1);
}

#if 0
int main(int argc, char **argv) {
  LibLSS::setupMPI(argc, argv);
  LibLSS::StaticInit::execute();
  using LibLSS::_p1;
  using LibLSS::_p2;


  //std::cout << fB->shape()[0] << std::endl;

  // Assign the cos part

  std::cout << std::pow(std::abs(fE), 2.5).sum()
            << std::endl; ////std::pow(std::abs(fE), 2.5).sum() << std::endl;
  std::cout << (std::abs(fE)).min()
            << std::endl; ////std::pow(std::abs(fE), 2.5).sum() << std::endl;
  std::cout << (std::abs(fE)).max()
            << std::endl; ////std::pow(std::abs(fE), 2.5).sum() << std::endl;
  double r = std::numeric_limits<double>::infinity();
  for (size_t i = 0; i < N; i++)
    for (size_t j = 0; j < N; j++)
      for (size_t k = 0; k < N; k++)
        r = std::min(r, std::abs((*fE)[i][j][k]));

  std::cout << r << std::endl;

  fwrap(B) = fwrap(A);

  fwrap(B) = -fwrap(A);

  std::cout << fwrap(B).sum() << " " << fwrap(A).sum() << std::endl;

  std::cout << fwrap(B).no_parallel().sum() << std::endl;

  multi_array<std::complex<double>, 3> c_B(extents[N][N][N]);
  auto f_c_B = fwrap(c_B);
  double x = std::real(f_c_B).sum();
  std::cout << x << std::endl;

  auto c_a = LibLSS::make_complex(fwrap(A), fwrap(B));

  f_c_B += A;

  //double sB;
  //auto scalar_A = fwrap(1.0);
  //auto scalar_B = fwrap(sB);

  //scalar_B = scalar_A + 2;

  return 0; //fA.sum();
}
#endif

int main(int argc, char **argv) {
  LibLSS::setupMPI(argc, argv);
  LibLSS::QUIET_CONSOLE_START = true;
  LibLSS::StaticInit::execute();
  LibLSS::Console::instance().setVerboseLevel<LibLSS::LOG_STD>();

  int ret = utf::unit_test_main(&init_unit_test, argc, argv);

  LibLSS::StaticInit::finalize();
  LibLSS::doneMPI();
  return 0;
}
