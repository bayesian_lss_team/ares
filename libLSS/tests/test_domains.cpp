#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/tools/domains.hpp"
#include "libLSS/tools/static_init.hpp"
#include "libLSS/tools/console.hpp"

using namespace LibLSS;

int main(int argc, char **argv) {
  auto comm = setupMPI(argc, argv);
  StaticInit::execute();
  auto &cons = Console::instance();
  cons.outputToFile(
      boost::str(boost::format("domain_log_%d.txt") % comm->rank()));

  CompleteDomainSpec<3> complete_input, complete_output;
  DomainSpec<3> input;
  DomainSpec<3> output;
  int r = comm->rank(), s = comm->size();
  int Ng = 8;

  // Rank 0 has everything
  if (r == 0) {
    input.domains.resize(1);
    input.domains[0][0] = 0;
    input.domains[0][1] = Ng;
    input.domains[0][2] = 0;
    input.domains[0][3] = Ng;
    input.domains[0][4] = 0;
    input.domains[0][5] = Ng;
  }
  output.domains.resize(1);
  output.domains[0][0] = r * Ng / s;
  output.domains[0][1] = (r + 1) * Ng / s;
  output.domains[0][2] = r * Ng / s;
  output.domains[0][3] = (r + 1) * Ng / s;
  output.domains[0][4] = 0;
  output.domains[0][5] = 8;

  computeCompleteDomainSpec(comm, complete_input, input);

  cons.print<LOG_VERBOSE>("Input domains");
  for (int r = 0; r < s; r++) {
    cons.format<LOG_VERBOSE>(
        " rank %d -> numDomains = %d", r,
        complete_input.domainOnRank[r].domains.size());
    for (auto &d : complete_input.domainOnRank[r].domains) {
      cons.format<LOG_VERBOSE>("         -> [%d, %d[ ", d[0], d[1]);
    }
  }

  computeCompleteDomainSpec(comm, complete_output, output);
  cons.print<LOG_VERBOSE>("Output domains");
  for (int r = 0; r < s; r++) {
    cons.format<LOG_VERBOSE>(
        " rank %d -> numDomains = %d", r,
        complete_output.domainOnRank[r].domains.size());
    for (auto &d : complete_output.domainOnRank[r].domains) {
      cons.format<LOG_VERBOSE>("         -> [%d, %d] ", d[0], d[1]);
    }
  }

  DomainTodo<3> todo;
  mpiDomainComputeTodo(comm, complete_input, complete_output, todo);

  cons.print<LOG_VERBOSE>("TODO");
  for (auto &task : todo.tasks) {
    cons.format<LOG_VERBOSE>(
        " rank %d %s slice=[%d,%d,%d,%d,%d,%d]", task.rankIO,
        task.recv ? "->" : "<-", task.slice[0], task.slice[1], task.slice[2],
        task.slice[3], task.slice[4], task.slice[5]);
  }

  boost::multi_array<double, 3> input_data(boost::extents[Ng][Ng][Ng]);
  fwrap(input_data) = comm->rank();
  boost::multi_array<double, 3> output_data(boost::extents[Ng][Ng][Ng]);

  mpiDomainRun(comm, input_data, output_data, todo);

  StaticInit::finalize();
  doneMPI();
  return 0;
}
