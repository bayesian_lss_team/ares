/*+
+*/
#define BOOST_TEST_MODULE markov_state
#define BOOST_TEST_NO_MAIN
#define BOOST_TEST_ALTERNATIVE_INIT_API
#include <boost/test/included/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include "libLSS/mpi/generic_mpi.hpp"
#include <iostream>
#include <boost/multi_array.hpp>
#include <boost/timer/timer.hpp>
#include "libLSS/tools/static_init.hpp"
#include "libLSS/mcmc/global_state.hpp"
#include "libLSS/mcmc/state_element.hpp"

namespace utf = boost::unit_test_framework;

BOOST_AUTO_TEST_CASE(test_empty_markov_state) {
  LibLSS::MarkovState s;

  s.newScalar<double>("test_double", 1.0);
  BOOST_CHECK_EQUAL(s.getScalar<double>("test_double"), 1.0);
}

//BOOST_AUTO_TEST_CASE(test_split_markov_state) {
//  LibLSS::MarkovState s;
//  LibLSS::MarkovState s2;
//
//  s.newScalar<double>("test_double", 1.0);
//  s.newElement(
//      "test_array",
//      new LibLSS::ArrayStateElement<double, 2>(boost::extents[32][32]));
//
//  LibLSS::Console::instance().setVerboseLevel(-1);
//  BOOST_CHECK_THROW(s.split({"test_double"}), LibLSS::ErrorNotImplemented);
//  LibLSS::Console::instance().setVerboseLevel<LibLSS::LOG_ERROR>();
//  BOOST_CHECK_NO_THROW(std::exchange(s2, s.split({"test_array"})));
//  {
//    auto element1 = s.get<LibLSS::ArrayStateElement<double, 2>>("test_array");
//    auto element2 = s2.get<LibLSS::ArrayStateElement<double, 2>>("test_array");
//    BOOST_CHECK_NE(element1, element2);
//    BOOST_CHECK_EQUAL(element1->array.get(), element2->array.get());
//  }
//}

int main(int argc, char **argv) {
  LibLSS::setupMPI(argc, argv);
  LibLSS::QUIET_CONSOLE_START = true;
  LibLSS::StaticInit::execute();
  LibLSS::Console::instance().setVerboseLevel<LibLSS::LOG_STD>();

  int ret = utf::unit_test_main(&init_unit_test, argc, argv);

  LibLSS::StaticInit::finalize();
  LibLSS::doneMPI();
  return 0;
}
// ARES TAG: num_authors = 1
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: year(0) = 2021
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
