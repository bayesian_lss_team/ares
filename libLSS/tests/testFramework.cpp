/*+
    ARES/HADES/BORG Package -- ./libLSS/tests/testFramework.cpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2020)

+*/
#include <string>
#include "libLSS/tests/testFramework.hpp"
#include "libLSS/cconfig.h"

std::string LibLSS_tests::reference_path = __LIBLSS_TEST_REFERENCE_PATH;
