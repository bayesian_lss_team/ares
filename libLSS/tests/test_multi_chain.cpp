#define BOOST_TEST_NO_MAIN
#define BOOST_TEST_ALTERNATIVE_INIT_API
#define BOOST_TEST_MODULE multi_chain
#include <boost/test/included/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/variant.hpp>
#include "libLSS/samplers/core/main_loop.hpp"
#include "libLSS/tools/fusewrapper.hpp"

namespace utf = boost::unit_test;

using namespace LibLSS;

struct level_guard {
  int save;

  level_guard() : save(LibLSS::Console::instance().getVerboseLevel()) {
    LibLSS::Console::instance().setVerboseLevel(-1);
  }

  ~level_guard() { LibLSS::Console::instance().setVerboseLevel(save); }
};

class TestSampler : public MarkovSampler {
public:
  void initialize(std::list<StateTuple> states) override {
    BOOST_CHECK_EQUAL(states.size(), 2);
    std::array<bool, 2> seen{false, false};

    for (auto const &s : states) {
      BOOST_CHECK_EQUAL(seen[std::get<0>(s)], false);
      seen[std::get<0>(s)] = true;
    }
    for (auto const &s : seen) {
      BOOST_CHECK_EQUAL(s, true);
    }
  }
  void restore(std::list<StateTuple> states) override {}
  void sample(std::list<StateTuple> states) override {}
};

class DummySampler : public MarkovSampler {
protected:
  int id;
  bool groupped;
  int value_min, value_max;

public:
  DummySampler(int id_, bool groupped_ = false, int vmin = -1, int vmax = -1)
      : id(id_), groupped(groupped_), value_min(vmin), value_max(vmax) {}

  void initialize(std::list<StateTuple> states) override {
    auto &state = std::get<1>(states.front());

    if (id == 0) {
      state.newScalar("dummy", 0UL);
      state.newScalar("dummy_group", std::string(""));
    }
  }

  void restore(std::list<StateTuple> states) override {}
  void sample(std::list<StateTuple> states) override {

    auto &state = std::get<1>(states.front());

    Console::instance().format<LOG_VERBOSE>("running id=%d", id);
    auto &value = state.getScalar<unsigned long>("dummy");
    auto &last_group = state.getScalar<std::string>("dummy_group");
    if (id != 0) {
      BOOST_TEST((groupped || (id - 1) == value));
      BOOST_TEST(
          (!groupped || ((id - 1) >= value_min) && (id - 1) <= value_max));
    }
    value = std::max(int(value), id);
    last_group = getName();
  }
};

BOOST_AUTO_TEST_CASE(loop_2chain) {
  typedef LibLSS::ArrayStateElement<double, 1> Array1d;
  {
    MainLoop loop(MPI_Communication::instance_ptr(), "", 2);

    loop.getState(0);
    loop.getState(1);
    {
      level_guard guard;
      BOOST_CHECK_THROW(loop.getState(2), LibLSS::ErrorOutOfBounds);
    }

    Array1d *a = new Array1d(boost::extents[10]);

    loop.getState(0).newElement<Array1d>("array", a);
    loop.getState(1).newElement<Array1d>("array", (Array1d *)a->makeAlias());
    loop.getState(1).newElement<Array1d>(
        "array2", new Array1d(boost::extents[10]));

    BOOST_CHECK_EQUAL(
        loop.getState(0).get<Array1d>("array")->array,
        loop.getState(1).get<Array1d>("array")->array);

    fwrap(*a->array) = 1;

    loop.save();
  }
  {
    MainLoop loop(MPI_Communication::instance_ptr(), "", 2);
    bool aliasLoaded = false;

    Array1d *a = new Array1d(boost::extents[10]);
    loop.getState(0).newElement<Array1d>("array", a);
    fwrap((*a->array)) = 0;
    loop.getState(1)
        .newElement<Array1d>("array", (Array1d *)a->makeAlias())
        ->subscribeLoaded([&]() { aliasLoaded = true; });
    loop.restore("restart.h5");

    BOOST_CHECK_EQUAL(aliasLoaded, true);

    for (unsigned int i = 0; i < a->array->shape()[0]; i++)
      BOOST_CHECK_EQUAL((*a->array)[i], 1);
    auto b = loop.getState(1).get<Array1d>("array");
    for (unsigned int i = 0; i < b->array->shape()[0]; i++)
      BOOST_CHECK_EQUAL((*b->array)[i], 1);

    loop.setCurrentChainSpec({0, 1});
    loop << std::make_shared<TestSampler>();
    loop.finalizeGraph();
    loop.initialize();
  }
}

BOOST_AUTO_TEST_CASE(loop_fancy_ordering) {
  MainLoop loop(MPI_Communication::instance_ptr(), "", 1);

  auto sampler1 = std::make_shared<DummySampler>(0);
  auto sampler2 = std::make_shared<DummySampler>(1);
  auto sampler3 = std::make_shared<DummySampler>(2);
  sampler1->setName("0");
  sampler2->setName("1");
  sampler3->setName("2");

  loop << sampler3;
  loop << sampler1;
  loop << sampler2;

  loop.addDependency("1", {"0"});
  loop.addDependency("2", {"0", "1"});

  loop.finalizeGraph();

  loop.initialize();
  loop.run();
}

BOOST_AUTO_TEST_CASE(loop_fancy_ordering_group) {
  MainLoop loop(MPI_Communication::instance_ptr(), "", 1);

  auto sampler1 = std::make_shared<DummySampler>(0);
  auto sampler2 = std::make_shared<DummySampler>(1, true, 0, 2);
  auto sampler3 = std::make_shared<DummySampler>(2, true, 0, 2);
  auto sampler4 = std::make_shared<DummySampler>(3);
  sampler1->setName("0");
  sampler2->setName("1");
  sampler3->setName("1");
  sampler4->setName("2");

  loop << sampler3;
  loop << sampler4;
  loop << sampler1;
  loop << sampler2;

  loop.addDependency("1", {"0"});
  loop.addDependency("2", {"1"});

  loop.finalizeGraph();

  loop.initialize();
  loop.run();
}

int main(int argc, char **argv) {
  setupMPI(argc, argv);

  LibLSS::QUIET_CONSOLE_START = true;
  StaticInit::execute();
  LibLSS::Console::instance().setVerboseLevel<LOG_DEBUG>();

  int ret = utf::unit_test_main(&init_unit_test, argc, argv);

  StaticInit::finalize();

  doneMPI();
  return 0;
}
