/*+
    ARES/HADES/BORG Package -- ./libLSS/tests/dummy_file.cpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018, 2020)

+*/
/* empty file just to quiet CMake */
