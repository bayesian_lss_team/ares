===============================================
ARES: Algorithm for REconstruction and Sampling
===============================================

Copyright(c) 2009-2020 Jens Jasche, 2014-2021 Guilhem Lavaux

This is the development branch. Everything here is unstable.

|build_status| |made_with| |rtd|

Description
-----------

This is the main component of the Bayesian Large Scale Structure inference
pipeline.

A lot of complementary informations are available on the Readthedocs
(https://aquila-ares.readthedocs.io/en/latest/user/building.html).

Building and requirements
-------------------------

Check this page on RTD https://aquila-ares.readthedocs.io/en/latest/user/building.html.

The main shell scripts have been written to be used with BASH (typically available on GNU/Linux) / ZSH (used on MacOS).
DASH or POSIX shells *will not work*.

Documentation
-------------

Please refer to `docs/README.txt`.
Check readthedocs at https://aquila-ares.readthedocs.io/


Usage policy
------------

Please check that page https://aquila-ares.readthedocs.io/en/latest/index.html#citing.

Acknowledgements
----------------

Please check https://aquila-ares.readthedocs.io/en/latest/index.html#acknowledgements.

.. |build_status| image:: https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fwww.aquila-consortium.org%2Fjenkins%2Fjob%2Fares_multi_pipeline%2Fjob%2Fmain%2F&style=plastic
  :alt: Build status
  :target: https://www.aquila-consortium.org/jenkins/job/ares_multi_pipeline/job/main/

.. |made_with| image:: https://img.shields.io/badge/Made%20with-C%2B%2B%20%26%20Python-blue
  :alt: Made with C++ & Python

.. |rtd| image:: https://readthedocs.org/projects/aquila-ares/badge/?version=latest&style=flat
   :alt: Check readthedocs.org
   :target: https://aquila-ares.readthedocs.io/en/latest
