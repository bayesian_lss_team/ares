* Install python3 sphinx. You can use your favoured package manager (e.g. dnf install python3-sphinx)

* Install doxygen (dnf install doxygen)

* pip3 install --user -r requirements.txt

* "make html"
