Postprocessing
##############

.. _postprocessing:

.. include:: postprocessing/Postprocessing_scripts.inc.rst
.. include:: postprocessing/ARES_basic_outputs.inc.rst
.. include:: postprocessing/Diagnostics_ARES_BORG_chains.inc.rst
.. include:: postprocessing/HADES_generate_constrained_simulations.inc.rst
