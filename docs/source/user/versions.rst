.. _versions:

Versions and branches
=====================

Software versioning model
-------------------------

.. sectionauthor:: Guilhem Lavaux, Florent Leclercq (last update: 18 May 2021)

There are currently 5 "main" branches in |ares|:

* main
* release/1.0
* release/2.0alpha
* release/2.1
* release/2.2

The ``main`` branch contains the bleeding edge version of |ares|.

The ``release/*`` branches are stable, which means that the existing code cannot
change significantly. In particular, API and features are not altered. Bug fixes can still go there, as well as exceptional late merging of features. Release notes for these branches can
be found in the file ``CHANGES.rst``, which is included :ref:`below in this page <release_notes>`.

There are of course lots of other versions (and corresponding branches) containing different features,
and development branches for each member of the collaboration.

When starting a new development, the general advice is to branch from the latest ``release``
branch, unless you need a feature which is only available in ``main``. Please try to follow the
following naming convention: ``your_name/your_feature``.

.. _release_notes:

.. include:: ../../../CHANGES.rst
