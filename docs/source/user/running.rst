Running the executables
#######################

.. _running:

.. include:: running/ARES_Tutorials.inc.rst
.. include:: running/HADES_Tutorials.inc.rst
.. include:: running/BORG_Tutorials.inc.rst
.. include:: running/BORG_with_simulation_data.inc.rst
