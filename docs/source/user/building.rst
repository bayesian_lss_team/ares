.. _building:

Building
########

.. sectionauthor:: Guilhem Lavaux, Florent Leclercq (last update: 19 May 2021)

Prerequisites
=============

Compiling |ares| requires the following softwares:

* `cmake <https://cmake.org>`_ ≥ 3.13
* `automake <https://www.gnu.org/software/automake/>`_
* libtool
* pkg-config
* gcc ≥ 7, or intel compiler(≥ 2018), or Clang(≥ 7)
* wget (to download dependencies; the flag ``--use-predownload``
  can be used to bypass this dependency)

Optional requirements are :

* An `OpenMP <http://www.openmp.org>`_-enabled compiler (with OpenMP ≥ 2.0)

Requirements for the |ares| python package are described in :ref:`this section <installing_python_module>`.

|ares| does not require any pre-installed external libraries; it will download
and compile all necessary dependencies by default.

The total installation will take approximately **7-8 GByte** of disk
space.

Loading modules at HPC facilities
---------------------------------

HPC facilities usually use `modules <https://github.com/cea-hpc/modules>`_.
In this case, you can check which modules are available using

.. code:: bash

   module avail

and load modules using

.. code:: bash

    module load module_name

To build |ares|, load the modules corresponding to the compiler and build environment
(including cmake and python 3, see above).

.. _downloading_and_setting_up_for_building:

Instructions for some specific clusters are available :ref:`here <clusters>`.

Downloading and setting up for building
=======================================

General case (computer with internet access)
--------------------------------------------

The first step to obtain and build |ares| is to clone the git repository
from Bitbucket. If the computer has access to the
internet, use the following command:

.. code:: bash

   git clone --recursive git@bitbucket.org:bayesian_lss_team/ares.git
   cd ares

.. note::

    If you forgot the ``--recursive`` option, you can either start from
    scratch or do

    .. code:: bash

        git submodule init ; git submodule update

You may want to chose a branch of interest (see the :ref:`Release notes <release_notes>`).
If you want to work on a branch other than ``main``, use (for instance)

.. code:: bash

    git checkout release/2.1

To set up the rest of the environment, use the ``get-aquila-modules.sh``
script. The first step consists in cloning the Aquila private modules
in the ``extra/`` subdirectory, using:

.. code:: bash

    bash get-aquila-modules.sh --clone

The second step is to ensure that active branches of the main |ares| repository
and of the private Aquila repositories are consistent, using

.. code:: bash

    bash get-aquila-modules.sh --branch-set

This command uses the information found in the file ``.aquila-modules``.
Whenever you want to modify |ares| or one of the Aquila private modules,
create a new branch in the main repository and/or the module, and work
in that branch. You may modify the ``.aquila-modules`` file accordingly.


When all modules have been cloned and set up, download the dependencies
using

.. code:: bash

    bash build.sh --download-deps

You can check that dependencies (``.tar.gz``, ``.tar.bz2`` and ``.zip`` archives)
have appeared in the ``downloads/`` subdirectory.

After that you can move on to :ref:`building <the_build.sh_script>`.

.. _supercomputer_without_outgoing_access_to_internet:

Supercomputer without outgoing access to internet
-------------------------------------------------

On some supercomputers (e.g. TGCC in France), it is impossible to access
the internet directly. In this case, a first clone of |ares| and its modules
should be done on your laptop/workstation. Use another directory, for example:

.. code:: bash

   git clone --recursive git@bitbucket.org:bayesian_lss_team/ares.git ares_clean
   cd ares_clean
   bash get-aquila-modules.sh --clone
   bash get-aquila-modules.sh --branch-set

You should then download dependencies locally, using

.. code:: bash

   bash build.sh --download-deps

Now replicate this directory on the distant computer:

.. code:: bash

   cd ..
   rsync -av ares_clean THE_COMPUTER:

And you can now proceed to :ref:`building <the_build.sh_script>`
on the distant computer.

For updating the git tree later, there are two special
commands available in ``get-aquila-modules.sh``. On your laptop/workstation,
run the following from the |ares| root directory:

.. code:: bash

   bash get-aquila-modules.sh --send-pack THE_COMPUTER ares_clean origin

This will send the content of the current git tree (including the
registered modules in .aquila-modules) from the remote ``origin`` to
the remote directory ``ares_clean`` on the computer ``THE_COMPUTER``.
However the checked out branch will not be remotely merged! A second
operation is required. Now login on the distant computer and run

.. code:: bash

   bash get-aquila-modules.sh --local-merge origin

This will merge all the corresponding branches from origin to the
checked out branches. If everything is ok you should not get any error
messages. Errors can happen if you modified the branches in an
incompatible way. In that case you have to fix the git merge in the
usual way (edit files, add them, commit).

.. _the_build.sh_script:

Configuring using the build.sh script
=====================================

To help with the building process, the ``build.sh`` script in the root directory
will ensure that cmake is called correctly, with all of the adequate parameters.
At the same time, it will clean the build directory if needed.

The basic scenario for configuring is the following:

.. code:: bash

    bash build.sh --use-predownload

Please pay attention to warnings and error messages. The most important are
marked in color. In particular, some problems may occur if two versions of the
same compiler are used for C and C++.

You can configure the cmake executable and compilers using:

.. code:: bash

    bash build.sh --cmake CMAKE_BINARY --c-compiler YOUR_C_COMPILER --cxx-compiler YOUR_CXX_COMPILER --use-predownload

If you want to compile with MPI support, add the ``--with-mpi`` flag to ``build.sh``.

The full usage is the following (it obtained with ``bash build.sh --help`` or ``bash build.sh -h``):

.. code:: text

    Ensure the current directory is ARES
    This is the build helper. The arguments are the following:

    --cmake CMAKE_BINARY    instead of searching for cmake in the path,
    use the indicated binary

    --without-openmp        build without openmp support (default with)
    --with-mpi              build with MPI support (default without)
    --c-compiler COMPILER   specify the C compiler to use (default to cc)
    --cxx-compiler COMPILER specify the CXX compiler to use (default to c++)
    --julia JULIA_BINARY    specify the full path of julia interpreter
    --build-dir DIRECTORY   specify the build directory (default to "build/" )
    --debug                 build for full debugging
    --no-debug-log          remove all the debug output to increase speed on parallel
                            filesystem.
    --perf                  add timing instructions and report in the log files

    --extra-flags FLAGS     extra flags to pass to cmake
    --download-deps         Predownload dependencies
    --use-predownload       Use the predownloaded dependencies. They must be in
                            downloads/
    --no-predownload        Do not use predownloaded dependencies in downloads/
    --purge                 Force purging the build directory without asking
                            questions.
    --native                Try to activate all optimizations supported by the
                            running CPU.
    --python[=PATH]         Enable the building of the python extension. If PATH
                            is provided it must point to the executable of your
                            choice for (e.g `/usr/bin/python3.9`)
    --with-julia            Build with Julia support (default false)
    --hades-python          Enable hades-python (implies --python)
    --skip-building-tests   Do not build all the tests
    --install-system-python Install python package in the python system dir
    --install-user-python   Install python package in the user directory [default]

    Advanced usage:

    --eclipse                Generate for eclipse use
    --ninja                  Use ninja builder
    --update-tags            Update the TAGS file
    --use-system-boost[=PATH] Use the boost install available from the system. This
                            reduces your footprint but also increases the
                            possibilities of miscompilation and symbol errors.
    --use-system-fftw[=PATH] Same but for FFTW3. We require the prefix path.
    --use-system-gsl         Same but for GSL
    --use-system-eigen=PATH  Same but for EIGEN. Here we require the prefix path of
                            the installation.
    --use-system-hdf5[=PATH] Same but for HDF5. Require an HDF5 with C++ support.
                            The path indicate the prefix path of the installation of HDF5
                            (e.g. /usr/local or /usr). By default it will use
                            environment variables to guess it (HDF5_ROOT)

    After the configuration, you can further tweak the configuration using ccmake
    (if available on your system).

.. note::

    If you have built |ares| before cloning (some of) the extra modules,
    you can still recompile using your previous build. For that, go to
    your build directory and run

    .. code:: bash

        ${CMAKE} .

    where ``${CMAKE}`` is the cmake executable that you used originally
    (using the ``--cmake`` flag). If you did not specify anything just use

    .. code:: bash

        cmake .

A typical successful completion of the configuration ends like that:

.. code:: text

   Configuration done.
   Move to /home/lavaux/PROJECTS/ares/build and type 'make' now.
   Please check the configuration of your MPI C compiler. You may need
   to set an environment variable to use the proper compiler.
   Some example (for SH/BASH shells):
      OpenMPI:
      OMPI_CC=cc
      OMPI_CXX=c++
      export OMPI_CC OMPI_CXX

.. _compiling:

Compiling
=========

The successful configuration message tells you to move to the build directory
(by default the ``build/`` subdirectory of the |ares| root directory) and compile:

.. code:: bash

    cd build
    make

Some additional ``make`` arguments:

* You can indicate a specific target if you do not want to build everything. For
  instance,

  .. code:: bash

      make test_console

  will only build the executable ``libLSS/tests/test_console``.

* You may use ``make`` parallelism using the ``-j`` option. A number
  indicates the number of CPU cores to be used for compilation. For example,

  .. code:: bash

      make all -j4

  will compile everything using 4 parallel tasks. If the -j option is given
  without an argument, make will not limit the number of jobs that can run
  simultaneously.

  .. note::

      As we may not have accounted for all of the detailed compilation dependencies yet,
      a compilation failure may happen when using parallelism. In this case,
      just execute ``make`` again to ensure that everything is in order (it should be).

* Finally, you can use

  .. code:: bash

      make VERBOSE=1

  to see exactly what the compiler is doing.

Upon compilation success, you will find executables in the ``src/`` subdirectory of
your ``build/`` directory, notably::

    ./src/ares3


.. _installing_python_module:

Installing the python module
============================

Setting up the python environment
---------------------------------

The command ``bash get-aquila-modules.sh --clone`` (see
:ref:`this section <downloading_and_setting_up_for_building>`)
automatically clones the |ares| python module in ``extra/``.

Installation requires at least the following packages:

* python3-dev
* python3-distutils
* zlib1g-dev

It is recommended to use a python environment manager such as `anaconda <https://www.anaconda.com>`__, `miniconda <https://docs.conda.io/en/latest/miniconda.html>`__ or `pyenv <https://github.com/pyenv/pyenv>`__, which will automatically take care of these usual dependencies.

You should create a fresh environment for your |ares| installation. With anaconda, the commands are:

.. code:: bash

    conda create -n ares
    conda activate ares

Python scripts have been tested with the following:

* python == 3.6 - 3.9
* healpy == 1.10.3 (Guilhem has also a special version of healpy on Github `here <https://github.com/glavaux/healpy>`__)
* h5py == 2.7.0
* numexpr == 2.6.2
* numba == 0.33.0 - 0.35.0

To install these dependencies in your python environment, use

.. code:: bash

    conda install python=3 healpy h5py numexpr numba

.. _python_installation_without_mpi:

Configuration and compilation without mpi
-----------------------------------------

To configure the installation with the python module, run ``build.sh``
with the ``--python`` flag (see :ref:`this section<the_build.sh_script>`):

.. code:: bash

    bash build.sh --use-predownload --python

By default, the python module will be installed in the user site-package
directory. If you use a dedicated python environment for |ares|, you may
want to install in the system site-package directory (that of your environment).
To do so, you can use the flag ``--install-system-python`` (available from
:ref:`release 2.2 <release_notes>`):

.. code:: bash

    bash build.sh --use-predownload --install-system-python --python

.. note::

    You can check (and change) the path of system and user site-package
    directories by moving to your build directory and using ccmake, if
    available on your machine:

    .. code:: bash

        cd build
        ccmake .

    Toggle to advanced mode by typing [t] and check the variables ``SYSTEM_PYTHON_SITE_PACKAGES``
    and ``USER_PYTHON_SITE_PACKAGES``. If changing something, you should reconfigure (using [c])
    and regenerate (using [g]).

The cmake output should contain lines such as:

.. code:: text

    -- Found PythonInterp: your_anaconda_install/envs/ares/bin/python3 (found version "3.9.4")
    -- Found PythonLibs: your_anaconda_install/envs/ares/lib/libpython3.9.so

To compile and install, go to your build directory and type ``make python-install`` (see :ref:`this section <compiling>`):

.. code:: bash

    cd build
    make python-install

.. note::

    The target ``python-install`` has been introduced in release 2.2. In earlier versions, the command to
    install the python module is ``make install``.

    The difference between the ``python-install`` and ``install`` targets is that ``make install`` implicitly
    assumes that all other targets have been built (including benchmarks and tests, which can be slow), while
    ``make python-install`` only relies on the necessary target ``_borg``. From release 2.2, ``make install``
    will still install the python module, after building everything else.

At the end of compilation, the python module will be installed in the site-package
directory and made available to python virtual machine. To check the installation,
start a python console and type ``import borg``. The output should look like this:

.. code:: python3

    >>> import borg
    Initializing console.
    [INFO   ] libLSS version v2.0.0alpha-412-g62e3c165 built-in modules ares_fg;borg;hades;hmclet;python
    [INFO   ] Registering data IO converters
    [INFO S ] Registered forward models:
    [INFO S ]   - 2LPT_CIC
    [INFO S ]   - 2LPT_CIC_OPENMP
    [INFO S ]   - 2LPT_DOUBLE
    [INFO S ]   - 2LPT_NGP
    [INFO S ]   - ALTAIR_AP
    [INFO S ]   - Downgrade
    [INFO S ]   - EnforceMass
    [INFO S ]   - HADES_LOG
    [INFO S ]   - HADES_PT
    [INFO S ]   - Haar
    [INFO S ]   - LPT_CIC
    [INFO S ]   - LPT_CIC_OPENMP
    [INFO S ]   - LPT_DOUBLE
    [INFO S ]   - LPT_NGP
    [INFO S ]   - LPT_NU_CIC
    [INFO S ]   - LPT_NU_CIC_OPENMP
    [INFO S ]   - PATCH_MODEL
    [INFO S ]   - PM_CIC
    [INFO S ]   - PM_CIC_OPENMP
    [INFO S ]   - PRIMORDIAL
    [INFO S ]   - PRIMORDIAL_AS
    [INFO S ]   - PRIMORDIAL_FNL
    [INFO S ]   - QLPT
    [INFO S ]   - QLPT_RSD
    [INFO S ]   - Softplus
    [INFO S ]   - TRANSFER_CLASS
    [INFO S ]   - TRANSFER_EHU
    [INFO S ]   - Transfer
    [INFO S ]   - Upgrade
    [INFO S ]   - bias::BrokenPowerLaw
    [INFO S ]   - bias::DoubleBrokenPowerLaw
    [INFO S ]   - bias::EFT
    [INFO S ]   - bias::EFT_Thresh
    [INFO S ]   - bias::Linear
    [INFO S ]   - bias::ManyPower_1^1
    [INFO S ]   - bias::ManyPower_1^2
    [INFO S ]   - bias::ManyPower_1^4
    [INFO S ]   - bias::ManyPower_2^2
    [INFO S ]   - bias::Noop
    [INFO S ]   - bias::PowerLaw
    [INFO S ] Registered forward models (v3):
    [INFO S ]   - SphereProjection
    [INFO S ] Registered likelihoods:
    [INFO S ]   - BORG_POISSON
    [INFO S ]   - EFT_BIAS_LIKE
    [INFO S ]   - GAUSSIAN_BROKEN_POWER_LAW_BIAS
    [INFO S ]   - GAUSSIAN_LINEAR_BIAS
    [INFO S ]   - GAUSSIAN_MO_WHITE_BIAS
    [INFO S ]   - GAUSSIAN_POWER_LAW_BIAS
    [INFO S ]   - GENERIC_GAUSSIAN_MANY_POWER_1^1
    [INFO S ]   - GENERIC_GAUSSIAN_MANY_POWER_1^2
    [INFO S ]   - GENERIC_GAUSSIAN_MANY_POWER_1^4
    [INFO S ]   - GENERIC_POISSON_BROKEN_POWERLAW_BIAS
    [INFO S ]   - GENERIC_POISSON_MANY_POWER_1^1
    [INFO S ]   - GENERIC_POISSON_MANY_POWER_1^2
    [INFO S ]   - GENERIC_POISSON_MANY_POWER_1^2_DEGRADE4
    [INFO S ]   - GENERIC_POISSON_MANY_POWER_1^4
    [INFO S ]   - GENERIC_POISSON_MANY_POWER_2^2
    [INFO S ]   - GENERIC_POISSON_MANY_POWER_2^2_DEGRADE4
    [INFO S ]   - GENERIC_POISSON_MANY_POWER_2^4
    [INFO S ]   - GENERIC_POISSON_MANY_POWER_4^1
    [INFO S ]   - GENERIC_POISSON_MO_WHITE_BIAS
    [INFO S ]   - GENERIC_POISSON_POWERLAW_BIAS
    [INFO S ]   - GENERIC_POISSON_POWERLAW_BIAS_DEGRADE4
    [INFO S ]   - HADES_LINEAR
    [INFO S ]   - POISSON
    [INFO S ]   - ROBUST_POISSON_BROKEN_POWERLAW_BIAS
    [INFO S ]   - ROBUST_POISSON_MANY_POWER_1^1
    [INFO S ]   - ROBUST_POISSON_MANY_POWER_1^2
    [INFO S ]   - ROBUST_POISSON_MANY_POWER_2^2
    [INFO S ]   - ROBUST_POISSON_POWERLAW_BIAS
    [INFO   ] CPU features: MMX [!AVX] [!AVX2] SSE SSE2 [!SSE3] [!SSE4.1] [!SSE4.2]


If you want to run |hades| with a likelihood written in python (see
:ref:`how to write a likelihood in python <building_python_likelihood_script>`),
then you also need to add the flag ``--hades-python`` when executing ``build.sh``.
This requirement will probably go away later. The command is:

.. code:: bash

    bash build.sh --use-predownload --python --hades-python

.. _python_installation_with_mpi:

Configuration and compilation with mpi
--------------------------------------

If you want to compile with both MPI support and python binding,
then you will need the mpi4py python package (if mpi4py is not found
in your python environment, then compilation
will proceed without error; see the :ref:`previous section <python_installation_without_mpi>`).
It is important to use a mpi4py package compiled with the **same MPI
framework** as |ares|. Not doing so will very likely result in a
segmentation fault when importing borg from python.

To use the same compilers for mpi4py and |ares|, the safest thing
is to install these compilers and mpi4py using your python environment
manager. For example, to install gcc, g++ and mpi4py on a Linux system
using anaconda:

.. code:: bash

    conda install gcc_linux-64 gxx_linux-64 mpi4py

(the executables added to your environment called ``x86_64-conda_cos6-linux-gnu-gcc`` and
``x86_64-conda_cos6-linux-gnu-g++``).

You can then use the ``build.sh`` script, using the simultaneously the flags
``--python``, ``--hades-python`` (if desired) and ``--with-mpi``, and specifying
the compiler that you have just installed. The full command should be

.. code:: bash

    bash build.sh --c-compiler $(which x86_64-conda_cos6-linux-gnu-gcc) --cxx-compiler $(which x86_64-conda_cos6-linux-gnu-g++) --install-system-python --python=$(which python3) --hades-python --with-mpi


To compile and install, go to your build directory and type ``make python-install`` (or ``make install``, see the :ref:`previous section <python_installation_without_mpi>`):

.. code:: bash

    cd build
    make python-install

A succesfull import of the python module with MPI will look like the following:

.. code:: python3

    >>> import borg
    Initializing console.
    [INFO   ] libLSS version v2.0.0alpha-412-g62e3c165 built-in modules ares_fg;borg;hades;hmclet;python
    [INFO   ] Registering data IO converters
    (...)
    [INFO   ] Found MPI4PY.
    [INFO   ] CPU features: MMX [!AVX] [!AVX2] SSE SSE2 SSE3 [!SSE4.1] [!SSE4.2]


As you can see, there is a line saying "Found MPI4PY".

.. _git_procedures:

Git procedures and the get-aquila-modules.sh script
===================================================

.. _general_checkup_management:

General checkup / management
----------------------------

The status of all git repositories can be obtained using the ``get-aquila-modules.sh``
script, using

.. code:: bash

    bash get-aquila-modules.sh --status

If the git states are clean, the output should look like this:

.. code:: text

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    This script can be run only by Aquila members.
    if your bitbucket login is not accredited the next operations will fail.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Checking GIT status...

    Root tree	 (branch main) : good. All clear.
    Module ares_fg	(branch master) : good. All clear.
    Module borg	(branch main) : good. All clear.
    Module hades	(branch main) : good. All clear.
    Module hmclet	(branch main) : good. All clear.
    Module python	(branch main) : good. All clear.

    Checking whether git hooks are there... It looks so. Skipping.

Pulling updates
---------------

The ``get-aquila-modules.sh`` can be used to pull updates from the |ares|
repository and the Aquila module repositories, using the following command

.. code:: bash

    bash get-aquila-modules.sh --pull

Please check the full usage, obtained with ``bash get-aquila-modules.sh --help`` or
``bash get-aquila-modules.sh -h``:

.. code:: text

    This is the get-aquila-module helper script. It clones and updates the modules
    common to the collaboration. By default it uses SSH protocol. We advise using
    a SSH key to avoid being asked too much about your password.

    --hooks            Setup git hooks
    --clone            Clone missing modules
    --https USER       Use HTTPS protocol instead of SSH
    --pull             Pull all the modules
    --push             Push all modules to origin (including root)
    --help             This information message
    --send-pack H D R  Send packs of ref R to remote ssh server (host H, directory D)
    --local-merge      Use local GIT database to merge the reference to checked
                    out branches
    --update-copyright Update copyright notices
    --update-indent    Update indentation
    --status           Show git status
    --report           Create a git version report for reproductibility
    --branch-set       Setup branches of default modules
                    directory D (including modules)
    --set-branch       synonym for --branch-set
    --purge            Purge the content of modules (DANGEROUS)

    Developer tools:
    --tags             Tags all submodule with current version in ARES

    Experimental / not working:
    --work-tree B D    Create a complete worktree of branch B in

.. _git_submodules:

Git submodules
--------------

:ref:`Extra modules <extras>` for |ares| have their own git repository, but they
are not *submodules* (in the git sense) of the main |ares| repository.
Therefore, you should not have to touch the ``.gitmodules`` files. By default,
it contains the following:

.. code:: bash

    [submodule "external/cosmotool"]
        path = external/cosmotool
        url = https://bitbucket.org/glavaux/cosmotool.git
        ignore = dirty

To synchronize and update submodules, use:

.. code:: bash

    git submodule sync
    git submodule update --init --recursive


.. _frequently_encountered_problems_fep:

Frequently Encountered Problems (FEP)
=====================================

.. _mpi_compiler_with_dependency:

MPI compiler with dependency
----------------------------

Problem
~~~~~~~

There is a potential pitfall when using some MPI C compilers, if they
have been installed by system administrators to work by default with
another compiler. In particular, the Intel C compiler requires basic
infrastructure provided by GCC. Your default environment may have a
very old GCC (e.g. GCC 4.x) and thus a modern Intel Compiler (19 or 20)
would be using old GCC libraries, resulting in compilation failure.

Solution
~~~~~~~~

In this case, you should load a modern gcc compiler first (gcc ≥ 7)
and then load the Intel compiler. You can see the version of gcc that
is used by icc and check the compatibility using

.. code:: bash

    icc -v

You may also have to force the use of the correct the MPI C compiler
with an environment variable, otherwise you risk having
inconsistent compiled code and errors when building final executables.
Check the following message at the end of configuration:

.. code:: text

   Please check the configuration of your MPI C compiler. You may need
   to set an environment variable to use the proper compiler.
   Some example (for SH/BASH shells):
      OpenMPI:
      OMPI_CC=cc
      OMPI_CXX=c++
      export OMPI_CC OMPI_CXX


.. _permissions_quota_etc:

Permissions and quota
---------------------

Problem
~~~~~~~

Some HPC facilities have a peculiar quota system (e.g. TGCC in
France): you have to belong to a group to get access to full disk quota.

Solution
~~~~~~~~

You can switch groups using

.. code:: bash

    newgrp name_of_the_group

and then execute all commands in the spawn shell.

.. _non_linked_files:

External dependencies not found (cfitsio)
-----------------------------------------

Problem
~~~~~~~

After transferring to a supercluster, ``build.sh`` may complain about not
finding external dependencies such as ``external/cfitsio``. The output
contains something like:

.. code:: text

    configure: error: could not find the cfitsio library
    CMake Error at [..]/external/configure_healpix.cmake:23 (MESSAGE):
        Cannot execute configure command

    CMakeFiles/healpix.dir/build.make:106: recipe for target
    'external_build/healpix-prefix/src/healpix-stamp/healpix-configure' failed

Solution
~~~~~~~~

Purging all the ``*.o`` and ``*.a`` in ``external/cfitsio``, and forcing
a rebuild of libcfitsio by removing the file
``build/external_build/cfitsio-prefix/src/cfitsio-stamp/cfitsio-build``
and typing ``make``.

MPI_CXX not found
-----------------

Problem
~~~~~~~

MPI_C is found but MPI_CXX is not found by cmake. The output of ``build.sh``
contains something like:

.. code:: text

   -- Found MPI_C: /path/to/libmpi.so (found version "3.1")
   -- Could NOT find MPI_CXX (missing: MPI_CXX_WORKS)
   -- Found MPI_Fortran: /path/to/libmpi_usempif08.so (found version "3.1")

Solution
~~~~~~~~

You probably have two versions of MPI (the one you intend to use, e.g.
your installation of OpenMPI) and one which pollutes the environment
(e.g. from your anaconda). Therefore the compilation of the MPI C++ test
program (``build/CMakeFiles/FindMPI/test_mpi.cpp``) by cmake fails. To
troubleshoot:

* Check the commands that defined your environment variables using

  .. code:: bash

    set | grep -i MPI

* Check the paths used in ``CPATH``, ``CPP_FLAGS``, etc. for spurious
  MPI headers (e.g. ``mpi.h``)
* Control the file ``build/CMakeFiles/CMakeError.txt`` if it exists

.. _external_hdf5_not_found:

External HDF5 not found
-----------------------

Problem
~~~~~~~

When running ``build.sh`` (particularly with the flag
``--use-system-hdf5``), cmake gives errors such as

.. code:: text

   CMake Error: The following variables are used in this project, but they are set to NOTFOUND.
   Please set them or make sure they are set and tested correctly in the CMake files:
   HDF5_CXX_INCLUDE_DIR (ADVANCED)

   CMake Error in libLSS/CMakeLists.txt:
     Found relative path while evaluating include directories of "LSS":

       "HDF5_CXX_INCLUDE_DIR-NOTFOUND"

Solution
~~~~~~~~

*  HDF5 must be compiled with the flags ``--enable-shared`` and
   ``--enable-cxx``.
*  The environment variable ``HDF5_ROOT`` must point to the HDF5 prefix
   directory, and cmake should use it from version 3.12 (see also `cmake
   policy CMP0074 <https://cmake.org/cmake/help/latest/policy/CMP0074.html>`_ and commit `2ebe5e9 <https://bitbucket.org/bayesian_lss_team/ares/commits/2ebe5e9c323e30ece0caa124a0b705f3b1241273>`__ in |ares|).
