Running HADES
=============

Hades3 is built at the same time as ares3. The final binary is located
in ``$BUILD/src/hades3``, which is the main HADES3 program. Again typing
``$BUILD/src/hades3`` should give the following output:

.. code:: text

    setupMPI with threads
    Initializing console.
    [0/1] [DEBUG  ] INIT: MPI/FFTW
    [STD    ]
    [STD    ]                     /\_/\____,          ____________________________
    [STD    ]           ,___/\_/\ \  ~     /                       HADES3
    [STD    ]           \     ~  \ )   XXX
    [STD    ]             XXX     /    /\_/\___,     (c) Jens Jasche 2012 - 2017
    [STD    ]                \o-o/-o-o/   ~    /        Guilhem Lavaux 2014 - 2017
    [STD    ]                 ) /     \    XXX        ____________________________
    [STD    ]                _|    / \ \_/
    [STD    ]             ,-/   _  \_/   \
    [STD    ]            / (   /____,__|  )
    [STD    ]           (  |_ (    )  \) _|
    [STD    ]          _/ _)   \   \__/   (_
    [STD    ]         (,-(,(,(,/      \,),),)
    [STD    ] Please acknowledge XXXX
    [0/1] [DEBUG  ] INIT: FFTW/WISDOM
    [0/1] [INFO   ] Starting HADES3. rank=0, size=1
    [0/1] [INFO   ] ARES3 base version c9e74ec93121f9d99a3b2fecb859206b4a8b74a3
    [0/1] [ERROR  ] HADES3 requires exactly two parameters: INIT or RESUME as first parameter and the configuration file as second parameter.
