Inputs
######

.. include:: inputs/Configuration_file_v1.inc.rst
.. include:: inputs/Configuration_file_v2.inc.rst
.. include:: inputs/Configuration_file_v2.1.inc.rst
.. include:: inputs/Configuration_file_v2.2.inc.rst
.. include:: inputs/Create_config-file.inc.rst
.. include:: inputs/Text_catalog_format.inc.rst
.. include:: inputs/HDF5_catalog_format.inc.rst
.. include:: inputs/Radial_selection.inc.rst
