Extra modules
#############

.. _extras:

List of available ARES modules
==============================

.. sectionauthor:: Florent Leclercq, Guilhem Lavaux (last update: 19 May 2021)

|ares| is the parent project of many sub-projects and sub-modules. This section provides an overview on these modules, and their documentation follows.

- `hades <https://bitbucket.org/bayesian_lss_team/ares_hades/>`__ :
  this module declares and implements some of the fundamental API
  for manipulating general likelihoods and deterministic forward models in the
  ARES/BORG framework. In particular, samplers like the
  Hamiltonian Markov Chain algorithm are implemented there.
- `borg <https://bitbucket.org/bayesian_lss_team/ares_hades/>`__:
  this module deals with physical aspect and the statistics
  of the large-scale structure. In particular, it holds the code for implementing
  first and second-order Lagrangian perturbation theory, and the particle mesh
  (with tCOLA) model.
- `virbius <https://bitbucket.org/bayesian_lss_team/ares_virbius/>`__:
  this module is related to the inference of cosmological velocity fields, as
  described in `Lavaux (2016) <https://arxiv.org/abs/1512.04534>`__. Additional contributions from Florian Führer.
  As of 19 May 2021, the module does not compile and needs fixing.
- `ares_fg <https://bitbucket.org/bayesian_lss_team/ares_fg/>`__:
  this module contains code to deal with foregrounds in |ares|, as described
  in `Jasche & Lavaux (2017) <https://arxiv.org/abs/1706.08971>`__.
- `dm_sheet <https://bitbucket.org/bayesian_lss_team/dm_sheet/>`__:
  this module implements the Simplex-in-Cell (SIC) estimator (`Abel et al. 2011 <https://arxiv.org/abs/1111.3944>`__, `Hahn et al. 2015 <https://arxiv.org/abs/1404.2280>`__, `Leclercq et al. 2017 <https://arxiv.org/abs/1601.00093>`__) for density and velocity fields. It will be embedded in BORG from release 2.2.
  Its documentation is :ref:`here <extras_dm_sheet>`.
- `python <https://bitbucket.org/bayesian_lss_team/ares_python/src>`__:
  this modules implements the python bindings, both as an external
  module for other python virtual machines (VM), or with an embedded python VM
  to interpret likelihoods and configuration written in python.
  Its documentation is :ref:`here <extras_python>`.
- `hmclet <https://bitbucket.org/bayesian_lss_team/hmclet/src/main/>`__:
  this module handles simplified low-dimensional likelihoods, for which
  dense mass matrices can be stored in memory. Additionally, it defines the
  ``hades_julia`` tool to write likelihoods in the Julia language. Its documentation
  is :ref:`here <extras_hmclet>`.

.. include:: extras/dm_sheet.inc.rst
.. include:: extras/python.inc.rst
.. include:: extras/hmclet.inc.rst
