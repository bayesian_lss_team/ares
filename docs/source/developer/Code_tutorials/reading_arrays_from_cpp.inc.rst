.. _reading_in_meta_parameters_and_arrays:

Reading in meta-parameters and arrays
=====================================

If one wishes to access the the content of ARES MCMC files in C++,
functions are available in CosmoTool and LibLSS. For example:

.. code:: c++

   #include <iostream>
   #include <boost/multi_array.hpp> //produce arrays
   #include "CosmoTool/hdf5_array.hpp" //read h5 atributes as said arrays
   #include "libLSS/tools/hdf5_scalar.hpp" //read h5 attributes as scalars
   #include <H5Cpp.h> //access h5 files

   using namespace std;
   using namespace LibLSS;

   int main()
   {
       typedef  boost::multi_array<double, 3> array3_type;

       //access mcmc and restart files
       H5::H5File meta("restart.h5_0", H5F_ACC_RDONLY);
       H5::H5File f("mcmc_0.h5", H5F_ACC_RDONLY);

       //read the number of pixels of the cube as integrer values (x,y,z)
       int N0 = LibLSS::hdf5_load_scalar<int>(meta, "scalars/N0");
       int N1 = LibLSS::hdf5_load_scalar<int>(meta, "scalars/N1");
       int N2 = LibLSS::hdf5_load_scalar<int>(meta, "scalars/N2");

       array3_type density(boost::extents[N0][N1][N2]);

       //read the density field as a 3d array
       CosmoTool::hdf5_read_array(f, "scalars/s_field", density);
   }
