.. _obtaining_timing_statistics:

Obtaining timing statistics
===========================

By default the statistics are not gathered. It is possible (and advised
during development and testing) to activate them through a build.sh
option ``--perf``. In that case, each "ConsoleContext" block is timed
separately. In the C++ code, a console context behaves like this:

.. code:: c++

   /* blabla */
   {
     LibLSS::ConsoleContext<LOG_DEBUG> ctx("costly computation");

     /* Computations */
     ctx.print("Something I want to say");
   } /* Exiting context */
   /* something else */

Another variant that automatically notes down the function name and the
filename is

.. code:: c++

   /* blabla */
   {
      LIBLSS_AUTO_CONTEXT(LOG_DEBUG, ctx);
     /* Computations */
     ctx.print("Something I want to say");
   } /* Exiting context */
   /* something else */

A timer is started at the moment the ConsoleContext object is created.
The timer is destroyed at the "Exiting context" stage. The result is
marked in a separate hash table. Be aware that in production mode you
should turn off the performance measurements as they take time for
functions that are called very often. You can decide on a log level
different than LOG_DEBUG (it can be LOG_VERBOSE, LOG_INFO, ...), it is
the default level for any print call used with the context.

The string given to console context is used as an identifier, so please
use something sensible. At the moment the code gathering performances is
not aware of how things are recursively called. So you will only get one
line per context. Once you have run an executable based on libLSS it
will produce a file called "timing_stats.txt" in the current working
directory. It is formatted like this:

.. code:: text

   Cumulative timing spent in different context
   --------------------------------------------
   Context,   Total time (seconds)

                             BORG LPT MODEL        2       0.053816
                      BORG LPT MODEL SIMPLE        2       0.048709
                         BORG forward model        2       0.047993
                     Classic CIC projection        2       0.003018
   (...)

It consists in three columns, separated by a tab. The first column is
the name of the context. The second column is the number of times this
context has been called. The last and third column is the cumulative
time taken by this context, in seconds. At the moment the output is not
sorted but it may be in future. You want the total time to be as small
as possible. This time may be large for two reasons: you call the
context an insane amount of time, or you call it a few times but each
one is very costly. The optimization to achieve is then up to you.
