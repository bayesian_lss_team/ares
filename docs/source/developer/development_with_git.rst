.. _development_with_git:

Development with git
====================

In case you are not familiar with the git version control system please
also consult the corresponding tutorial on git for bitbucket/atlassian
`here <https://www.atlassian.com/git/tutorials/what-is-version-control>`__.

In the following we will assume that your working branch is called
"my_branch". In addition the "master" branch should reflect the "master"
of the "blss" repository (the reference repository). Further in the
following we will consider the ARES main infrastructure here.

Slides of the tutorial
----------------------

See `this file <https://www.aquila-consortium.org/wiki/index.php/File:ARES_git.pdf>`__.

Finding the current working branch
----------------------------------

.. code:: bash

   git branch

Branching (and creating a new branch) from current branch
---------------------------------------------------------

.. code:: bash

   git checkout -b new_branch

This will create a branch from current state move to the new branch
"new_branch"

Setting up remote
-----------------

First we add the remote:

.. code:: bash

   git remote add blss git@bitbucket.org:bayesian_lss_team/ares.git

Next we can fetch:

.. code:: bash

   git fetch blss

Pulling updates
---------------

Be sure that you are in the master branch

.. code:: bash

   git checkout master

Pull any updates from blss

.. code:: bash

   git pull blss master

Here you may get merge problem due to submodules if you have touched the
.gitmodules of your master branch. In that case you should revert the
.gitmodules to its pristine status:

.. code:: bash

   git checkout blss/master -- .gitmodules

This line has checked out the file .gitmodules from the blss/master
branch and has overwritten the current file.

And then do a submodule sync:

.. code:: bash

   git submodule sync

And an update:

.. code:: bash

   git submodule update

Now your master branch is up to date with blss. You can push it to
bitbucket:

.. code:: bash

   git push

This will update the master branch of *your fork* on bitbucket. Now you
can move to your private branch (e.g. "my_branch").

Rebase option for adjusting
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rebasing is better if you intend to create a pull request for the
feature branch to the master. That ensures that no spurious patch will
be present coming from the main branch which would create a merge
conflict.

Now you can rebase your branch on the new master using:

.. code:: bash

   git rebase master

Merging option
~~~~~~~~~~~~~~

If you want to merge between two branches (again you should not merge
from master to avoid polluting with extra commits):

.. code:: bash

   git merge other_branch

Pushing modifications, procedures for pull requests
---------------------------------------------------

Cherry picking
~~~~~~~~~~~~~~

It is possible to cherry pick commits in a git branch. Use "git
cherry-pick COMMIT_ID" to import the given commit to the current branch.
The patch is applied and directly available for a push.

Procedure for a pull request
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section describes the procedure of how to create your own developer
branch from the ARES master repository. Go to the master branch (which
should reflect BLSS master branch):

.. code:: bash

   git checkout blss/master

Create a branch (e.g. 'your_branch') with:

.. code:: bash

   git checkout -b your_branch

Import commits, either with git merge:

.. code:: bash

   git merge your_branch

or with cherry-picking:

.. code:: bash

   git cherry-pick this_good_commit
   git cherry-pick this_other_commit

where this_good_commit and this_other_commit refer to the actual commits
that you want to pick from the repository

Push the branch:

.. code:: bash

   git push origin your_branch

and create the pull request.

Please avoid at maximum to contaminate the pull request with the
specificity of your own workspace (e.g. gitmodules update etc).

Using tags
----------

To add a tag locally and push it:

.. code:: bash

   git tag <tagname>
   git push --tags

To delete a local tag:

.. code:: bash

   git tag --delete >tagname>

To delete a remote tag:

.. code:: bash

   git push --delete <remote> <tagname>

or

.. code:: bash

   git push <remote> :<tagname>

Reference: this `stackoverflow question <https://stackoverflow.com/questions/5480258/how-to-delete-a-remote-tag>`_.

.. _archivingrestoring_a_branch:

Archiving/restoring a branch
----------------------------

The proper way to do archive a branch is to use tags. If you delete the
branch after you have tagged it then you've effectively kept the branch
around but it won't clutter your branch list. If you need to go back to
the branch just check out the tag. It will effectively restore the
branch from the tag.

To archive and delete the branch:

.. code:: bash

   git tag archive/<branchname> <branchname>
   git branch -D <branchname>

To restore the branch some time later:

.. code:: bash

   git checkout -b <branchname> archive/<branchname>

The history of the branch will be preserved exactly as it was when you
tagged it. Reference: this `stackoverflow question <https://stackoverflow.com/questions/1307114/how-can-i-archive-git-branches>`__.

The pre-commit git hook
-----------------------

.. sectionauthor:: Florent Leclercq, Guilhem Lavaux (last update: 21 May 2021)

The ``get-aquila-modules.sh`` script sets up a git hook to enforce quality standards for the code
that is committed to the |ares| repository.

ASCII filenames
~~~~~~~~~~~~~~~

The hook will first check that all your file names are in ASCII format. If it's not the case, you will have to fix this by hand.

Bad whitespaces
~~~~~~~~~~~~~~~

* **Trailing whitespaces**. Using the ``git diff-index`` command, the hook will then check if your files contain bad whitespaces, such as *trailing whitespaces* (i.e. extra spaces or tabs at the end of lines). To automatically remove trailing whitespaces from a given file, you can use (on Linux):

  .. code:: bash

      sed -i 's/[ \t]*$//' FILE

  and on MAC OSX:

  .. code:: bash

      sed -i '' -E 's/[ '$'\t'']+$//' FILE

  (reference: this `stackoverflow question <https://unix.stackexchange.com/questions/15308/how-to-use-find-command-to-search-for-multiple-extensions>`__). The hook will write the list of files with trailing whitespaces in ``files_with_trailing_whitespace_errors.hook.txt``. You can then try to run the following command (it is also printed by the hook) at the root of your git worktree before committing again: on Linux:

  .. code:: bash

      while read -r filename; do sed -i 's/[ \t]*$//' $filename ; git add -u $filename ; done < files_with_trailing_whitespace_errors.hook.txt ; rm -f files_with_trailing_whitespace_errors.hook.txt

  and on MAC OSX:

  .. code:: bash

      while read -r filename; do sed -i '' -E 's/[ '$'\t'']+$//' $filename ; git add -u $filename ; done < files_with_trailing_whitespace_errors.hook.txt ; rm -f files_with_trailing_whitespace_errors.hook.txt

  You should also check the configuration of your IDE: they will usually include an option to automatically remove trailing whitespaces when saving. For instance, with `Kate <https://kate-editor.org>`_, you should go to Settings > Configure Kate > Open/Save and set Remove trailing spaces: In Entire Document (this `see page <https://kate-editor.org/2012/10/27/remove-trailing-spaces/>`__). This `stackoverflow question <https://stackoverflow.com/questions/44482062/removing-trailing-spaces-with-clang-format>`__ gives the procedure for some other IDEs.

* **Blank lines at EOF**. The hook will also check for files with several blank lines at the end-of-file (EOF). To automatically remove these lines from a given file, you can use

  .. code:: bash

      sed -i -e '/./,$!d' -e :a -e '/^\n*$/{$d;N;ba' -e '}' FILE

  (reference: this `stackexchange question <https://unix.stackexchange.com/questions/552188/how-to-remove-empty-lines-from-begin-and-end-of-file>`__; the MAC OS syntax may differ). The hook will write the list of files with trailing whitespaces in ``files_with_EOF_errors.hook.txt``. You can then try to run the following command (it is also printed by the hook) at the root of your git worktree before committing again:

  .. code:: bash

      while read -r filename; do sed -i -e '/./,$!d' -e :a -e '/^\n*$/{$d;N;ba' -e '}' $filename ; git add -u $filename ; done < files_with_EOF_errors.hook.txt ; rm -f files_with_EOF_errors.hook.txt

* **Other whitespace errors**. You may get further whitespace errors, for instance if you mix tabulations and whitespaces. You will have to fix those by hand.

clang formatting
~~~~~~~~~~~~~~~~

Finally the hook checks the formatting of your ``*.cpp`` and ``*.hpp`` files using ``clang-format``. On GNU/Linux systems, it is available in usual package repositories: if you have root access, type

.. code:: bash

    sudo apt-get install clang-format

In other cases, you can download static binaries of clang-format `here <https://aur.archlinux.org/packages/clang-format-static-bin/>`__.

To pass the syntax validation check when committing, you will usually have to pre-process your files using the following command:

.. code:: bash

    clang-format -style=style_file -i FILE

The hook will write the list of files with trailing whitespaces in ``files_failing_check.hook.txt``. You can then try to run the following command (it is also printed by the hook) at the root of your git worktree before committing again:

.. code:: bash

    while read -r filename; do clang-format -style=file -i $filename ; git add -u $filename ; done < files_failing_check.hook.txt ; rm -f files_failing_check.hook.txt
