Code tutorials
##############

.. include:: Code_tutorials/Types.inc.rst

.. include:: Code_tutorials/FFTW_manager.inc.rst

.. include:: Code_tutorials/reading_arrays_from_cpp.inc.rst

.. include:: Code_tutorials/timing_stats.inc.rst

.. include:: Code_tutorials/CPP_Multiarray.inc.rst

.. include:: Code_tutorials/Julia_and_TensorFlow.inc.rst

.. include:: Code_tutorials/New_core_program.inc.rst

.. include:: Code_tutorials/Adding_a_new_likelihood_in_C++.inc.rst

.. .. include:: Code_tutorials/tiled_arrays.inc.rst

Adding a new likelihood/bias combination in BORG
================================================

*To be written...*

Useful resources
================

-  `Google code of conduct in C++ <https://google.github.io/styleguide/cppguide.html>`__
