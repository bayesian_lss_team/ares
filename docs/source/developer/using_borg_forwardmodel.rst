.. _borg_forward_model:

The ForwardModel interface
##########################

Rationale
=========

The whole concept of BORG relies on the capabilities of running differentiable
simulations. The physical effects that must be encoded in these simulations are
very varied and can be solved at different orders of approximations. Thus
writing a single solver that would do everything from end to end is not
practical (nor actually necessary). We adopt the strategy of moving blocks which
are all black boxes from each other. These moving blocks which can be chained at
will provided that the output can be plugged in the input of the next one. There
a default single input and single output scheme implemented. However, more could
be added though the API becomes less transparent.

Three versions of the API has been implemented. The BORG 2.0 and 2.1 focused on
stabilizing and deepening the use of the version 2, which was still limited in
handling IOs that were mostly three-dimensional. BORG 2.2 introduces the third
version for which no such restriction exists. The version 3 is compatible with version 2
in the sense that the entry-points for version 2 still exist and thin compatibility layer
has been introduced for program that make use of only the version 3. To summarize:

* if a forward model is written for version 2, it will still work and supports automatically
  version 3 entry-points;
* if a forward model is written for version 3, it will not be possible to call it with version 2 entry points.


Relevant headers
================

The main header for the definition of the API is
`libLSS/physics/forward_model.hpp
<https://bitbucket.org/bayesian_lss_team/ares_hades/src/main/libLSS/physics/forward_model.hpp>`_
(located in ``extra/hades``).

The definition of ``ModelIO<N>`` classes are in `libLSS/physics/model_io.hpp
<https://bitbucket.org/bayesian_lss_team/ares_hades/src/main/libLSS/physics/model_io.hpp>`_.

The definition of data representations is in
``libLSS/physics/data/data_representation.hpp``. The corresponding general model
io that replaces ``ModelIO`` is in `libLSS/physics/general_model_io.hpp
<https://bitbucket.org/bayesian_lss_team/ares_hades/src/main/libLSS/physics/general_model_io.hpp>`_.

Executing a model under API v2
==============================

*This is deprecated in main.*

Here we will assume that ``forward`` is properly constructed
``LibLSS::BORGForwardModel`` with type
`std::shared_ptr<LibLSS::BORGForwardModel>
<https://www.cplusplus.com/reference/memory/shared_ptr/>`_.

Running the forward model on an array ``a``, that we define here-below, is just a matter of having this line in your code:

.. code:: c++

  using namespace LibLSS;

  // mgr is a FFTW_Manager<3>  cf. libLSS/tools/mpi_fftw_helper.hpp
  // box is a BoxModel ~ LibLSS::NBoxModel<3>   cf. libLSS/physics/model_io/box.hpp
  // For example:
  auto mgr = forward->lo_mgr;  // lo_mgr is part of the v2 API, it is the input FFTW Manager
  auto box = forward->get_box_model(); // this obtains the input box of the model.
  auto a_p = mgr->allocate_array();  // We allocate some temporary array.
  auto& a = a_p.get_array();  // Obtain the boost::multi_array representation

  // Fill `a` with something (Beware of MPI limits!)

  // Now we run
  forward->forwardModel_v2(ModelInput<3>(mgr, box, a));

  // Same as above, but for the output.
  auto out_mgr = forward->out_mgr;
  auto box_output = forward->get_box_model_output();
  auto b_p = out_mgr->allocate_array();
  auto& b = b_p.get_array();

  // Now we request the output density from the forward model.
  forward->getDensityFinal(ModelOutput<3>(out_mgr, box_output, b));


As you can see, most of the lines are about allocating the arrays but not
running the forward model and getting the result. There is a possibility to run
a forward model even *without* any input data. This can happen in some rare
occasions where the information is transmitted by other means. In that case, the
following piece of code is sufficient:

.. code:: c++

  forward->forwardModel_v2(ModelInput<3>());

Of course, if the model actually requires data, you will trigger different sort of exceptions/errors in return.

The same procedure can be followed for computing the adjoint Jacobian matrix
application to an adjoint gradient. The difference is that ``ModelInputAdjoint``
must be used instead of ``ModelInput`` and ``ModelOutputAdjoint`` replaces
``ModelOutput``. The two member functions concerned by the adjoint model
evaluation are ``adjointModel_v2`` and ``getAdjointModelOutput``.

Executing a model under API v3
==============================


Running a model under the version 3 of the API implies going through one more
abstraction level. The spirit of APIv3 is that there is no direct access to the
underlying data. One has to go through a representation which may be transformed
to something more acceptable by the forward model, or the user. As an example,
one may start with a standard slabbed array as it is handled by
``FFTW_Manager<N>`` and ``ModelIO``, but the forward model requires a tiled
array. The slabbed array may even be a Fourier representation of the array if we wish so.

.. (cf tutorial :ref:`tiled_array`)

Let us assume that ``a_hat`` is such an array. Then running a forward model requires the following step:

.. code:: c++
  :linenos:

  forward->forwardModel_v3(
    GeneralIO::Input(
      ModelIORepresentation<3>(
        ModelInput<3>(mgr, box, a)
      )
    )
  );

To understand the line, we have to start from inner-most part. First a ``ModelInput<3>`` is created from the array, indicating that it is slabbed and with a Fourier representation (implicitly from the type of ``a``).

Querying/Setting parameters of the model
========================================

General case
------------

Special case: cosmology
-----------------------
