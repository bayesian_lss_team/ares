.. default-domain:: py

{{ name | escape | underline}}

.. currentmodule:: {{ module }}

.. auto{{ objtype }}:: {{ module }}.{{ objname }}
