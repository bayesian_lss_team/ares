|ares| is the main component of the Bayesian Large Scale Structure inference
pipeline. The present version of the ARES framework is 2.1. Please consult
:ref:`the Release notes <release_notes>` for an overview of the different improvements over the
different versions.

|ares| is written in C++14 and has been parallelized with OpenMP and MPI. It currently compiles with major compilers (gcc, intel, clang).

|build_status| |made_with| |rtd|

Table of contents
-----------------

.. toctree::
   :maxdepth: 1
   :caption: Theory

   theory/ARES
   theory/BORG
   theory/ARES&BORG_FFT_normalization

.. toctree::
   :maxdepth: 1
   :caption: User documentation

   user/versions
   user/building
   user/inputs
   user/outputs
   user/running
   user/postprocessing
   user/extras
   user/clusters

.. toctree::
   :maxdepth: 1
   :caption: Developer documentation

   developer/development_with_git
   developer/code_architecture
   developer/using_borg_forwardmodel
   developer/mpi_in_borg
   developer/code_tutorials
   developer/contributing_to_this_documentation
   developer/copyright_and_authorship

.. toctree-filt::
   :maxdepth: 1
   :caption: Python reference documentation

   :aquila:pythonref.rst

Citing
------

If you are using |ares| for your project, please cite the following articles for ARES2, ARES3 and BORG3:

* Jasche, Kitaura, Wandelt, 2010, MNRAS, 406, 1 (arxiv 0911.2493)
* Jasche & Lavaux, 2015, MNRAS, 447, 2 (arxiv 1402.1763)
* Lavaux & Jasche, 2016, MNRAS, 455, 3 (arxiv 1509.05040)
* Jasche & Lavaux, 2019, A&A, 625, A64 (arxiv 1806.11117)

|hades| and |borg| papers have a different listing.

For a full listing of publications from the Aquila consortium, please check the
`Aquila website <https://aquila-consortium.org/publications/>`_.

Acknowledgements
----------------

This work has been funded by parts by the following grants and institutions over the
years:

* The DFG cluster of excellence "Origin and Structure of the Universe"
  (http://www.universe-cluster.de). The Excellence cluster funded the salaries of
  a few individual ona case by case basis, as well as the buying of equipments;
* Institut Lagrange de Paris (grant ANR-10-LABX-63, http://ilp.upmc.fr) within
  the context of the Idex SUPER subsidized by the French government through
  the Agence Nationale de la Recherche (ANR-11-IDEX-0004-02). ILP has funded
  the salaries of some post-doc and PhD student and provided travel grants;
* The BIG4 project funded by the ANR (ANR-16-CE23-0002) (https://big4.iap.fr);
* The "Programme National de Cosmologie et Galaxies" (PNCG, CNRS/INSU) for some of the early developments;
* Through the grant code ORIGIN, it has received support from
  the "Domaine d'Interet Majeur (DIM) Astrophysique et Conditions d'Apparitions
  de la Vie (ACAV)" from Ile-de-France region. It has allowed notably
  access to computational hardware at IAP.
* The Starting Grant (ERC-2015-STG 678652) "GrInflaGal" of the European Research Council.

.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

..  Order of headings used throughout the documentation:

    ######### part
    ********* chapter
    ========= sections
    --------- subsections
    ~~~~~~~~~ subsubsections
    ^^^^^^^^^
    '''''''''


.. |build_status| image:: https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fwww.aquila-consortium.org%2Fjenkins%2Fjob%2Fares_multi_pipeline%2Fjob%2Fmain%2F&style=plastic
  :alt: Build status
  :target: https://www.aquila-consortium.org/jenkins/job/ares_multi_pipeline/job/main/

.. |made_with| image:: https://img.shields.io/badge/Made%20with-C%2B%2B%20%26%20Python-blue
  :alt: Made with C++ & Python

.. |rtd| image:: https://readthedocs.org/projects/aquila-ares/badge/?version=latest&style=flat
   :alt: Check readthedocs.org
   :target: https://aquila-ares.readthedocs.io/en/latest
