#!/bin/sh

LCOV="$1"
GENHTML="$2"

$LCOV --capture --directory . --output-file coverage.info
$LCOV --directory . --output-file coverage.info \
    --remove coverage.info "/usr/**" "**/ext_install/**" ".moc" "test/*"
$GENHTML -o coverage coverage.info
