#+
#   ARES/HADES/BORG Package -- ./examples/prepare_2mpp.py
#   Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015, 2018, 2020)
#
#+
import numpy as np

LIGHTSPEED=299792.458

tmpp = np.load("2m++.npy")

with open("2MPP.txt", mode="wt") as f:
    for i,c in enumerate(tmpp):
        f.write("%d %.15lg %.15lg %.15lg %.15lg %.15lg %.15lg\n" %
            (i,np.radians(c['ra']),np.radians(c['dec']),c['velcmb']/LIGHTSPEED,c['K2MRS'],0,c['best_velcmb']/LIGHTSPEED) )
