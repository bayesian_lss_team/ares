# Examples description

This folder contains example files necessary or running `ares/borg/hades` executables.

## Ini files

The ini files necessary for running the ares and hades code are the
`2mpp_ares.ini` and `2mpp_hades.ini` respectively. Another more complex example
for hades exists in `2mpp-chain.ini`. For description of the additional ini
file parameters take a look at
[aquila-ares.readthedocs.io](https://aquila-ares.readthedocs.io/en/latest/).

## Galaxy dataset

The `2MPP.txt` file contains the galaxy data used in the BORG-PM reconstruction
as discussed in [this paper](https://arxiv.org/abs/1806.11117). The columns are
arranged in the same manner as described in the
[readthedocs](https://aquila-ares.readthedocs.io/en/latest/user/inputs.html?highlight=2M%2B%2B#text-catalog-format)
(**[Galaxy survey]**). It is important to note that the angles are given in
**equatorial** coordinates.
The text file is generated from the original numpy file using `prepare_2mpp.py`

## Example julia likelihood


The folder `julia_lkl` contains an example of an implementation of a likelihood
in the julia language to be invoked with the `hades_julia` executable.
