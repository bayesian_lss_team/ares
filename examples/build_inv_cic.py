import h5py as h5
import numpy as np

N=256
L=677.7

ik=np.fft.fftfreq(N,d=L/N)*2*np.pi
kx,ky,kz=np.meshgrid(ik,ik,ik[:(N//2+1)],indexing='ij')

W= lambda q: (np.sinc(q/(2.*np.pi)*L/N)**2)

cutter = np.ones((N,N,N//2+1))
cutter[np.sqrt(kx**2+ky**2+kz**2) > (2*np.pi*N/L)] = 0

with h5.File("inverse_CIC.h5", mode="w") as ff:
   q = 1/(W(kx)*W(ky)*W(kz))
   ff['transfer'] = (cutter*q).astype(np.complex128)

if False:
  with h5.File("phases.h5", mode="w") as ff:
     ff['phases'] = np.random.randn(N,N,N) * np.sqrt(N**3 / L**6)
