Release notes
-------------

This file only lists the most important changes to each version. We try to follow semantic versioning:

- Major release means API incompatibilities
- Minor release means API compatibilities, but significant feature differences
- Bugfix release only fixes bugs

-------------------------------------------------------------------------------

Release next
~~~~~~~~~~~~

-------------------------------------------------------------------------------

Release 2.2
~~~~~~~~~~~

Code style
^^^^^^^^^^

The code style is now fully enforced. The source code files have updated accordingly.

FFTW API
^^^^^^^^

* The old upgrade/adjoint gradient upgrade has been moved out of
  :code:`FFTW_Manager<T,N>`. If you still use it you have change to do the
  following operation:

.. code-block:: diff

        -    mgr->upgrade_complex(*lo_mgr, deltao, *c_deltao);
        +    UpDeGrade::upgrade_complex(*lo_mgr, *mgr, deltao, *c_deltao);

* The 2d routines have been removed from the 3D api. Please use now
  :code:`FFTW_Manager<T,2>` if you need the 2D plan manager


Load balancing
^^^^^^^^^^^^^^

* New MPI domain decomposition algorithm. It will facilitate massive
  parallelization by allowing the developer to redesign mesh splitting on the
  fly.

Lensing
^^^^^^^

* Lensing model written by N. Porqueres has been merged and adapted.

Python
^^^^^^

* Documentation improvements
* :code:`BORGForwardModel.forwardModel` has been removed. Only
  :code:`forwardModel_v2` available from now on.
* Update to pybind11 v2.6.2
* Bind to load balancing API
* Bind the likelihood registry and samplers builders.
* A new data representation is being developped to allow BORG to leverage DASK/Tensorflow/... parallelisation
  at best performances |in_progress|

Build related
^^^^^^^^^^^^^

* Add new flags ``--install-system-python`` and ``--install-user-python`` to ``build.sh``.
* Introduce the target ``python-install`` to ``make python-install`` without having to compile benchmarks and tests.
* Improvements of the pre-commit git hook.

Core API
^^^^^^^^

* MarkovSampler has a new (still compatible) API to handle ensemble of chains. Samplers that supports
  only one chain at a time do not need any update (i.e. all of them).
* MainLoop now can be provided with more complex task structures to handle multiple chains. It supports
  an explicit dependency support of samplers by name.
* :code`fwrap` supports in place transforms now (e.g. operator :code:`+=`)

Core tools
^^^^^^^^^^

* The tool subdirectory (historically called :code:`src`) has been morphed into
  :code:`tools` and reorganized to avoid source code over-mixing in the same
  namespace.
* :code:`hades_lensing` has joined the crowd of existing tools to manipulate lensing
* Add HADES likelihood dynamic registry. This allows massive simplification of core tools to
  initialize samplers based on ini file.
* HADES3 uses a dynamic system to build algorithm for density sampling now. It fully
  relies on the dependency tracking of MainLoop for building the sampler graph.


Samplers
^^^^^^^^

* Added AO-HMC sampler with support for N chains parallel.
* QN-HMC branch is merged (thanks to Minh Nguyen for testing and improving the algorithm).

BORG
^^^^

* The dm_sheet module has been made obsolete and its content has been moved to BORG.
* BORG PM acquired a new way of projecting particles to grid to leverage a bit better OpenMP parallelization.
* EFT Likelihood and bias has been merged from "fabians/eftlike_improvements".
* New oct likelihood attempt to spread more fairly our prior on the model misspecification

Forward models
^^^^^^^^^^^^^^

* Third version of the IO API. This API supports more exotic topology and ways
  of splitting N-dimensional arrays. PyBORG has been updated to make use of
  this scheme. An example of its use in python is

.. code-block:: python3

        import aquila_borg as borg
        import mpi4py.MPI as MPI

        comm = MPI.COMM_WORLD
        b = borg.forward.BoxModel()

        #x some numpy array which is NxNxN
        y = np.zeros(b.N)

        # We generate IO representation that satisfies v2 slabbing (ModelIO)
        input_data = borg.modelio.newInputModelIO(comm, b, np.fft.rfftn(x))
        output_data = borg.modelio.newOutputModelIO(comm, b, y)

        # Generate a tiled array representation from the input data.
        tiled_input = borg.modelio.transform_for_forward(
           input_data, borg.modelio.makeTiledArrayDescriptor(comm, b.N)
        )

        # input_data cannot be used now as it has been consumed by the transformation
        # otherwise there could be inconsistencies on the shared memory buffer if
        # the original representation is not locked.
        # You may get it back with a close request.
        input_data = tiled_input.close()

        # Note that you may pile up as many transform as you want, but you want to close them
        # from bottom up afterwards if you want to access the adequate representation.

        # fwd is some forward model
        # GInput indicates the intent of the IO, here an input
        fwd.forwardModel_v3(borg.modelio.GInput(input_data))
        # GOutput indicates that we expect the output to go into output_data
        fwd.getResultForward_v3(borg.modelio.GOutput(output_data))

* The C++ class hierarchy have been updated to introduce one more abstract level of forward model. This hierarchy
  is required to support the more abstract IO for the new forward model API. In practice, existing forward models may
  be compiled and run unchanged as the API is forward compatible. The new hierarchy is:

.. graphviz::

   digraph {
     rankdir="LR";
     f -> f2;
     f2 -> pf;
     pf -> lpt;
     pf -> twolpt;
     pf -> pm;

     f [label="ForwardModel"];
     f2 [label="BORGForwardModel"];
     pf [label="ParticleBasedForwardModel"];

     lpt [label="BorgLpt"];
     twolpt [label="Borg2Lpt"];
     pm [label="BorgPM"];
   }


-------------------------------------------------------------------------------

Release 2.1
~~~~~~~~~~~

- The lensing model by Natalia has been merged to BORG.

Forward related
^^^^^^^^^^^^^^^

- Add a way of transforming all bias models into a forward deterministic
  transition. It means more flexibility at the cost of losing performance/memory
  by doing more computations than required. For example, each subcatalog needs
  its own bias which could trigger quite a lot of recomputation and/or caching.
- PMv2 optimization when sampling.
- Implement a simple (non-MPI) haar transform.
- Add EnforceMass model element to articifially fix the mass conservation.
- Forward models may support a new behavior for adjointModel_v2. They can accumulate all adjoint vectors that are provided to them through
  adjointModel_v2. The new behavior must be requested by calling BORGForwardModel::accumulateAdjoint. In that case, the user is explicitly
  requested to clear the adjoint gradient when the computation is done by calling BORGForwardModel::clearAdjointGradient.
  That behavior has been ported to pyborg. If the mode is not supported, an exception will be triggered.
- Merged Altair code.
- Bind ClassCosmo to ARES. Python binding is also active and vectorized for get_Tk.

Sampler related
^^^^^^^^^^^^^^^

- Add CG89 "higher order" symplectic integrator.

API related:
^^^^^^^^^^^^

- ManyPower bias model needs a likelihood info entry now to set the width of the prior on parameters. The name is ManyPower_prior_width in [info].
- Code cleanup in velocity field estimator. It also now supports Simplex-In-Cell (no adjoint gradient yet and only non-MPI).
- Models accept a broader range of parameters using BORGForwardModel::setModelParams.

Python related:
^^^^^^^^^^^^^^^

- *NEW tool* hades_python which supports a full deterministic transition written in python/tensorflow/jax. Data loading is still work in progress and
  may need hacking at the moment
- Python extension is supporting LikelihoodInfo and the bias as forward model element.
- Add a 'setup.py' to support compiling the BORG python module directly with pip and packaging as a wheel file.
- Samplers fully supported from Python.

Build related
^^^^^^^^^^^^^

- build.sh only downloads the dependency if the file is not already there
- Error reporting include a full C++ stacktrace on supported platforms (cmake flag is STACKTRACE_USE_BACKTRACE=ON, experimental at the moment
  It can be turned off).
- Added GIT hooks to check on basic text elements (like formatting) before running commits.
  clang-formatter absence may be overridden using ARES_CLANG_OVERRIDE=1

-------------------------------------------------------------------------------

Release 2.0alpha
~~~~~~~~~~~~~~~~

- Use a prior that is purely gaussian unit variance (Fourier) in HMC now. The cosmology is completely moved as a BORGForwardModel.
- BORGForwardModel adds the v2 API to executes model: forwardModel_v2, and adjointModel_v2. This relies heavily on the mechanics of ModelIO
- Deterministic models are now self-registering and the available lists can be dynamically queried.
- Add a hook to optionally dump extra bias fields.
- Add QLPT and QLPT_RSD forward model in extra/borg
- Lots of documentation reorganization
- Added the lyman alpha model in extra/borg
- Merged the EFT likelihood effort in extra/borg

-------------------------------------------------------------------------------

Release 1.0
~~~~~~~~~~~


Initial release



.. |in_progress| unicode:: 0x0001F528 .. hammer sign
