/*+
    ARES/HADES/BORG Package -- ./extra/demo/libLSS/demo/demo.cpp
    Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018, 2020)

+*/
#warning This is a demonstration to compile a file.
